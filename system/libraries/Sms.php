<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Shopping Cart Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Shopping Cart
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/libraries/cart.html
 * @deprecated	3.0.0	This class is too specific for CI.
 */
class CI_Sms {

	private $smppServerAddress = "sms6.rmlconnect.net"; // domestic carrier sms6.routesms.com
		private $smppServerPort = "8080"; // domestic carrier 8080
		private $userName = "FILMBOARD";
		private $password = "route@12";
		private $admin = "";
		private $to;
		private $from = "FLMBRD";
		private $msg = "";
		private $url = "";

		// == CONFIG METHODS ==
		function setSMPPServerAddress($smppServerAddress){
			$this->smppServerAddress = $smppServerAddress;
		}
		function setSMPPServerPort($smppServerPort){
			$this->smppServerPort = $smppServerPort;
		}
		function setSMSAPIUserName($userName){
			$this->userName = $userName;
		}
		function setSMSAPIPassword($password){
			$this->password = $password;
		}
		// == CONFIG METHODS ==

		function setAdminAddress($admin){
			$this->admin = $admin;
		}
		function setAddress($to){
			$this->to = $to;
		}
		function setFromAddress($from){
			$this->from = $from;
		}
		function setOTPMessage($msg){
			$this->msg = $msg;
		}
		function sendOTP(){
			// if(PROJECTSTATUS!="LIVE" && PROJECTSTATUS!="STAGING"){ // DO NOT WASTE SMS
			// 	return true;
			// }

			// == VALIDATION ==
			if(!isset($this->smppServerAddress) || empty($this->smppServerAddress)){
				return false;
			}
			if(!isset($this->smppServerPort) || empty($this->smppServerPort)){
				return false;
			}
			if(!isset($this->userName) || empty($this->userName)){
				return false;
			}
			if(!isset($this->password) || empty($this->password)){
				return false;
			}
			if(!isset($this->to) || empty($this->to)){
				return false;
			}
			if(!isset($this->from) || empty($this->from)){
				return false;
			}
			if(!isset($this->msg) || empty($this->msg)){
				return false;
			}
			// == VALIDATION ==

			$encodedMessage = urlencode($this->msg);
			$url = 'https://'.$this->smppServerAddress.'/bulksms/bulksms?username='.$this->userName.'&password='.$this->password.'&type=0&dlr=1&destination='.$this->to.'&source='.$this->from.'&message='.$encodedMessage;

			$responseStr = file_get_contents($url);

			// == TEST ==
				// echo "<pre>";
				// print_r($responseStr);
				// echo "</pre>";
				// exit;
				// 1703|8767510333
				// 1701|918767510333:cea7962f-1534-4275-af62-b4bdfd888b55
				// 1707|8767510333
			// == TEST ==
			$pos = strpos($responseStr, "|");
			if($pos!=false && $pos>=0){
				if(substr($responseStr, 0, $pos)==1701){
					// 1701:Success, Message Submitted Successfully, In this case you will receive
					// the response 1701|<CELL_NO>|<MESSAGE ID>, The message Id can
					// then be used later to map the delivery reports to this message.

					// send a copy to ADMIN
					if(isset($this->admin) && !empty($this->admin)){
						if(PROJECTSTATUS!="LIVE" && PROJECTSTATUS!="STAGING"){ // DO NOT WASTE SMS
							// $responseStr = file_get_contents($this->msg);
						}
					}
					return true;
				} else {
					return false;
				}
			}
		}
		function generateOTP(){
			$id = rand(111111,999999);
			return $id;
		}
		function send($to, $message){
			$this->to = $to;
			$this->encodedMessage = urlencode($message);

			//$encodedMessage = urlencode($this->msg);
			$this->url = 'https://'.$this->smppServerAddress.'/bulksms/bulksms?username='.$this->userName.'&password='.$this->password.'&type=0&dlr=1&destination='.$this->to.'&source='.$this->from.'&message='.$this->encodedMessage;
			$result = $this->api_call($this->url);
			print_r($result);
			//$otpObj->setOTPMessage('A new Job Requirement has been posted on the portal. Open Jobs Master in Admin Panel to view details.
			//Approve the requirement only after confirming its genuineness. Once approved, status cannot be reverted.');
			//$smsStatus = $otpObj->sendOTP();
			//$this->api_call($url);
		}
		function api_call($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;

    }
}
