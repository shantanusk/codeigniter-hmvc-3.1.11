<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class CI_Common extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }
    function file_check($url, $file_name){
  		if(file_exists($url) && !$file_name == "")
  		{
  			return TRUE;
  		}else{
  			return FALSE;
  		}
  	}
}

/* End of file Pdf.php */
/* Location: ./application/libraries/Common.php */
