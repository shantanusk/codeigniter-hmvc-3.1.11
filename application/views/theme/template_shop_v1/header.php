<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $descriptions;?>">
    <meta name="keywords" content="<?php echo $keywords;?>">
    <title><?php echo $title;?></title>

    <!-- using online links -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/bootstrap-4.3.1-dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/fontawesome-free-5.8.2/css/all.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/scrollbar/jquery.mCustomScrollbar.min.css');?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/sidebar/css/main.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/sidebar/css/sidebar-themes.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css');?>">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/vendor-library/sidebar/img/favicon.png');?>" />
</head>

<body>
    <div class="page-wrapper default-theme sidebar-bg bg1">