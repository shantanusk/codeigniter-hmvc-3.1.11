<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- page-content" -->
<footer class="bg-dark text-center">
    <?php echo $copyright;?>
</footer>
</div>

<!-- page-wrapper -->

<!-- using online scripts -->
<script src="<?php echo base_url('assets/vendor-library/jquery-3.3.1/jquery.min.js');?>"></script>
</script>
<script src="<?php echo base_url('assets/vendor-library/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js');?>">
</script>
<script src="<?php echo base_url('assets/vendor-library/scrollbar/jquery.mCustomScrollbar.concat.min.js');?>"></script>

<script src="<?php echo base_url('assets/vendor-library/sidebar/js/main.js');?>"></script>
</body>

</html>