<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><nav class="navbar fixed-top navbar-expand-lg navbar-light bg-dark">
<span id="toggle-sidebar" class="text-white"><i class="fas fa-bars fa-2x"></i></span>
<a class="navbar-brand" href="<?php echo base_url();?>">
&nbsp;&nbsp;<?php echo APP_SRT_NAME;?>
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav ml-auto">
<li class="nav-item">
    <a class="nav-link" href="#"><i class="fas fa-sign-out-alt"></i></a>
</li>
</ul>
</div>
</nav>