<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
      <footer class="footer bg-dark-sec text-white">
      	<div class="container-fluid p-0 m-0">
      		<div class="row m-0 p-0 pt-4">
      			<div class="col-md-3 col-sm-4 col-xs-4 px-4">
      				<div class="footer-info">
      					<h5>THE COMPANY</h5>
      					<ul>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>founders">The Founders</a></li>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>strategic-advisors">Strategic Advisors</a></li>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>careers">Careers</a></li>
      						<!--<li><a href="<?php //echo BASE_URL; ?>/contact-us.php">Contact Us</a></li>-->
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>news-and-media">News & Media</a></li>
      					</ul>
      				</div>
      			</div>
      			<div class="col-md-3 col-sm-4 col-xs-4">
      				<div class="footer-info">
      					<h5>QUICK LINKS</h5>
      					<?php

                  // getlisturl
                  $getListedURL = '';
                  if (isset($isSellerLogin) && !empty($isSellerLogin)) {
                      $getListedURL = 'my-inventory';
                  } else if (isset($isProducerLogin) && !empty($isProducerLogin)) {
                      $getListedURL = 'producer-dashboard';
                  } else {
                      $getListedURL = 'user/sign-in';
                  }

                  // post review url
                  $postReviewURL = '';
                  if ((isset($isSellerLogin) && !empty($isSellerLogin)) || (isset($isProducerLogin) && !empty($isProducerLogin))) {
                      $postReviewURL = 'index.php#banner-section';
                  } else {
                      $postReviewURL = 'user/sign-in';
                  }
                  ?>

      					<ul>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url() . $getListedURL; ?>">Get Listed</a></li>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url() . $postReviewURL; ?>">Post a Review</a></li>
      							<?php
                        $postJobURL = '';
                        if (isset($isProducerLogin) && !empty($isProducerLogin)) {
                            ?>
      									<li style="margin-left: -25px !important;"><a href="<?php echo base_url() . 'post-a-requirement'; ?>">Post a Job</a></li>
      									<?php
                          } else if (!isset($isSellerLogin) || empty($isSellerLogin)) {?>
      									<li style="margin-left: -25px !important;"><a href="<?php echo base_url() . 'producer-sign-in'; ?>">Post a Job</a></li>
      									<?php
                          }
                          ?>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>faq">FAQs</a></li>
      						<li style="margin-left: -25px !important;"><a href="#0">Sitemap</a></li>
      						<li style="margin-left: -25px !important;"><a target="_blank" href="http://blog.filmboardmovies.com/">Blog</a></li>
      					</ul>
      				</div>
      			</div>
      			<div class="col-md-3 col-sm-4 col-xs-4">
      				<div class="footer-info">
      					<h5>LEGAL</h5>
      					<ul>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>legal/terms-conditions">Terms & Conditions</a></li>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>legal/privacy-policy">Privacy Policy</a></li>
      						<li style="margin-left: -25px !important;"><a href="<?php echo base_url(); ?>legal/disclaimer">Disclaimer</a></li>
      					</ul>
      				</div>
      			</div>
      			<div class="col-md-3 col-sm-4 col-xs-12 last_col">
      				<?php //$contact_usData = $functions->getFilmBoardContactDetails();?>
      				<div class="footer-info">
      					<span class="dis0o">
      						<h5>FOLLOW US</h5>
      							<?php
                      foreach($this->COM->get_social_link() as $social) {
                        if($social->icon_link == "https://www.facebook.com/filmboardmovies"){
                          ?>

      										<a href="<?php echo $social->icon_link;?>" target="_blank">
      											<i class="text-red fab fa-facebook-square fa-2x"></i>
      										</a>

      							<?php
                  }elseif($social->icon_link == "https://twitter.com/filmboardmovies"){?>

      										<a href="<?php echo $social->icon_link;?>" target="_blank">
      											<i class="ml-2 text-red fab fa-twitter-square fa-2x"></i>
      										</a>

      							<?php
                  }elseif($social->icon_link == "https://www.linkedin.com/company/filmboard-movies-pvt-ltd/"){
                          ?>

      										<a href="<?php echo $social->icon_link;?>" target="_blank">
      											<i class="ml-2 text-red fab fa-linkedin-in fa-2x"></i>
      										</a>

      							<?php
                  }elseif($social->icon_link == "https://www.instagram.com/filmboardmovies/"){
                          ?>

      										<a href="<?php echo $social->icon_link;?>" target="_blank">
      										    <i class="ml-2 text-red fab fa-instagram fa-2x"></i>
      										</a>

      							<?php
                  }
                  }?>
                  <br />
                  <br />
      					</span>
      					<span class="dis1o">
      						<a href="<?php echo base_url(); ?>/contact-us"><h5>CONTACT US</h5></a>
      						<ul class="call_contact">
      							<li style="margin-left: -25px !important;"><a href="#0"><i class="fas fa-phone-volume"></i> 9326257428</a></li>
      							<li style="margin-left: -25px !important;"><a href="mailto:hr@filmboardmovies.com"><i class="fas fa-envelope"></i> hr@filmboardmovies.com</a></li>
      						</ul>
      					</span>
      				</div>
              <?php ?>
            </div>
      		</div>
      		<div class="bg-dark-sec text-center text-white p-3">
            <hr class="border-white" />
      			<?php echo $copyright;?>
      		</div>
      	</div>
      </footer>

    </div>
      <!-- page-wrapper -->

      <!-- using online scripts -->

      <script src="<?php echo base_url('assets/vendor-library/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js');?>"></script>

</body>

</html>
