<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section>
  <div class="row m-0 p-0">
    <div class="col-12 m-0 p-0">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <?php
            $ac=1;
            foreach ($banner as $bnr) {
                if($ac==1){
                ?>
                <div class="carousel-item active">
                  <a href="<?php echo $bnr->link;?>">
                    <img class="d-block w-100" style="height:80vh !important;" src="<?php echo base_url('assets/uploads/banner/'.str_replace('.jpg','_crop.jpg',$bnr->image_name));?>" alt="<?php echo $bnr->alt_text;?>">
                  </a>
                </div>
                <?php
                $ac++;
              }else{
                ?>
                <div class="carousel-item">
                  <a href="<?php echo $bnr->link;?>">
                    <img class="d-block w-100" style="height:80vh !important;" src="<?php echo base_url('assets/uploads/banner/'.str_replace('.jpg','_crop.jpg',$bnr->image_name));?>" alt="<?php echo $bnr->alt_text;?>">
                  </a>
                </div>
                <?php
              }
            }
          ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</section>
