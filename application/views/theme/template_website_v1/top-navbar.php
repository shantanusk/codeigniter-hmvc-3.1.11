<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header>
  <div class="row m-0 p-0">
      <div class="col-12">
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light border-bottom">
          <a class="navbar-brand" href="<?php echo base_url();?>">
            <img style="width:13rem;" src="<?php echo base_url('assets/uploads/logo/logo.png');?>" />
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item<?php echo ($page=="Home") ? " active":""; ?>">
                <a class="nav-link" href="<?php echo base_url();?>">Home</a>
              </li>
                <li class="nav-item<?php echo ($page=="About Us") ? " active":""; ?>">
                  <a class="nav-link" href="<?php echo base_url('about-us');?>">About Us</a>
                </li>
              <li class="nav-item<?php echo ($page=="FILMBOARD LINE PRODUCTION") ? " active":""; ?>">
                <a class="nav-link" href="<?php echo base_url('line-production');?>">Line Production</a>
              </li>
              <li class="nav-item<?php echo ($this->router->fetch_class() == "search") ? " active":""; ?>">
                <a class="nav-link" href="<?php echo base_url('search/talent/actors');?>">Listing</a>
              </li>
              <li class="nav-item<?php echo ($this->router->fetch_class() == "jobs") ? " active":""; ?>">
                <a class="nav-link" href="<?php echo base_url('requirements');?>">Requirement</a>
              </li>
              <li class="nav-item<?php echo ($page=="FAQ's") ? " active":""; ?>">
                <a class="nav-link" href="<?php echo base_url('faq');?>">FAQ's</a>
              </li>
              <li class="nav-item<?php echo ($page=="Contact Us") ? " active":""; ?>">
                <a class="nav-link" href="<?php echo base_url('contact-us');?>">Contact Us</a>
              </li>
              <?php if(!$is_logged_in){?>

                <li class="nav-item<?php echo ($page=="User Sign UP") ? " active":""; ?>">
                  <a class="nav-link" href="<?php echo base_url('user/sign-up');?>">Register</a>
                </li>
                <li class="nav-item<?php echo ($page=="User Sign IN") ? " active":""; ?>">
                  <a class="nav-link" href="<?php echo base_url('user/sign-in');?>">Login</a>
                </li>

              <?php }else{?>

              <li class="nav-item dropdown<?php echo ($page=="Login") ? " active":""; ?>">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user-cog"></i> My Profile
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo base_url('user/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                    <a class="dropdown-item" href="<?php echo base_url('user/inbox');?>"><i class="fas fa-envelope"></i> Inbox <span class="badge badge-pill badge-danger float-right">3</span></a>
                      <a class="dropdown-item" href="<?php echo base_url('user/notification');?>"><i class="fas fa-bell"></i> Notification <span class="badge badge-pill badge-danger float-right">3</span></a>
                      <div class="dropdown-divider border-bottom-green"></div>
                    <a class="dropdown-item" href="<?php echo base_url('user/profile');?>"><i class="fas fa-user-circle"></i> Your's Profile</a>
                  <a class="dropdown-item" href="<?php echo base_url('user/change-password');?>"><i class="fas fa-key"></i> Change Password</a>
                    <div class="dropdown-divider border-bottom-green"></div>
                  <a class="dropdown-item" href="<?php echo base_url('user/wallet');?>"><i class="fas fa-wallet"></i> Wallet</a>
                  <div class="dropdown-divider border-bottom-green"></div>
                  <a class="dropdown-item" href="<?php echo base_url('user/sign-out');?>"><i class="fas fa-sign-out-alt"></i> Logout</a>
                </div>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fas fa-shopping-cart"></i> Cart</a>
              </li>
              <?php }?>
            </ul>
          </div>
        </nav>

      </div>

  </div>
</header>

<section class="container-fluid border-top-green">
  <div class="row m-0 pt-2 justify-content-center bg-white">
    <div class="col-md-2 mt-2 mr-2 pr-2 m-0 p-0">
      <?php if($this->router->fetch_class() == "jobs"){?>
      <a class="btn btn-success bg-green form-control border-0 rounded-0 text-white">Post Your Requirements</a>
    <?php }else{
      ?>
      <a class="btn btn-success bg-green form-control border-0 rounded-0 text-white">Get Yourself Listed</a>
      <?php
    }?>
    </div>
    <div class="col-md-9 mt-2">

      <div class="input-group mb-3">
        <div class="input-group-prepend">

            <?php
            $types = array(
              "" => "Choose....",
            );
            if($this->router->fetch_class() == "jobs"){
              foreach ($this->COM->get_active_type() as $ty) {
                $types[base_url('requirements/'.strtolower(url_title($ty->category_name)))] = $ty->category_name;
                //$types[strtolower($ty->category_name)] = $ty->category_name;
                //echo '<option value="'.base_url('search/'.strtolower($ty->category_name)).'">'.$ty->category_name.'</option>';
              }
              $vty_atr = ' id="type" class="selectpicker show-tick form-control custom-select rounded-0" data-live-search="true" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"';
              echo form_dropdown('type', $types, base_url('requirements/'.strtolower($this->uri->segment(2))), $vty_atr);
            }else{
              foreach ($this->COM->get_active_type() as $ty) {
                $types[base_url('search/'.strtolower($ty->category_name))] = $ty->category_name;
                //$types[strtolower($ty->category_name)] = $ty->category_name;
                //echo '<option value="'.base_url('search/'.strtolower($ty->category_name)).'">'.$ty->category_name.'</option>';
              }
              $vty_atr = ' id="type" class="selectpicker show-tick form-control custom-select rounded-0" data-live-search="true" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"';
              echo form_dropdown('type', $types, base_url('search/'.strtolower($this->uri->segment(2))), $vty_atr);
            }

            ?>
        </div>
        <?php
        $list = array(
          "" => "Choose",
        );

        if($this->router->fetch_class() == "jobs"){
          foreach ($this->JOB->get_requirement_list() as $lst) {
            $lst_view = ucwords(strtolower("Looking For ".$lst->no_of_openings_qty.' '.$lst->gender_type.' '.((!$lst->category_sub == 0) ? $lst->category_sub : $lst->category).' for '.$lst->project_type.' for '.$lst->project_name.' by '.(!$lst->company_name=="" ? $lst->company_name: $lst->first_name.' '.$lst->last_name)));

            if($lst->sub_sub_category_id == 0){

                $list[base_url('requirements/details/'.$lst->id.'/'.$lst->category_id.'/'.$lst->sub_category_id.'/0/'.str_replace(" ", "-",strtolower(strtolower($lst->types).'/'.strtolower($lst->category).'/'.url_title($lst_view))))] = $lst_view;

            }else{

                $list[base_url('requirements/details/'.$lst->id.'/'.$lst->category_id.'/'.$lst->sub_category_id.'/'.$lst->sub_sub_category_id.'/'.str_replace(" ", "-",strtolower(strtolower($lst->types).'/'.strtolower($lst->category).'/'.strtolower($lst->category_sub).'/'.url_title($lst_view))))] = $lst_view;


            }

            //echo '<option value="'.base_url('search/'.strtolower($ty->category_name)).'">'.$ty->category_name.'</option>';
          }
          $list_atr = ' id="list" data-size="5" title="Search Jobs Listing...." class="selectpicker form-control custom-select rounded-0" data-live-search="true" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"';
                     echo form_dropdown('list', $list, '', $list_atr);
        }else{

          foreach ($this->COM->get_active_profile_list() as $lst) {

            if(!isset($lst->category_sub)){
              if($lst->types_id==4){
                $list[base_url('search/'.str_replace(" ", "-",strtolower(strtolower($lst->types).'/'.strtolower($lst->category).'/details/'.$lst->user_id.'/'.$lst->pr_id.'/'.$lst->types_id.'/'.$lst->category_id.'/0/'.$lst->first_name.'-'.$lst->last_name)))] = ucwords(strtolower($lst->proeprty_name.' - '.$lst->category.' - '.$lst->types));
              }else{
                $list[base_url('search/'.str_replace(" ", "-",strtolower(strtolower($lst->types).'/'.strtolower($lst->category).'/details/'.$lst->user_id.'/'.$lst->pr_id.'/'.$lst->types_id.'/'.$lst->category_id.'/0/'.$lst->first_name.'-'.$lst->last_name)))] = ucwords(strtolower($lst->first_name.' '.$lst->last_name.' - '.$lst->category.' - '.$lst->types));
              }

            }else{
                if($lst->types_id==3){
                  $list[base_url('search/'.$lst->category_sub_id.'/'.str_replace(" ", "-",strtolower(strtolower($lst->types).'/'.strtolower($lst->category).'/'.strtolower($lst->category_sub).'/details/'.$lst->user_id.'/'.$lst->pr_id.'/'.$lst->types_id.'/'.$lst->category_id.'/'.$lst->first_name.'-'.$lst->last_name)))] = ucwords(strtolower($lst->inventory_name.' '.$lst->first_name.' '.$lst->last_name.' - '.$lst->category_sub.' - '.$lst->types));
                }else if($lst->types_id==4){
                  $list[base_url('search/'.$lst->user_id.'/'.str_replace(" ", "-",strtolower(strtolower($lst->types).'/'.strtolower($lst->category).'/'.strtolower($lst->category_sub).'/details/'.$lst->pr_id.'/'.$lst->types_id.'/'.$lst->category_id.'/'.$lst->category_sub_id.'/'.$lst->first_name.'-'.$lst->last_name)))] = ucwords(strtolower($lst->proeprty_name.' - '.$lst->category_sub.' - '.$lst->types));
                }else{
                    $list[base_url('search/'.str_replace(" ", "-",strtolower(strtolower($lst->types).'/'.strtolower($lst->category).'/'.strtolower($lst->category_sub).'/details/'.$lst->user_id.'/'.$lst->pr_id.'/'.$lst->types_id.'/'.$lst->category_id.'/'.$lst->category_sub_id.'/'.$lst->first_name.'-'.$lst->last_name)))] = ucwords(strtolower($lst->first_name.' '.$lst->last_name.' - '.$lst->category_sub.' - '.$lst->types));
                }

            }

            //echo '<option value="'.base_url('search/'.strtolower($ty->category_name)).'">'.$ty->category_name.'</option>';
          }
          $list_atr = ' id="list" data-size="5" data-container="body"
           data-live-search="true" title="Choose Listing...."
            data-actions-box="true" data-virtual-scroll="false"
             class="selectpicker form-control custom-select rounded-0" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"';
                     echo form_dropdown('list', $list, '', $list_atr);
        }

        ?>

        <div class="input-group-append">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary rounded-0" type="button"><i class="fas fa-search"></i> Search</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
