<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-dark p-0">
  <span class="navbar-brand bg-white" style="width: 280px !important;">
    <span id="toggle-sidebar" class="text-dark"><i class="pl-2 fas fa-bars fa-2x"></i></span>
    <a href="<?php echo base_url('user/dashboard');?>">

      &nbsp;&nbsp;<img style="width:13rem;margin-top:-15px !important;" src="<?php echo base_url('assets/uploads/logo/logo.png');?>" alt="<?php echo APP_SRT_NAME;?>" />
    </a>
  </span>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item<?php echo ($page=="Home") ? " active":""; ?>">
        <a class="nav-link" href="<?php echo base_url();?>">Home</a>
      </li>
        <li class="nav-item<?php echo ($page=="About Us") ? " active":""; ?>">
          <a class="nav-link" href="<?php echo base_url('about-us');?>">About Us</a>
        </li>
      <li class="nav-item<?php echo ($page=="FILMBOARD LINE PRODUCTION") ? " active":""; ?>">
        <a class="nav-link" href="<?php echo base_url('line-production');?>">Line Production</a>
      </li>
      <li class="nav-item<?php echo ($this->router->fetch_class() == "search") ? " active":""; ?>">
        <a class="nav-link" href="<?php echo base_url('search');?>">Listing</a>
      </li>
      <li class="nav-item<?php echo ($this->router->fetch_class() == "jobs") ? " active":""; ?>">
        <a class="nav-link" href="<?php echo base_url('requirements');?>">Requirement</a>
      </li>
      <li class="nav-item<?php echo ($page=="FAQ's") ? " active":""; ?>">
        <a class="nav-link" href="<?php echo base_url('faq');?>">FAQ's</a>
      </li>
      <li class="nav-item<?php echo ($page=="Contact Us") ? " active":""; ?>">
        <a class="nav-link" href="<?php echo base_url('contact-us');?>">Contact Us</a>
      </li>

      <?php if(!$is_logged_in){?>

        <li class="nav-item<?php echo ($page=="User Sign UP") ? " active":""; ?>">
          <a class="nav-link" href="<?php echo base_url('user/sign-up');?>">Register</a>
        </li>
        <li class="nav-item<?php echo ($page=="User Sign IN") ? " active":""; ?>">
          <a class="nav-link" href="<?php echo base_url('user/sign-in');?>">Login</a>
        </li>

      <?php }else{?>

      <li class="nav-item dropdown<?php echo ($page=="Login") ? " active":""; ?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-cog"></i> My Profile
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url('user/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
            <a class="dropdown-item" href="<?php echo base_url('user/inbox');?>"><i class="fas fa-envelope"></i> Inbox <span class="badge badge-pill badge-danger float-right">3</span></a>
              <a class="dropdown-item" href="<?php echo base_url('user/notification');?>"><i class="fas fa-bell"></i> Notification <span class="badge badge-pill badge-danger float-right">3</span></a>
              <div class="dropdown-divider border-bottom-green"></div>
            <a class="dropdown-item" href="<?php echo base_url('user/profile');?>"><i class="fas fa-user-circle"></i> Your's Profile</a>
          <a class="dropdown-item" href="<?php echo base_url('user/change-password');?>"><i class="fas fa-key"></i> Change Password</a>
            <div class="dropdown-divider border-bottom-green"></div>
          <a class="dropdown-item" href="<?php echo base_url('user/wallet');?>"><i class="fas fa-wallet"></i> Wallet</a>
          <div class="dropdown-divider border-bottom-green"></div>
          <a class="dropdown-item" href="<?php echo base_url('user/sign-out');?>"><i class="fas fa-sign-out-alt"></i> Logout</a>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-shopping-cart"></i> Cart</a>
      </li>
      <?php }?>
    </ul>
  </div>
</nav>
