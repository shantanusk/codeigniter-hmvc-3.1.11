<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="<?php echo $descriptions;?>">
  <meta name="keywords" content="<?php echo $keywords;?>">
  <title><?php echo $title;?></title>

  <base href="<?php echo base_url();?>">
  <link rel="icon" href="<?php echo base_url('favicon.ico');?>">

  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('assets/uploads/ico/apple-icon-57x57.png');?>">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/uploads/ico/apple-icon-60x60.png');?>">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/uploads/ico/apple-icon-72x72.png');?>">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/uploads/ico/apple-icon-76x76.png');?>">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/uploads/ico/apple-icon-114x114.png');?>">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('assets/uploads/ico/apple-icon-120x120.png');?>">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('assets/uploads/ico/apple-icon-144x144.png');?>">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/uploads/ico/apple-icon-152x152.png');?>">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/uploads/ico/apple-icon-180x180.png');?>">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url('assets/uploads/ico/android-icon-192x192.png');?>">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/uploads/ico/favicon-32x32.png');?>">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/uploads/ico/favicon-96x96.png');?>">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/uploads/ico/favicon-16x16.png');?>">
  <link rel="manifest" href="<?php echo base_url('assets/uploads/ico/manifest.json');?>">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo base_url('assets/uploads/ico/ms-icon-144x144.png');?>">
  <meta name="theme-color" content="#ffe400">
  <meta name="google-site-verification" content="TRESsLvvl-cT0fu3CDinmGCbjjuY4vB9OtNXFD_HAkI" />
  <meta name="msvalidate.01" content="11CEC95DAA1BA2D7FAFC38A38F541648" />
  <meta name="yandex-verification" content="7ece7a8360149b56" />
  
  <link rel="manifest" crossorigin="use-credentials"  href="<?php echo base_url('assets/uploads/ico/manifest.json');?>"/>


    <!-- using online links -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/bootstrap-4.3.1-dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/fontawesome-free-5.8.2/css/all.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/scrollbar/jquery.mCustomScrollbar.min.css');?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/sidebar/css/main.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/sidebar/css/sidebar-themes.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/user-custom.css');?>">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/vendor-library/sidebar/img/favicon.png');?>" />

    <!-- using online scripts -->
    <script src="<?php echo base_url('assets/vendor-library/jquery-3.3.1/jquery.min.js');?>"></script>
    </script>
    <script src="<?php echo base_url('assets/vendor-library/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js');?>">
    </script>
</head>

<body>
    <div class="page-wrapper default-theme sidebar-bg bg1 toggled">
