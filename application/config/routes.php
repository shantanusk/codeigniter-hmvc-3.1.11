<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'website/welcome';
$route['404_override'] = 'website/welcome/show_404';
$route['translate_uri_dashes'] = TRUE;

/**Web Pages */
$route['about-us'] = "website/welcome/about_us";
$route['line-production'] = "website/welcome/fblp";
$route['contact-us'] = "website/welcome/contact_us";
$route['founders'] = "website/welcome/founders";
$route['strategic-advisors'] = "website/welcome/strategic_advisors";
$route['careers'] = "website/welcome/career";
$route['news-and-media'] = "website/welcome/media";

/**Legal Pages*/
$route['legal/terms-conditions'] = "legal/terms_conditions";
$route['legal/privacy-policy'] = "legal/privacy_policy";
$route['disclaimer'] = "lega/disclaimer";

/**User Routing */
$route['login'] = "user/login";
$route['user/sign-in'] = "user/login";
$route['register'] = "user/login/sign_up";
$route['user/sign-up'] = "user/login/sign_up";
$route['logout'] = "user/login/logout";
$route['user/sign-out'] = "user/login/logout";

require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();
$db->where('active', 'Yes');
$query = $db->get( 'fb_category_master' );
$result = $query->result();
foreach( $result as $row )
{
  $route[ "search/".strtolower($row->category_name) ] = "website/search";
    $route[ "search/".strtolower($row->category_name)."/(:num)" ] = "website/search";
    $db->where('category_id', $row->id);
    $db->where('active', 'Yes');
    $query_c = $db->get( 'fb_sub_category_master' );
    $result_c = $query_c->result();
    foreach ($result_c as $cat) {
      $route[ "search/".strtolower($cat->permalink) ] = "website/search";
      $route[ "search/".strtolower($cat->permalink)."/(:num)" ] = "website/search";
      $route[ "search/".strtolower($cat->permalink)."/details/(:num)/(:num)/(:num)/(:num)/(:num)/(:any)" ] = "website/search/details/$1/$2/$3/$4/$5";
      $db->where('subcategory_id', $cat->id);
      $db->where('active', 'Yes');
      $query_sc = $db->get( 'fb_sub_sub_category_master' );
      $result_sc = $query_sc->result();

      foreach ($result_sc as $scat) {
          $route[ "search/".strtolower($scat->permalink) ] = "website/search";
          $route[ "search/".strtolower($scat->permalink)."/(:num)" ] = "website/search";
          $route["search/".strtolower($scat->permalink)."/details/(:num)/(:num)/(:num)/(:num)/(:num)/(:any)"] = "website/search/details/$1/$2/$3/$4/$5";
          //$route[ "search/".strtolower($cat->permalink)."/details/(:num)/(:num)/(:num)/(:num)/(:num)/(:any)" ] = "website/search/details/$1/$2/$3/$4/$5";
      }
    }
    //$route[ $row->fldv_sub_module_url.'/details/(:any)' ] = 'admin/product/details/$0';
}
//echo "<pre>";
//print_r($route);
/***List */
$route['search'] = "website/search";
$route['search/(:num)'] = "website/search";
$route['search/(:any)'] = "website/search";
$route['search/(:any)/(\d+)'] = "website/search";
$route['search/(:any)/(:any)'] = "website/search";
$route['search/(a-z)/(:any)/(\d+)'] = "website/search";
$route['search/(:any)/(:any)/(:any)'] = "website/search";
$route['search/(:any)/(:any)/(:any)/(\d+)'] = "website/search";
$route['search/details/(:num)/(:num)/(:num)/(:num)/(:num)/(:any)/(:any)/(:any)'] = "website/search/details/$1/$2/$3/$4";
$route['search/details/(:num)/(:num)/(:num)/(:num)/(:num)/(:any)/(:any)/(:any)/(:any)'] = "website/search/details/$1/$2/$3/$4";
//$route['search/(:any)'] = "website/search/index";

/***Jobs */
$route['requirements'] = "website/jobs";
$route['requirements/(:num)'] = "website/jobs";
$route['requirements/(:any)'] = "website/jobs";
$route['requirements/(:any)/(\d+)'] = "website/jobs";
$route['requirements/(:any)/(:any)'] = "website/jobs";
$route['requirements/(a-z)/(:any)/(\d+)'] = "website/jobs";
$route['requirements/(:any)/(:any)/(:any)'] = "website/jobs";
$route['requirements/(:any)/(:any)/(:any)/(\d+)'] = "website/jobs";
$route['requirements/details/(:num)/(:num)/(:num)/(:num)/(:any)/(:any)/(:any)'] = "website/jobs/details/$1/$2/$3/$4";
$route['requirements/details/(:num)/(:num)/(:num)/(:num)/(:any)/(:any)/(:any)/(:any)'] = "website/jobs/details/$1/$2/$3/$4";
/**Admin Routing */
