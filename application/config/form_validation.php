<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$config['error_prefix'] = '<div class="with-errors mt-1">';
$config['error_suffix'] = '</div>';

/**Login */
$config['login'] = array(
    array(
        'field' => 'username',
        'label' => 'Email / Mobile',
        'rules' => 'trim|required',
        'errors' => array(
            'required' => 'Email / Mobile Required!',
        ),
    ),
    array(
        'field' => 'user_password',
        'label' => 'User Password',
        'rules' => 'trim|required',
        'errors' => array(
            'required' => 'Password Required!',
        ),
    )
);


/**Login */
$config['register'] = array(
    array(
        'field' => 'first_name',
        'label' => 'First Name',
        'rules' => 'trim|required',
        'errors' => array(
            'required' => 'First Name Required!',
        ),
    ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'trim|required',
            'errors' => array(
                'required' => 'Last Name Required!',
            ),
        ),
    array(
        'field' => 'email',
        'label' => 'Email',
        'rules' => 'trim|required|valid_email|is_unique[fb_user.email]',
        'errors' => array(
            'required' => 'Email ID Required!',
                'valid_email' => 'Enter Valid Email ID!',
                    'is_unique' => 'This Email ID Is Already Registered!',
        ),
    ),
        array(
            'field' => 'mobile',
            'label' => 'Mobile',
            'rules' => 'trim|required|min_length[10]|is_unique[fb_user.mobile]',
            'errors' => array(
                'required' => 'Mobile Required!',
                    'min_length' => 'Enter Valid Mobile Number!',
                        'is_unique' => 'This Mobile Number Is Already Registered!',
            ),
        ),
    array(
        'field' => 'user_password',
        'label' => 'User Password',
        'rules' => 'trim|required|min_length[6]',
        'errors' => array(
            'required' => 'Password Required!',
                'min_length' => 'Password Should Be Greater Then 6 Character!',
        ),
    ),
    array(
        'field' => 'confirm_password',
        'label' => 'Confirm Password',
        'rules' => 'trim|required|matches[user_password]',
        'errors' => array(
            'required' => 'Confirm Password Required!',
            'matches' => 'Confirm Password Should Match With Password!',
        ),
    )
);
