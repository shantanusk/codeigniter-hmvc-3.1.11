<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// ========== social login redirect ====================

$redirectURL="";
  //include_once "include/social-login-config.inc.php";

  // == FACEBOOK LOGIN ==

    //https://www.filmboardmovies.com/social-login-callback.php?loginType=seller&redirect=https://www.filmboardmovies.com/my-inventory.php
    $fbLoginUrl = (base_url().'/social-login-callback.php?loginType=seller&redirect='.$redirectURL);
  // == FACEBOOK LOGIN ==

  // == GOOGLE PLUS LOGIN ==

    //http://www.filmboardmovies.com/google-login-callback.php?loginType=seller&redirect=http://www.filmboardmovies.com/my-inventory.php
    $google_redirect_url 	= base_url().'/google-login-callback.php?loginType=seller&redirect='.$redirectURL; //path to your script
    //$gClient->setRedirectUri($google_redirect_url);
    $googleAuthUrl = "";//$gClient->createAuthUrl();

    /* if(isset($_SESSION[SITE_NAME.'googlePlusToken'])){
      $gClient->setAccessToken($_SESSION[SITE_NAME.'googlePlusToken']);
    } */
  // == GOOGLE PLUS LOGIN ==

  // ============ linkedin ====================

    //$loginType = 'seller';
    //include_once('linkedin/test/config.php');

    //https://www.filmboardmovies.com/linkedin-login-callback.php?loginType=seller
    //https://www.filmboardmovies.com/linkedin-login-callback.php?loginType=producer

    $linkedAuthURL = "";//"https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=".$config['Client_ID']."&redirect_uri=".$config['seller_callback_url']."&state=623846780&scope=r_basicprofile r_emailaddress";

  // ============ linkedin::end ====================

?>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 p-0 justify-content-center">
    <div class="col-md-12 col-12 m-0 p-0">
      <div class="row m-0 py-3 justify-content-center bg-red text-white">
        <div class="col-md-10 pt-5 pb-4 cap text-center">
          <h4>Show the world what you’re made of!</h4>
			       <p>Get listed on FILMBOARD to get noticed and open DOORS to great opportunities</p>
        </div>
      </div>
    </div>
    <div class="col-md-6 py-5 px-5 border-right">
      <h3 class="text-green font-weight-bold mb-3">CREATE NEW ACCOUNT</h3>
      <hr class="border-bottom-green" />
      <?php if($this->session->flashdata('message')) {$message =$this->session->flashdata('message'); ?>
        <div class="<?php echo $message['class'] ?>" role="alert">
          <strong><?php echo $message['type'] ?>!</strong> <?php echo $message['message'] ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <script type="application/javascript">
          /** After windod Load */
          $(window).bind("load", function() {
          window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
            });
          }, 10000);
          });
          </script>
        <?php }?>
        <?php
        $form_attr = array('class' => 'row', 'data-toggle' => 'validator', 'role' => 'form');
          echo form_open(current_url(), $form_attr);
        ?>
        <div class="form-group col-md-6 mt-2">
          <label for="first_name"><i class="fas fa-id-card-alt"></i> First Name <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
          <input type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name');?>" required data-error="First Name Required!" class="form-control rounded-0" aria-describedby="first_nameHelp" placeholder="Enter First Name">
          <span class="help-block mt-1 with-errors"></span>
          <?php echo form_error('first_name'); ?>
        </div>
          <div class="form-group col-md-6 mt-2">
            <label for="last_name"><i class="fas fa-id-card-alt"></i> Last Name <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
            <input type="text" name="last_name" id="last_name" value="<?php echo set_value('last_name');?>" required data-error="Last Name Required!" class="form-control rounded-0" aria-describedby="last_nameHelp" placeholder="Enter Last Name">
            <span class="help-block mt-1 with-errors"></span>
            <?php echo form_error('last_name'); ?>
          </div>
        <div class="form-group col-md-12 mt-2">
          <label for="mobile"><i class="fas fa-phone-square"></i> Mobile No <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
            <div class="input-group mb-3 row m-0 p-0">
                <?php
                $countries = array(
                  "" => "Choose....",
                );
                  foreach ($this->COM->get_country_list() as $country) {
                    $countries[$country->country_code] = $country->country_name.' '.$country->country_code;
                    //$types[strtolower($ty->category_name)] = $ty->category_name;
                    //echo '<option value="'.base_url('search/'.strtolower($ty->category_name)).'">'.$ty->category_name.'</option>';
                  }
                  $vty_atr = ' id="country" required data-error="Country Code Required!" class="selectpicker col-4 m-0 p-0 show-tick form-control custom-select rounded-0" data-live-search="true"';
                  echo form_dropdown('country', $countries, '+91', $vty_atr);
                  ?>
              <div class="input-group-append col-8 m-0 p-0">
                <input type="number" name="mobile" id="mobile" value="<?php echo set_value('mobile');?>" required data-error="Mobile Number Required!" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Mobile No">

              </div>
            </div>
            <span class="help-block mt-1 with-errors"></span>
            <?php echo form_error('country'); ?><?php echo form_error('mobile'); ?>
        </div>
      <div class="form-group col-md-12">
        <label for="email"><i class="fas fa-envelope"></i> Email ID <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
        <span class="help-block mt-1 with-errors"></span>
        <input type="email" name="email" id="email" value="<?php echo set_value('email');?>" required data-error="Email ID Required!" class="form-control rounded-0" aria-describedby="emailHelp" placeholder="Enter Emial ID">
        <?php echo form_error('email'); ?>
      </div>
      <div class="form-group col-md-6 mt-2">
        <label for="organisation">Gender <sup><i class=" text-red fas fa-star-of-life"></i></sup></label><br />
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-secondary active">
            <input type="radio" name="gender" id="gender" value="1" required data-error="Gender Required!" autocomplete="off" checked> Male
          </label>
          <label class="btn btn-secondary">
            <input type="radio" name="gender" id="gender" value="2" required data-error="Gender Required!" autocomplete="off"> Female
          </label>
          <label class="btn btn-secondary">
            <input type="radio" name="gender" id="gender" value="3" required data-error="Gender Required!" autocomplete="off"> Other
          </label>
        </div>
        <span class="help-block mt-1 with-errors"></span>
        <?php echo form_error('gender'); ?>
      </div>

      <div class="form-group col-md-6 mt-2">
        <label for="organisation">Are You A Producer?</label><br />
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-secondary">
            <input type="radio" name="producer" id="producer" value="Yes" required data-error="Are You Producer? Required!" autocomplete="off"> Yes
          </label>
          <label class="btn btn-secondary active">
            <input type="radio" name="producer" id="producer" value="No" required data-error="Are You Producer? Required!" autocomplete="off" checked> No
          </label>
        </div>
        <span class="help-block mt-1 with-errors"></span>
        <?php echo form_error('producer'); ?>
      </div>
          <div class="form-group col-md-6 mt-2">
            <label for="user_password"><i class="fas fa-key"></i> Password  <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
            <input type="password" name="user_password" id="user_password" value="<?php echo set_value('user_password');?>" required data-error="Password Required!" class="form-control rounded-0" aria-describedby="user_passwordHelp" placeholder="Enter Password">
            <span class="help-block mt-1 with-errors"></span>
            <?php echo form_error('user_password'); ?>
          </div>
              <div class="form-group col-md-6 mt-2">
                <label for="confirm_password"><i class="fas fa-key"></i> Confirm Password  <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
                <input type="password" name="confirm_password" id="confirm_password" required data-error="Confirm Password Required!" class="form-control rounded-0" aria-describedby="confirm_passwordHelp" placeholder="Enter Confirm Password">
                <span class="help-block mt-1 with-errors"></span>
                <?php echo form_error('confirm_password'); ?>
              </div>
                  <div class="form-group col-md-12 mt-2">
                    <label for="organisation">Organisation Name</label>
                    <input type="text" name="organisation" id="organisation" value="<?php echo set_value('organisation');?>" class="form-control rounded-0" aria-describedby="eorganisationHelp" placeholder="Enter Organisation Name">
                    <span class="help-block mt-1 with-errors"></span>
                    <?php echo form_error('organisation'); ?>
                  </div>
                      <div class="form-group col-md-12 mt-2">

                        <input type="checkbox" name="terms" id="terms" required data-error="Accept Terms & Conditions" class="rounded-0" aria-describedby="termsHelp">&nbsp;&nbsp;<sup><i class=" text-red fas fa-star-of-life"></i></sup>
                        <label for="terms">I agree to the <a href="<?php echo base_url('legal/terms-conditions');?>" target="_blank">Term & Conditions</a>.</label>
                        <span class="help-block mt-1 with-errors"></span>
                        <?php echo form_error('terms'); ?>
                      </div>
                <div class="form-group col-md-12 pb-4 text-right">
                  <hr class="border-bottom-green" />
                  <button type="submit" class="btn btn-primary bg-green border-0 rounded-0">SIGN UP</button>
                </div>

      </form>
    </div>
    <div class="col-md-4 py-5 px-5 border-left">
      <h3 class="text-green font-weight-bold mb-3">Or Social Login</h3>
      <hr class="border-bottom-green" />
        <a class="btn btn-primary btn-lg bg-dark-blue border-0 <?php echo ($this->agent->is_mobile() ? "w-100": "w-75");?>" href=""><i class="fab fa-facebook-f"></i>&nbsp;&nbsp;&nbsp;Login With Facebook</a><br />
          <a class="btn btn-primary btn-lg bg-red border-0 mt-3 <?php echo ($this->agent->is_mobile() ? "w-100": "w-75");?>" href=""><i class="fab fa-google"></i>&nbsp;&nbsp;&nbsp;Login With Google</a>

    </div>

        </div>
  </div>
</section>
