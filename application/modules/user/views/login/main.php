<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// ========== social login redirect ====================

$redirectURL="";
  //include_once "include/social-login-config.inc.php";

  // == FACEBOOK LOGIN ==

    //https://www.filmboardmovies.com/social-login-callback.php?loginType=seller&redirect=https://www.filmboardmovies.com/my-inventory.php
    $fbLoginUrl = (base_url().'/social-login-callback.php?loginType=seller&redirect='.$redirectURL);
  // == FACEBOOK LOGIN ==

  // == GOOGLE PLUS LOGIN ==

    //http://www.filmboardmovies.com/google-login-callback.php?loginType=seller&redirect=http://www.filmboardmovies.com/my-inventory.php
    $google_redirect_url 	= base_url().'/google-login-callback.php?loginType=seller&redirect='.$redirectURL; //path to your script
    //$gClient->setRedirectUri($google_redirect_url);
    $googleAuthUrl = "";//$gClient->createAuthUrl();

    /* if(isset($_SESSION[SITE_NAME.'googlePlusToken'])){
      $gClient->setAccessToken($_SESSION[SITE_NAME.'googlePlusToken']);
    } */
  // == GOOGLE PLUS LOGIN ==

  // ============ linkedin ====================

    //$loginType = 'seller';
    //include_once('linkedin/test/config.php');

    //https://www.filmboardmovies.com/linkedin-login-callback.php?loginType=seller
    //https://www.filmboardmovies.com/linkedin-login-callback.php?loginType=producer

    $linkedAuthURL = "";//"https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=".$config['Client_ID']."&redirect_uri=".$config['seller_callback_url']."&state=623846780&scope=r_basicprofile r_emailaddress";

  // ============ linkedin::end ====================

?>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 p-0 mb-5 justify-content-center">
    <div class="col-md-12 col-12 m-0 p-0">
      <div class="row m-0 py-3 justify-content-center bg-red text-white">
        <div class="col-md-10 pt-5 pb-4 cap text-center">
          <h4>Show the world what you’re made of!</h4>
			       <p>Get listed on FILMBOARD to get noticed and open DOORS to great opportunities</p>
        </div>
      </div>
    </div>
    <div class="col-md-4 pb-5 px-3 border-right">
      <h3 class="text-green font-weight-bold mt-5">SIGN IN</h3>
      <hr class="border-bottom-green" />
      <?php if($this->session->flashdata('message')) {$message =$this->session->flashdata('message'); ?>
        <div class="<?php echo $message['class'] ?>" role="alert">
          <strong><?php echo $message['type'] ?>!</strong> <?php echo $message['message'] ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <script type="application/javascript">
          /** After windod Load */
          $(window).bind("load", function() {
          window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
            });
          }, 10000);
          });
          </script>
        <?php }?>
      <?php
      $form_attr = array('autocomplete' => 'off', 'data-toggle' => 'validator', 'role' => 'form');
        echo form_open(current_url(), $form_attr);
        $data_hidden = array(
          'redirect' => ($this->agent->referrer() != current_url() ? $this->agent->referrer(): base_url())
        );
        echo form_hidden($data_hidden);
      ?>
        <div class="form-group">
          <label for="exampleInputEmail1"><i class="fas fa-envelope"></i> Emial / <i class="fas fa-phone-square"></i> Mobile No <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
          <input type="text" required data-error="Email Or Mobile Number Required" class="form-control rounded-0" id="username" name="username" value="<?php echo set_value('username');?>" aria-describedby="user_name" placeholder="Enter Emial / Mobile No">
          <span class="help-block mt-1 with-errors"></span>
        </div>
          <div class="form-group mt-4">
            <label for="exampleInputEmail1"><i class="fas fa-key"></i> Password  <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
            <input type="password" required data-error="Password Required" class="form-control rounded-0" id="user_password" name="user_password" value="<?php echo set_value('user_password');?>" aria-describedby="user_password" placeholder="Enter Password">
            <span class="help-block mt-1 with-errors"></span>
          </div>
                <div class="form-group text-right">
                  <button type="submit" class="btn btn-primary bg-green border-0 rounded-0 mt-2">SIGN IN</button>
                    <button type="button" class="btn btn-primary bg-green border-0 rounded-0 mt-2">FORGOT PASSWORD ?</button>
                </div>

        <?php
        echo form_close();?>
    </div>

      <div class="col-md-4 pb-5 px-5 border-left">
        <h3 class="text-green font-weight-bold mt-5">Or Social Login</h3>
        <hr class="border-bottom-green" />
          <a class="btn btn-primary btn-lg bg-dark-blue border-0 <?php echo ($this->agent->is_mobile() ? "w-100": "w-75");?>" href=""><i class="fab fa-facebook-f"></i>&nbsp;&nbsp;&nbsp;Login With Facebook</a><br />
            <a class="btn btn-primary btn-lg bg-red border-0 mt-3 <?php echo ($this->agent->is_mobile() ? "w-100": "w-7");?>" href=""><i class="fab fa-google"></i>&nbsp;&nbsp;&nbsp;Login With Google</a>

      </div>
    </div>
  </div>
</section>
