<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products_model
 *
 * @author shiv
 */
class Login_models extends CI_Model {
protected $data = array();
    public function __construct() {
        parent::__construct();
    }

    function check(){
      $username = $this->input->post('username');
      $user_password = $this->input->post('user_password');
      $this->db->where('email', $username);
      $this->db->or_where('mobile', $username);
      $query = $this->db->get('fb_user');
      if($query->num_rows()>0){
            $result = $query->row();
            //if($user_password == $this->encryption->decrypt($result->fldt_user_password)){
            if(password_verify($user_password, $result->password) || $this->encryption->decrypt($result->password) == $this->input->post('user_password')){
                /*if($result->fldti_is_active && $result->fldti_is_approved){
                    $data = array(
                        'status' => 'success',
                        'logged_in_user' => $result
                    );
                    return $data;
                }else{*/
                $this->data = array(
                    'status' => 'success',
                    'logged_in_user' => $result
                );
                  /*
                    $data = array(
                        'status' => 'error',
                        'message' => 'Your Account Is Not Active.<br />Please Contact Your Admin'
                    );
                    $messge = array(
                        'message' => 'Your Account Is Not Active.<br />Please Contact Your Admin',
                        'type' => "Error: ",
                        'class' => 'alert alert-danger alert-dismissible fade show'
                    );*/
                    //$this->session->set_flashdata('message', $messge);
                    return $this->data;
                //}
            }else{
                $this->data = array(
                    'status' => 'error',
                    'message' => 'Invalid Login Details'
                );
                $messge = array(
                    'message' => 'Invalid Login Details',
                    'type' => "Error: ",
                    'class' => 'alert alert-danger alert-dismissible fade show'
                );
                $this->session->set_flashdata('message', $messge);
                return $data;
            }
        }else{
          $this->data = array(
              'status' => 'error',
              'message' => 'User Not Found!'
          );
            $messge = array(
                'message' => 'User Not Found!',
                'type' => "Error: ",
                'class' => 'alert alert-danger alert-dismissible fade show'
            );
            $this->session->set_flashdata('message', $messge);
            return $this->data;
        }
    }

    function user_type_count($type,$type_ser){
      $this->db->select('profile_no');
      //$this->db->where('seller_type', $type);
      $this->db->like('profile_no', $type_ser);
      $this->db->order_by('id', 'desc');
      $query = $this->db->get('fb_user');
      //print_r($query->row()->profile_no);
      //echo $this->db->last_query();
      $pr = explode('/',$query->row()->profile_no);
      //print_r($pr);
      return str_pad(($pr[4]+1),8,"0", STR_PAD_LEFT);
    }
}
