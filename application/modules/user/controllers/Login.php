<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Frontend {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
     protected $data = array();
     protected $page ="User Sign In";
    	protected $module_name = "Login";
    	protected $module_sub_name = "User";
     function __construct(){
       parent::__construct();
       $CI =& get_instance();

  		$this->load->library('encryption');
  		$this->load->model('Login_models', 'LM');
     }

	public function index()
	{
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
      if ($this->form_validation->run('login') == FALSE)
  				{
  					$messge = array(
  						'message' => validation_errors(),
  						'type' => "Warning",
  						'class' => 'alert alert-warning alert-dismissible fade show'
  					);
  					$this->session->set_flashdata('message', $messge);
  					redirect(current_url());
  				}else{
            $result = array();

      			$result = $this->LM->check();
      			if($result['status'] == 'success'){
      				$log_data = array(
      					'is_logged_in' => TRUE,
      					'is_type' => $result['logged_in_user']->user_type,
      					'user_roles_id' => $result['logged_in_user']->user_role,
      					'user_roles_name' => $result['logged_in_user']->user_type,
      					'logged_data' => $result['logged_in_user'],
                'os' => $this->agent->platform()
      				);
      				$this->session->set_userdata($log_data);
              if($this->input->post('redirect') != "")
              {
                redirect($this->input->post('redirect'));
              }else{
                redirect(base_url('user/dashboard'));
              }
      			}else{
      				$this->data['page'] = $this->module_sub_name.' : '.$this->module_name;
              $this->data['title'] = $this->module_name.' : '.$this->module_sub_name.' :: '.APP_NAME;
      				$view = array(
      					'login/main'
      				);
      				$this->template($view, $this->data, TRUE);
      			}
          }
    }else{
      $this->data['page'] = $this->page = $this->module_sub_name.' : '.$this->module_name;
      $this->data['title'] = $this->module_sub_name.' : '.$this->module_name.' :: '.APP_NAME;
      $view = array(
        'login/main'
      );
      $this->template($view, $this->data, true);
    }

	}
  	public function sign_up()
  	{
      if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        if ($this->form_validation->run('register') == FALSE)
    				{
    					$messge = array(
    						'message' => validation_errors(),
    						'type' => "Warning",
    						'class' => 'alert alert-warning alert-dismissible fade show'
    					);
    					$this->session->set_flashdata('message', $messge);
    					redirect(current_url());
    				}else{
              print_r($_POST);
              /*
              if ($userType == 'seller') {
            $prefix = 'SE/FB/';
            $isAccountBlocked = '1';
        } else {
            $prefix = 'PR/FB/';
            $isAccountBlocked = '0';
        }$query = "insert into " . PREFIX . "user
        (user_permalink, first_name, last_name, country_code,
        mobile, is_mobile_verified, email, is_email_verified,
        gender, password, company_name, app_login_token, profile_no,
        seller_type, user_type, email_verification_token, otp, is_account_blocked,
        referred_by) values ('" . $user_permalink . "', '" . $first_name . "', '" . $last_name . "', '" . $country_code . "', '" . $mobile . "', '0', '" . $email . "', '0', " . $gender . ", '" . $passwordHASH . "','" . $orgname . "', " . $appLoginToken . ", '" . $userNo . "', '" . $seller_type . "', '" . $userType . "', '" . $emailVerificationToken . "', '" . $newOTP . "', '" . $isAccountBlocked . "', $referral)";
      4491+977+2376
              */
              $profile_no = $this->LM->user_type_count(($this->input->post('producer')=="No" ? 'Individual':'Company'),($this->input->post('producer')=="No" ? 'SE/FB':'PR/FB'));
              $data_regsiter = array(
                "user_permalink" => uniqid(),
                  "first_name" => $this->security->xss_clean($this->input->post('first_name')),
                    "last_name" => $this->security->xss_clean($this->input->post('last_name')),
                      "country_code" => $this->security->xss_clean($this->input->post('country')),
                        "mobile" => $this->security->xss_clean($this->input->post('mobile')),
                          "is_mobile_verified" => 0,
                            "email" => $this->security->xss_clean($this->input->post('email')),
                              "is_email_verified" => 0,
                                "gender" => $this->security->xss_clean($this->input->post('gender')),
                                  "password" => $this->encryption->encrypt($this->security->xss_clean($this->input->post('user_password'))),
                                    "company_name" => $this->security->xss_clean($this->input->post('organisation')),
                                        "profile_no" => ($this->input->post('producer')=="No" ? 'SE/FB/'.date('Y/m/').$profile_no:'PR/FB/'.date('Y/m/').$profile_no),
                                          "seller_type" => ($this->input->post('producer')=="No" ? 'Individual':'Company'),
                                            "user_type" => ($this->input->post('producer')=="No" ? 'Seller':'Producer'),
                                              "email_verification_token" => md5(time().$this->security->xss_clean($this->input->post('email'))),
                                                  "is_account_blocked" => 1
              );
              //print_r($data_regsiter);
              //exit();
              $result = $this->db->insert('fb_user', $data_regsiter);
              if($result){
                $messge = array(
      						'message' => "Account Created Successfully!<br/>Please Login To Continue",
      						'type' => "Success",
      						'class' => 'alert alert-success alert-dismissible fade show'
      					);
      					$this->session->set_flashdata('message', $messge);
      					redirect(base_url('user/sign-in'));
              }else{
                $messge = array(
      						'message' => "Sorry! Someting Went Wrong. Try Again.",
      						'type' => "Warning",
      						'class' => 'alert alert-warning alert-dismissible fade show'
      					);
      					$this->session->set_flashdata('message', $messge);
      					redirect(current_url());
              }
            }
      }else{
          $this->module_sub_name = "User Registration";
          if($this->session->userdata('is_logged_in')){
            redirect(base_url());
          }
          $this->data['page'] = $this->page = $this->module_sub_name.' : '.$this->module_name;
          $this->data['title'] = $this->module_sub_name.' : '.$this->module_name.' :: '.APP_NAME;
      		$view = array(
            'login/register'
      		);
      		$this->template($view, $this->data, true);
        }
  	}


	function logout(){
		$log_data = array(
			'is_logged_in' => FALSE,
			'is_type' => '',
			'logged_data' => ''
		);
		$this->session->set_userdata($log_data);
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
