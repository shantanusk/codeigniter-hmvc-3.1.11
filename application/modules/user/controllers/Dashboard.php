<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends User_backend {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
     protected $page ="User Sign In";
    	protected $module_name = "Login";
    	protected $module_sub_name = "User";
     function __construct(){
       parent::__construct();
       $CI =& get_instance();

  		$this->load->library('encryption');
     }

	public function index()
	{
      $this->data['page'] = $this->page = $this->module_sub_name.' : '.$this->module_name;
      $this->data['title'] = $this->module_sub_name.' : '.$this->module_name.' :: '.APP_NAME;
      $view = array(
        'dashboard/main'
      );
      $this->template($view, $this->data, true);

	}
}
