<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends Frontend {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
     protected $page ="Home";
     function __construct(){
       parent::__construct();
       $CI =& get_instance();
     }

	public function index()
	{
    $this->data['page'] = $this->page = "Home";
    $this->data['banner'] = $this->COM->get_banner();
    $this->data['type'] = $this->COM->get_active_type();
		$view = array(
      'welcome/main'
		);
		$this->template($view, $this->data, true);
	}
  	public function about_us()
  	{
      $this->data['page'] = $this->page = "About Us";
      $this->data['before'] = $this->COM->before_filmboard_content();
      $this->data['after'] = $this->COM->after_filmboard_content();
      //print_r($this->data['after']);
      //$this->data['type'] = $this->COM->get_active_type();
  		$view = array(
        'welcome/about-us'
  		);
  		$this->template($view, $this->data, true);
  	}
    	public function fblp()
    	{
        $this->data['page'] = $this->page = "FILMBOARD LINE PRODUCTION";
        //print_r($this->data['after']);
        //$this->data['type'] = $this->COM->get_active_type();
    		$view = array(
          'welcome/filmboard-line-production'
    		);
    		$this->template($view, $this->data, true);
    	}
      	public function contact_us()
      	{
          $this->load->helper('captcha');
          if($this->input->post('submit')){
            $inputCaptcha = $this->input->post('captcha');
            $sessCaptcha = $this->session->userdata('captchaCode');
            if($inputCaptcha === $sessCaptcha){
                echo 'Captcha code matched.';
            }else{
                echo 'Captcha code does not match, please try again.';
            }
        }
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'/assets/captcha_images/',
            'font_path'     => 'system/fonts/texb.ttf',
            'img_width'     => '160',
            'img_height'    => 50,
            'word_length'   => 4,
            'font_size'     => 18,
            'colors' => array(
              'background' => array(255, 255, 255),
              'border' => array(0, 0, 0),
              'text' => array(0, 0, 0),
              'grid' => array(255, 40, 40)
            )
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and set new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode', $captcha['word']);

        // Pass captcha image to view
        $this->data['captchaImg'] = $captcha['image'];

          $this->data['page'] = $this->page = "Contact Us";
            $this->data['contact'] = $this->COM->contact();
      		$view = array(
            'welcome/contact-us'
      		);
      		$this->template($view, $this->data, true);
      	}
        public function refresh(){
          $this->load->helper('captcha');
          $config = array(
              'img_path'      => 'assets/captcha_images/',
              'img_url'       => base_url().'/assets/captcha_images/',
              'font_path'     => 'system/fonts/texb.ttf',
              'img_width'     => '160',
              'img_height'    => 50,
              'word_length'   => 4,
              'font_size'     => 18,
              'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(0, 0, 0),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
              )
          );
          $captcha = create_captcha($config);

          // Unset previous captcha and set new captcha word
          $this->session->unset_userdata('captchaCode');
          $this->session->set_userdata('captchaCode',$captcha['word']);

          // Display captcha image
          echo $captcha['image'];
      }


      	public function media()
      	{
          $this->data['page'] = $this->page = "News & Media";
          //print_r($this->data['after']);
          $this->data['media'] = $this->COM->media();
          $this->data['news'] = $this->COM->news();
      		$view = array(
            'welcome/news-and-media'
      		);
      		$this->template($view, $this->data, true);
      	}
      	public function founders()
      	{
          $this->data['page'] = $this->page = "FILMBOARD Founders";
          //print_r($this->data['after']);
          $this->data['founder'] = $this->COM->founder();
      		$view = array(
            'welcome/founders'
      		);
      		$this->template($view, $this->data, true);
      	}
      	public function strategic_advisors()
      	{
          $this->data['page'] = $this->page = "FILMBOARD Strategic Advisors";
          //print_r($this->data['after']);
          $this->data['strategic_advisors'] = $this->COM->strategic_advisors();
      		$view = array(
            'welcome/strategic-advisors'
      		);
      		$this->template($view, $this->data, true);
      	}
      	public function career()
      	{
          $this->data['career'] = $this->COM->career();
          $this->data['page'] = $this->page = "Careers";
          $this->data['title'] = $this->data['career']->meta_title;
          $this->data['keywords'] = $this->data['career']->meta_keywords;
          $this->data['descriptions'] = $this->data['career']->meta_description;
          $this->data['vacancy'] = $this->COM->vacancy();

      		$view = array(
            'welcome/career'
      		);
      		$this->template($view, $this->data, true);
      	}
      	public function jobs()
      	{
          $this->data['jobs'] = $this->COM->all_jobs();
          $this->data['page'] = $this->page = "Jobs";

      		$view = array(
            'welcome/jobs'
      		);
      		$this->template($view, $this->data, true);
      	}
    	public function show_404()
    	{
        $this->data['page'] = $this->page = "Page Not Found";
        $this->data['banner'] = $this->COM->get_banner();
        $this->data['type'] = $this->COM->get_active_type();
    		$view = array(
          'welcome/show-404'
    		);
    		$this->template($view, $this->data, true);
    	}
}
