<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Frontend {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//
    public $CI;
    protected $data = array();
    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
     protected $page ="List";
     function __construct(){
       parent::__construct();
       $CI =& get_instance();
     }

     function index(){
       if($this->uri->segment(1, 0) && $this->uri->segment(1) == "search"){
         $type = $category = $category_sub = "";
         $this->data['type'] = $this->data['category'] = $this->data['category_sub'] = array();

         $depth=2;
         $pcount=2;
         $this->data["base_url"] = base_url("search");
         if(($this->uri->segment(1, 0) && $this->uri->segment(2,0)) && (!$this->uri->segment(3,0) || is_numeric($this->uri->segment(3)))){
           $type = ucwords(str_replace("-", " ",$this->uri->segment(2)));
           $this->data['type_id'] = $this->COM->get_type_id_by_name($type);
           $this->data['category'] = $this->COM->get_active_category_by_id($this->data['type_id']);
           $this->data["base_url"] = base_url("search/".$this->uri->segment(2));
           $pcount++;

           if(!empty($this->uri->segment($pcount))){
             $offset = (int)$this->uri->segment($pcount)-1;
             $offset = (($offset*$this->pagination->per_page));
           }else{
             $offset = 0;
           }
           $this->data['users'] = $this->SRCH->search_list($type, $category, $category_sub,"Yes", $this->pagination->per_page, $offset);
           //echo $this->db->last_query();

           $this->data['users_count'] = $this->SRCH->search_list($type, $category, $category_sub,"No", $this->pagination->per_page, $offset);
         }
         if($this->uri->segment(2,0) && $this->uri->segment(3,0) && !is_numeric($this->uri->segment(3)) &&  (!$this->uri->segment(4,0) || is_numeric($this->uri->segment(4)))){

             $type = ucwords(str_replace("-", " ",$this->uri->segment(2)));
             $this->data['type_id'] = $this->COM->get_type_id_by_name($type);
             $this->data['category'] = $this->COM->get_active_category_by_id($this->data['type_id']);
             $category = ucwords(str_replace("-", " ",$this->uri->segment(3)));
             //echo str_replace(" ", "%",$category);
             $this->data['category_id'] = $this->COM->get_category_id_by_permalink();
             //echo $this->db->last_query();
             $this->data['category_sub'] = $this->COM->get_active_sub_category_by_id($this->data['category_id']);
             $this->data["base_url"] = base_url("search/".$this->uri->segment(2)."/".$this->uri->segment(3));
             $pcount= $pcount+2;

             if(!empty($this->uri->segment($pcount))){
               $offset = (int)$this->uri->segment($pcount)-1;
               $offset = (($offset*$this->pagination->per_page));
             }else{
               $offset = 0;
             }
             $this->data['users'] = $this->SRCH->search_list($type, $category, $category_sub,"Yes", $this->pagination->per_page, $offset);
             //echo $this->db->last_query();

             $this->data['users_count'] = $this->SRCH->search_list($type, $category, $category_sub,"No", $this->pagination->per_page, $offset);
         }
         if($this->uri->segment(2,0) && $this->uri->segment(3,0) && $this->uri->segment(4,0) && !is_numeric($this->uri->segment(4))){

             $type = ucwords(str_replace("-", " ",$this->uri->segment(2)));
             $this->data['type_id'] = $this->COM->get_type_id_by_name($type);
             $this->data['category'] = $this->COM->get_active_category_by_id($this->data['type_id']);
             $category = ucwords(str_replace("-", " ",$this->uri->segment(3)));
             $this->data['category_id'] = $this->COM->get_category_id_by_permalink();
             $this->data['category_sub'] = $this->COM->get_active_sub_category();
             $category_sub = ucwords(str_replace("-", " ",$this->uri->segment(4)));
             $this->data['category_sub_id'] = $this->COM->get_category_sub_id_by_permalink(); //$this->COM->get_category_id_by_name($category);
             $this->data["base_url"] = base_url("search/".$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4));
             $pcount= $pcount+4;

             if(!empty($this->uri->segment($pcount))){
               $offset = (int)$this->uri->segment($pcount)-1;
               $offset = (($offset*$this->pagination->per_page));
             }else{
               $offset = 0;
             }
             $this->data['users'] = $this->SRCH->search_list($type, $category, $category_sub,"Yes", $this->pagination->per_page, $offset);
             echo $this->db->last_query();

             $this->data['users_count'] = $this->SRCH->search_list($type, $category, $category_sub,"No", $this->pagination->per_page, $offset);
         }

         $this->data['offset'] = $offset;
         $config['total_rows'] = count($this->data['users_count']);
         $this->data['per_page'] = $this->pagination->per_page;
         $config['base_url'] =   $this->data["base_url"];
         $this->data['total_rows'] = $config['total_rows'];
         $config['reuse_query_string'] = TRUE;
         $this->pagination->initialize($config);
         $this->data["links"] = $this->pagination->create_links();
         $this->pagination_summary();
         /*print_r($this->COM->get_user_profile_search($type, $category, $category_sub, $depth=2));
         echo count($this->COM->get_user_profile_search("Talent", $category = "", $category_sub = "", $depth=2));
         echo $this->db->last_query();*/
         $this->data['page'] = $this->page = "Search";
         $this->data['type'] = $this->COM->get_active_type();
         $this->data['city'] = $this->COM->get_city();
         $view = array(
           'search/main'
         );
         $this->template($view, $this->data, true);

       }

     }
          public function index2(){
            $type = $category = $category_sub = "";
            $depth=2;
            $pcount=2;
            $this->data['type'] = $this->data['category'] = $this->data['category_sub'] = array();
            $this->data["base_url"] = base_url("search");
            if($this->uri->segment(2, 0) && !is_numeric($this->uri->segment(2)) || is_numeric($this->uri->segment(3))){
              $type = ucwords(str_replace("-", " ",$this->uri->segment(2)));
              $this->data['type_id'] = $this->COM->get_type_id_by_name($type);
              $this->data['category'] = $this->COM->get_active_category_by_id($this->data['type_id']);
              //$this->data["active_type"] = $this->uri->segment(2);
              $this->data["base_url"] = base_url("search/".$this->uri->segment(2));
              $pcount++;
            }
            if($this->uri->segment(3, 0) && !is_numeric($this->uri->segment(3))){
              $category = ucwords(str_replace("-", " ",$this->uri->segment(3)));
              $this->data['category_id'] = $this->COM->get_category_id_by_name($category);
              $this->data['category_sub'] = $this->COM->get_active_sub_category();
              //$this->data["active_category"] = $this->uri->segment(2);
              $this->data["base_url"] = base_url("search/".$this->uri->segment(2).'/'.$this->uri->segment(3));
              $pcount++;
            }
            if($this->uri->segment(4, 0) && !is_numeric($this->uri->segment(4))){
              $category_sub = ucwords(str_replace("-", " ",$this->uri->segment(4)));
              $this->data['category_sub_id'] = $this->COM->get_category_id_by_name($category);
              //$this->data["active_category_sub"] = $this->uri->segment(3);
              $this->data["base_url"] = base_url("search/".$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4));
              $pcount++;
            }
            $config['base_url'] =   $this->data["base_url"];
            if(!empty($this->uri->segment($pcount))){
              $offset = (int)$this->uri->segment($pcount)-1;
              $offset = (($offset*$this->pagination->per_page));
            }else{
              $offset = 0;
            }

            if($this->uri->segment(1, 0) && !$this->uri->segment(2, 0) && !is_numeric($this->uri->segment(2)) && !$this->uri->segment(3, 0) && !is_numeric($this->uri->segment(3)) && !$this->uri->segment(4, 0) && !is_numeric($this->uri->segment(4))){
              $this->data['users'] = $this->COM->get_user_profile_search($type, $category="", $category_sub="", $depth=4,$this->pagination->per_page, $offset);


              $this->data['users_count'] = $this->COM->get_user_profile_search_count($type, $category, $category_sub, $depth=4,$this->pagination->per_page, $offset);
              //print_r($this->data['users']);
            }else if($this->uri->segment(1, 0) && $this->uri->segment(2, 0) && is_numeric($this->uri->segment(3))){

              $this->data['users'] = $this->COM->get_user_profile_search($type, $category, $category_sub, $depth=4,$this->pagination->per_page, $offset);

              $this->data['users_count'] = $this->COM->get_user_profile_search_count($type, $category, $category_sub, $depth=4,$this->pagination->per_page, $offset);

            }else if($this->uri->segment(1, 0) && $this->uri->segment(2, 0) && $this->uri->segment(3, 0) && is_numeric($this->uri->segment(4))){
              $this->data['users'] = $this->COM->get_user_profile_search($type, $category, $category_sub, $depth=4,$this->pagination->per_page, $offset);

              $this->data['users_count'] = $this->COM->get_user_profile_search_count($type, $category, $category_sub, $depth=4,$this->pagination->per_page, $offset);
            }else{
                $this->data['users'] = $this->COM->get_user_profile_search($type, $category, $category_sub, $depth=4,$this->pagination->per_page, $offset);
                $this->data['users_count'] = $this->COM->get_user_profile_search_count($type, $category, $category_sub, $depth=4,$this->pagination->per_page, $offset);
            }
            echo $this->db->last_query();
            echo count($this->data['users_count']);
            exit();
            if($type == ""){
                $this->data['offset'] = round($offset/4);
                $config['total_rows'] = round(count($this->data['users_count'])/4);
                $this->data['per_page'] = 3;
            }else{
              $this->data['offset'] = $offset;
              $config['total_rows'] = count($this->data['users_count']);
              $this->data['per_page'] = $this->pagination->per_page;
            }

            $this->data['total_rows'] = $config['total_rows'];
            $config['reuse_query_string'] = TRUE;
            $this->pagination->initialize($config);
            $this->data["links"] = $this->pagination->create_links();
            $this->pagination_summary();
            /*print_r($this->COM->get_user_profile_search($type, $category, $category_sub, $depth=2));
            echo count($this->COM->get_user_profile_search("Talent", $category = "", $category_sub = "", $depth=2));
            echo $this->db->last_query();*/
            $this->data['page'] = $this->page = "Search";
            $this->data['type'] = $this->COM->get_active_type();
            $this->data['city'] = $this->COM->get_city();
            $view = array(
              'search/main'
            );
            $this->template($view, $this->data, true);
          }

    public function details($id, $profile_id, $type_id, $category_id, $category_sub_id)
    {

      $this->data['user'] = $user = $this->SRCH->get_user_by_id($id);
      if($type_id==3){
        $this->data['user_profile'] = $user_profile = $this->SRCH->get_user_profile_services_by_user_id($id, $profile_id, $type_id, $category_id, $category_sub_id);
      }else if($type_id==4){
        $this->data['user_profile'] = $user_profile = $this->SRCH->get_user_profile_locations_by_user_id($id, $profile_id, $type_id, $category_id, $category_sub_id);
      }else{
          $this->data['user_profile'] = $user_profile = $this->SRCH->get_user_profile_by_user_id($id, $profile_id, $type_id, $category_id, $category_sub_id);
      }
/*
      echo $this->db->last_query();
      print_r($user_profile);
      exit();*/
      if($type_id==3){
        //if(!$category_sub_id==0){
          $this->data['title'] = ucwords(strtolower($user_profile->first_name.' '.$user_profile->last_name.' : '.$this->COM->get_category_sub_name_by_id($category_sub_id).' : '.$user_profile->category.' : '.$user_profile->types).' :: '.APP_NAME);
          $this->data['keywords'] = ucwords(strtolower($user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name.', '.$this->COM->get_category_sub_name_by_id($usr->category_sub_id).', '.$user_profile->category.', '.$user_profile->types.', Filmboard Movies'));
        /*}else{
          $this->data['title'] = ucwords(strtolower($user_profile->first_name.' '.$user_profile->last_name.' : '.$user_profile->category.' : '.$user_profile->types).' :: '.APP_NAME);
          $this->data['keywords'] = ucwords(strtolower($user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name.', '.$user_profile->category.', '.$user_profile->types.', Filmboard Movies'));
        }*/
      }else if($type_id==4){
        if($category_sub_id!==0){

          $this->data['title'] = ucwords(strtolower($user_profile->proeprty_name.' By '.$user_profile->first_name.' '.$user_profile->last_name.' : '.$this->COM->get_category_sub_name_by_id($usr->category_sub_id).' : '.$user_profile->category.' : '.$user_profile->types).' :: '.APP_NAME);
          $this->data['keywords'] = ucwords(strtolower($user_profile->proeprty_name.' By '.$user_profile->proeprty_name.' '.$user_profile->first_name.' '.$user_profile->last_name.', '.$this->COM->get_category_sub_name_by_id($usr->category_sub_id).', '.$user_profile->category.', '.$user_profile->types.', Filmboard Movies'));
        }else{
          $this->data['title'] = ucwords(strtolower($user_profile->proeprty_name.' By '.$user_profile->first_name.' '.$user_profile->last_name.' : '.$user_profile->category.' : '.$user_profile->types).' :: '.APP_NAME);
          $this->data['keywords'] = ucwords(strtolower($user_profile->proeprty_name.' By '.$user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name.', '.$user_profile->category.', '.$user_profile->types.', Filmboard Movies'));
        }
      }else{
        $this->data['user_languages'] = $this->SRCH->get_user_languages_by_user_id($id, $profile_id);
        $this->data['user_interest'] = $this->SRCH->get_user_interest_by_user_id($id, $profile_id);
        $this->data['user_genres'] = $this->SRCH->get_user_genres_by_user_id($id, $profile_id);
        $this->data['user_worked_for'] = $this->SRCH->get_user_worked_for_by_user_id($id, $profile_id);
        $this->data['user_roles_played'] = $this->SRCH->get_user_roles_played_by_user_id($id, $profile_id);
        $this->data['user_service_locations'] = $this->SRCH->get_user_service_locations_by_user_id($id, $profile_id);
        $this->data['user_audio_links'] = $this->SRCH->get_user_audio_links_by_user_id($id, $profile_id);
        $this->data['user_video_links'] = $this->SRCH->get_user_video_links_by_user_id($id, $profile_id);
        $this->data['user_photos'] = $this->SRCH->get_user_photos_by_user_id($id, $profile_id);

        if($type_id!==0){
          $this->data['title'] = ucwords(strtolower($user_profile->first_name.' '.$user_profile->last_name.' : '.$user_profile->category.' : '.$user_profile->types).' :: '.APP_NAME);

          $this->data['keywords'] = ucwords(strtolower($user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name.', '.@$user_profile->category_sub.', '.$user_profile->category.', '.$user_profile->types.', Filmboard Movies'));
        }else{
          $this->data['title'] = ucwords(strtolower($user_profile->first_name.' '.$user_profile->last_name.' : '.$user_profile->category.' : '.$user_profile->types).' :: '.APP_NAME);
          $this->data['keywords'] = ucwords(strtolower($user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name.', '.$user_profile->category.', '.$user_profile->types.', Filmboard Movies'));
        }
      }
      $this->data['page'] = $this->page = "User Detail Page";

      $this->data['descriptions'] = ucwords(strtolower('Search '.$user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name));
      if($type_id==3){
        $view = array(
          'search/services-details'
        );
        $this->template($view, $this->data, true);
      }else if($type_id==4){
        $view = array(
          'search/location-details'
        );
        $this->template($view, $this->data, true);
      }else{
        $view = array(
          'search/details'
        );
        $this->template($view, $this->data, true);
      }

    }


        function get_test(){
          $this->db->select('*');
          $this->db->from('fb_user a');
          $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
          $this->db->join('fb_user_category e','e.category_id=b.category_id and b.user_id=e.user_id', 'LEFT');
          $this->db->join('fb_category_master d','e.mainCategory=d.id', 'LEFT');
          $this->db->join('fb_sub_category_master c','c.id=e.subCategory', 'LEFT');
          $this->db->where('a.is_account_blocked', '0');
          $this->db->where('a.is_deleted', '0');
          //$this->db->where('b.category_type', 'subCatid');
          $this->db->where_not_in('a.profile_image', '');
          $this->db->limit(10);
          $query = $this->db->get();

          echo count($query->result());
          echo "<pre>";
          print_r($query->result());
          echo $this->db->last_query();
        }
}

/*
$config = array();
			$config["base_url"] = base_url() . "admin/common/account/index";
			if(!empty($this->uri->segment(5))){
				$offset = (int)$this->uri->segment(5)-1;
				$offset = (($offset*$this->pagination->per_page));
			}else{
				$offset = 0;
			}
			$this->data['offset'] = $offset;
			$config['total_rows'] = $this->COM->get_count("common_account_type");
			$this->data['total_rows'] = $config['total_rows'];
			$this->data['per_page'] = $this->pagination->per_page;
			$this->pagination->initialize($config);

			$this->data["links"] = $this->pagination->create_links();

			$this->data['account'] = $this->COM->get_pagination_list("common_account_type", $this->pagination->per_page, $offset);
			*/
