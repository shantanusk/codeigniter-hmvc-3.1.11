<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends Frontend {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
     protected $page ="Jobs";
     function __construct(){
       parent::__construct();
       $CI =& get_instance();
     }

    public function index()
    {

        $type = $category = $category_sub = "";
        $depth=2;
        $pcount=2;
        $this->data['type'] = $this->data['category'] = $this->data['category_sub'] = array();
        $this->data["base_url"] = base_url("requirements");
        if($this->uri->segment(2, 0) && !is_numeric($this->uri->segment(2)) || is_numeric($this->uri->segment(3))){
          $type = ucwords(str_replace("-", " ",$this->uri->segment(2)));
          $this->data['type_id'] = $this->COM->get_type_id_by_name($type);
          $this->data['category'] = $this->COM->get_active_category_by_id($this->data['type_id']);
          //$this->data["active_type"] = $this->uri->segment(2);

          $this->data["base_url"] = base_url("requirements/".$this->uri->segment(2));
          $pcount++;
        }
        if($this->uri->segment(3, 0) && !is_numeric($this->uri->segment(3))){
          $category = ucwords(str_replace("-", " ",$this->uri->segment(3)));
          $this->data['category_id'] = $this->COM->get_category_id_by_name($category);
          $this->data['category_sub'] = $this->COM->get_active_sub_category();
          //$this->data["active_category"] = $this->uri->segment(2);
          $this->data["base_url"] = base_url("requirements/".$this->uri->segment(2).'/'.$this->uri->segment(3));
          $pcount++;
        }
        if($this->uri->segment(4, 0) && !is_numeric($this->uri->segment(4))){
          $category_sub = ucwords(str_replace("-", " ",$this->uri->segment(4)));
          $this->data['category_sub_id'] = $this->COM->get_category_id_by_name($category);
          //$this->data["active_category_sub"] = $this->uri->segment(3);
          $this->data["base_url"] = base_url("requirements/".$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4));
          $pcount++;
        }
        $config['base_url'] =   $this->data["base_url"];
        if(!empty($this->uri->segment($pcount))){
          $offset = (int)$this->uri->segment($pcount)-1;
          $offset = (($offset*$this->pagination->per_page));
        }else{
          $offset = 0;
        }

        /*if($this->uri->segment(1, 0) && !$this->uri->segment(2, 0) && !is_numeric($this->uri->segment(2)) && !$this->uri->segment(3, 0) && !is_numeric($this->uri->segment(3)) && !$this->uri->segment(4, 0) && !is_numeric($this->uri->segment(4))){
          $this->data['jobs'] = $this->JOB->get_requirement_search($this->pagination->per_page, $offset);
          //echo $this->db->last_query();
          $this->data['job_count'] = $this->JOB->get_requirement_search_count($this->pagination->per_page, $offset);
          //print_r($this->data['users']);
        }else if($this->uri->segment(1, 0) && $this->uri->segment(2, 0) && is_numeric($this->uri->segment(2))){

          $this->data['jobs'] = $this->JOB->get_requirement_search($this->pagination->per_page, $offset);
          $this->data['job_count'] = $this->JOB->get_requirement_search_count($this->pagination->per_page, $offset);

        }else if($this->uri->segment(1, 0) && $this->uri->segment(2, 0) && $this->uri->segment(3, 0) && !is_numeric($this->uri->segment(3))){

          $this->data['jobs'] = $this->JOB->get_requirement_search($this->pagination->per_page, $offset);

          $this->data['job_count'] = $this->JOB->get_requirement_search_count($this->pagination->per_page, $offset);
        }else{*/
            $this->data['jobs'] = $this->JOB->get_requirement_search($this->pagination->per_page, $offset);

            $this->data['job_count'] = $this->JOB->get_requirement_search_count();
        //}

        if($type == ""){
            $this->data['offset'] = round($offset);
            $config['total_rows'] = round(count($this->data['job_count']));
            $this->data['per_page'] = 3;
        }else{
          $this->data['offset'] = $offset;
          $config['total_rows'] = count($this->data['job_count']);
          $this->data['per_page'] = $this->pagination->per_page;
        }

        $this->data['total_rows'] = $config['total_rows'];
        $config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $this->pagination_summary();
      $this->data['page'] = $this->page = "Jobs";
      $this->data['type'] = $this->COM->get_active_type();
      $this->data['city'] = $this->COM->get_city();

      $view = array(
        'jobs/list'
      );
      $this->template($view, $this->data, true);
    }
    public function details($job_id, $type, $category, $category_sub)
    {
      $this->data['jobs'] = $this->JOB->get_requirement_details();
      $this->data['job_bids'] = $this->COM->get_bids_by_id($job_id);
      $this->data['type'] = $this->COM->get_active_type();
      $this->data['page'] = $this->page = "Jobs";
      $title= 'Require '.$this->data['jobs']->no_of_openings_qty.' '.(($this->data['jobs']->gender_type !="") ? $this->data['jobs']->gender_type.' '.$this->data['jobs']->category : $this->data['jobs']->category_sub).' For '.$this->data['jobs']->project_type;
      $this->data['title'] = ucwords($title.' | '.ucwords(str_replace("-", " ",$this->data['jobs']->types.' | '.$this->data['jobs']->category.(($this->data['jobs']->category_sub == 'na') ? '' : ' | '.$this->data['jobs']->category_sub))));
      $this->data['keywords'] = ucwords($title.', '.$this->data['jobs']->types.', '.$this->data['jobs']->category.(($this->data['jobs']->category_sub == 'na') ? '' : ', '.$this->data['jobs']->category_sub));
      $this->data['descriptions'] = ucwords($this->data['jobs']->project_description);
      $view = array(
        'jobs/details'
      );
      $this->template($view, $this->data, true);
    }
}
