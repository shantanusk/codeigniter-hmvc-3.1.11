<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
    <title>Hello, world!</title>
    <?php if($is_logged_in){?>
      <style>
        body{
          margin-top:7.5rem;
        }
      </style>
      <?php }else{?>
        <style>
          body{
            margin-top:5.2rem;
          }
        </style>
      <?php }?>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/js/all.min.js"></script>
  </head>
  <body>
    <div class="container-fluid m-0 p-0">
      <header>
        <div class="row m-0 p-0">
            <div class="col-12">
              <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">
                  <img style="width:15rem;" src="<?php echo base_url('assets/uploads/logo/logo.png');?>" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">WHAT IS FILMBOARD ?</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-center" href="#">FILMBOARD LINE PRODUCTION<br />(FBLP)</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#" target="_blank">BLOG</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">FAQ's</a>
                    </li>
                    <li class="nav-item text-center">
                      <a class="nav-link" href="#">Contact Us</a>
                      <a target="_blank" href="https://wa.me/+919326257428/?text=Hi there..!! I am" style="display: inline;padding: 3px!important;"><img src="https://cdn2.iconfinder.com/data/icons/social-icons-33/128/WhatsApp-512.png" width="25px" height="25px"></a>
                      <a href="tel:+919326257428" style="display: inline;padding: 3px!important;"><img src="https://image.flaticon.com/icons/svg/724/724664.svg" width="25px" height="25px"></a>

                    </li>
                    <?php if(!$is_logged_in){?>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        SIGN IN / SIGN OUT
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">SIGN IN</a>
                        <a class="dropdown-item" href="#">SIGN UP</a>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                </div>
              </nav>

            </div>
            <?php if($is_logged_in){?>
            <div class="col-12 border-bottom">
              <ul class="nav bg-white fixed-top justify-content-end" style="margin-top:5.2rem !important;">
                <li class="nav-item">
                  <a class="nav-link active" href="#"><i class="fas fa-home"></i> Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="fas fa-envelope"></i> Inbox</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="fas fa-bell"></i> Notification</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-users-cog"></i> My Account</a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">My Profile</a>
                    <a class="dropdown-item" href="#">Wallet</a>
                    <a class="dropdown-item" href="#">SIGN OUT</a>
                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="fas fa-shopping-cart"></i> Cart</a>
                </li>
              </ul>
            </div>
            <?php }?>
        </div>
      </header>
      <section>
        <div class="row m-0 p-0">
          <div class="col-12 m-0 p-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
                <?php
                  $ac=1;
                  foreach ($banner as $bnr) {
                      if($ac==1){
                      ?>
                      <div class="carousel-item active">
                        <a href="<?php echo $bnr->link;?>">
                          <img class="d-block w-100" style="height:95vh !important;" src="<?php echo base_url('assets/uploads/banner/'.str_replace('.jpg','_crop.jpg',$bnr->image_name));?>" alt="<?php echo $bnr->alt_text;?>">
                        </a>
                      </div>
                      <?php
                      $ac++;
                    }else{
                      ?>
                      <div class="carousel-item">
                        <a href="<?php echo $bnr->link;?>">
                          <img class="d-block w-100" style="height:95vh !important;" src="<?php echo base_url('assets/uploads/banner/'.str_replace('.jpg','_crop.jpg',$bnr->image_name));?>" alt="<?php echo $bnr->alt_text;?>">
                        </a>
                      </div>
                      <?php
                    }
                  }
                ?>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
      </section>
      <section class="container">
        <div class="row mt-5 m-0">
          <div class="col-md-8 col-12" id="nav-tab">
            <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
              <?php
              $ct=1;
                foreach ($category as $cat) {
                  if($ct==1){
                    ?><li class="nav-item">
                      <a class="nav-link active" id="pills-<?php echo strtolower($cat->category_name);?>-tab" data-toggle="pill" href="#pills-<?php echo strtolower($cat->category_name);?>" role="tab" aria-controls="pills-<?php echo strtolower($cat->category_name);?>" aria-selected="true" title="<?php echo ucwords(strtolower($cat->meta_title));?>"><?php echo ucwords(strtolower($cat->category_name));?></a>
                    </li>
                    <?php
                    $ct++;
                  }else{
                  ?>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-<?php echo strtolower($cat->category_name);?>-tab" data-toggle="pill" href="#pills-<?php echo strtolower($cat->category_name);?>" role="tab" aria-controls="pills-<?php echo strtolower($cat->category_name);?>" aria-selected="true" title="<?php echo ucwords(strtolower($cat->meta_title));?>"><?php echo ucwords(strtolower($cat->category_name));?></a>
                    </li>
                  <?php
                }
              }
              ?>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Locations</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" id="pills-hub-tab" data-toggle="pill" role="tab" href="#pills-hub">SELECT HUB</a>
                  <a class="dropdown-item" id="pills-category-tab" data-toggle="pill" role="tab" href="#pills-category">SELECT CATEGORY</a>
                </div>
              </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
              <?php
              $ct=1;
                foreach ($category as $cat) {
                  if($ct==1){
                    ?>
                      <div class="tab-pane fade show active" id="pills-<?php echo strtolower($cat->category_name);?>" role="tabpanel" aria-labelledby="pills-<?php echo strtolower($cat->category_name);?>-tab">
                        <div class="row justify-content-start">
                          <?php $sub_category = $this->COM->get_active_sub_category($cat->id);
                            foreach ($sub_category as $sub) {
                              ?>
                              <div class="col-md-4">
                                <div class="thumbnail bg-dark m-2">
                                  <img class="w-100" src="<?php echo base_url('assets/uploads/category/'.str_replace('.','_crop.',$sub->image_name));?>" alt="<?php echo $sub->meta_title;?>">
                                  <div class="caption text-center text-white">
                                    <h6 class="py-2"><?php echo $sub->category_name;?></h6>
                                  </div>
                                </div>

                              </div>
                              <?php
                            }
                          ?>
                        </div>
                      </div>
                    <?php
                    $ct++;
                  }else{
                  ?>
                    <div class="tab-pane fade" id="pills-<?php echo strtolower($cat->category_name);?>" role="tabpanel" aria-labelledby="pills-<?php echo strtolower($cat->category_name);?>-tab">
                      <div class="row justify-content-start">
                        <?php $sub_category = $this->COM->get_active_sub_category($cat->id);

                          foreach ($sub_category as $sub) {
                            ?>
                            <div class="col-md-4">
                              <div class="thumbnail bg-dark m-2">
                                <img class="w-100" src="<?php echo base_url('assets/uploads/category/'.str_replace('.','_crop.',$sub->image_name));?>" alt="<?php echo $sub->meta_title;?>">
                                <div class="caption text-center text-white">
                                  <h6 class="py-2"><?php echo $sub->category_name;?></h6>
                                </div>
                              </div>

                            </div>
                            <?php
                          }
                        ?>
                      </div>
                    </div>
                  <?php
                }
              }
              ?>
              <div class="tab-pane fade" id="pills-hub" role="tabpanel" aria-labelledby="pills-hub-tab">
                <div class="row justify-content-start">
                  <?php $sub_category = $this->COM->get_active_region($cat->id);
                    foreach ($sub_category as $sub) {
                      ?>
                      <div class="col-md-4">
                        <div class="thumbnail bg-dark m-2">
                          <img class="w-100" src="<?php echo base_url('assets/uploads/category/'.str_replace('.','_crop.',$sub->image_name));?>" alt="<?php echo $sub->region_name;?>">
                          <div class="caption text-center text-white">
                            <h6 class="py-2"><?php echo $sub->region_name;?></h6>
                          </div>
                        </div>

                      </div>
                      <?php
                    }
                  ?>
                </div>
              </div>
              <div class="tab-pane fade" id="pills-category" role="tabpanel" aria-labelledby="pills-category-tab">
                <div class="row justify-content-start">
                  <?php $sub_category = $this->COM->get_active_sub_category(4);

                    foreach ($sub_category as $sub) {
                      ?>
                      <div class="col-md-4">
                        <div class="thumbnail bg-dark m-2">
                          <img class="w-100" src="<?php echo base_url('assets/uploads/category/'.str_replace('.','_crop.',$sub->image_name));?>" alt="<?php echo $sub->meta_title;?>">
                          <div class="caption text-center text-white">
                            <h6 class="py-2"><?php echo $sub->category_name;?></h6>
                          </div>
                        </div>

                      </div>
                      <?php
                    }
                  ?>
                </div>
              </div>
            </div>

          </div>
          
        </div>
            </section>
    </div>

  </body>
</html>
