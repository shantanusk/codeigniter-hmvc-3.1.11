<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid mb-5 p-0 m-0" style="background: #ccc;">
  <div class="row m-0 pt-2 justify-content-center bg-white">

    <div class="col-md-2 mr-2 pr-2 m-0 p-0 shadow">
      <div class="row m-0 p-0">
        <?php if(count($category)>0){?>
        <div class="col-md-12 m-0 p-0">
        <hr class="border-bottom-green m-0 p-0" />
          <h5 class="text-red m-0 p-0 my-2">
          <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Category
          </h5>
          <hr class="border-bottom-green m-0 p-0" />
        </div>
        <div class="col-md-12 m-0 p-0 py-2" style="height:200px;overflow-y:auto;overflow-x:hidden;">
          <div class="row">
            <?php
              foreach ($category as $sct) {
                ?>
                <div class="col-12">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="category" id="<?php echo $sct->category_name;?>"  value="<?php echo url_title(strtolower(str_replace(" ", "-",$sct->category_name)));?>" <?php if($this->uri->segment(3,0) && $this->uri->segment(3)==strtolower(str_replace(" ", "-",$sct->category_name)) && !is_numeric($this->uri->segment(3))){ echo "checked";}?>>
                    <label class="form-check-label" for="<?php echo $sct->category_name;?>"><?php echo $sct->category_name;?></label>
                  </div>
                </div>
                <!--<div class="col-1">
                  <input type="radio" name="category" value="<?php echo strtolower(str_replace(" ", "-",$sct->category_name));?>" <?php if($this->uri->segment(3,0) && $this->uri->segment(3)==strtolower(str_replace(" ", "-",$sct->category_name)) && !is_numeric($this->uri->segment(3))){ echo "checked";}?> />
                </div>
                <div class="col"> <?php echo $sct->category_name;?></div>
                <div class="w-100"></div>-->
                <?php
              }
            ?>
            <script>
              $('input[type="radio"]').on('click', function() {
                   window.location = "<?php echo base_url('search/'.url_title($this->uri->segment(2)).'/');?>"+$(this).val();
              });
              </script>
          </div>
        </div>
        <?php
      }
        if($this->uri->segment(2,0) && $this->uri->segment(3,0) && !is_numeric($this->uri->segment(3))){
          //echo $this->COM->get_category_id_by_permalink();
          //echo $this->db->last_query();

          //exit();
        $category_sub_filter = $this->COM->get_active_sub_category_by_id($this->COM->get_category_id_by_permalink());
          if(count($category_sub_filter)>0){
        ?>
        <div class="col-md-12 m-0 p-0">
          <hr class="border-bottom-green m-0 p-0" />
          <h5 class="text-red m-0 p-0 my-2">
          <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Sub Category
          </h5>
          <hr class="border-bottom-green m-0 p-0" />
        </div>
        <div class="col-md-12 m-0 p-0 py-2" style="height:200px;overflow-y:auto;overflow-x:hidden;">
            <div class="row">
              <?php
                foreach ($category_sub_filter as $sct) {
                  ?>
                  <div class="col-12">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="category_sub" id="<?php echo $sct->category_name;?>" value="<?php echo base_url('search/'.$this->uri->segment(2).'/'.url_title($this->uri->segment(3)).'/'.url_title(str_replace(" ","-",strtolower($sct->category_name)))).'?'.$_SERVER['QUERY_STRING'];?>" <?php if($this->uri->segment(4,0) && $this->uri->segment(4)==strtolower(str_replace(" ", "-",$sct->category_name)) && !is_numeric($this->uri->segment(4))){ echo "checked";}?> />
                      <label class="form-check-label" for="<?php echo $sct->category_name;?>"><?php echo $sct->category_name;?></label>
                    </div>
                  </div>
                  <?php
                }
              ?>
            </div>
            <script>
              $('input[name="category_sub"]').on('click', function() {
                   window.location = $(this).val();
              });
            </script>
          </div>
        <?php }}

        ?>
          <div class="col-md-12 m-0 p-0">
            <hr class="border-bottom-green m-0 p-0" />
            <h5 class="text-red m-0 p-0 my-2">
            <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Gender
            </h5>
            <hr class="border-bottom-green m-0 p-0" />
          </div>
          <div class="col-md-12 m-0 p-0 py-2">
              <div class="row">
                <div class="col-12">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="male" value="<?php echo current_url().'?';?>gender=male&city=<?php echo $this->input->get('city');?>&order=<?php echo (empty($this->input->get('order')) ? 'desc':$this->input->get('order'));?>" <?php if($this->input->get('gender')== "male"){ echo "checked";}?> />
                    <label class="form-check-label" for="male">Male</label>
                  </div>
                </div>
                  <div class="col-12">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="gender" id="female" value="<?php echo current_url().'?';?>gender=female&city=<?php echo $this->input->get('city');?>&order=<?php echo (empty($this->input->get('order')) ? 'desc':$this->input->get('order'));?>" <?php if($this->input->get('gender')== "female"){ echo "checked";}?> />
                      <label class="form-check-label" for="female">Female</label>
                    </div>
                  </div>

                    <div class="col-12">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="others" value="<?php echo current_url().'?';?>gender=others&city=<?php echo $this->input->get('city');?>&order=<?php echo (empty($this->input->get('order')) ? 'desc':$this->input->get('order'));?>" <?php if($this->input->get('gender')== "others"){ echo "checked";}?> />
                        <label class="form-check-label" for="others">Others</label>
                      </div>
                    </div>
              </div>

              <script>
                $('input[name="gender"]').on('click', function() {
                     window.location = $(this).val();
                });
                </script>
            </div>
          <div class="col-md-12 m-0 p-0">
            <h5 class="text-red m-0 p-0 my-2">
            <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Location
            </h5>
            <hr class="border-bottom-green m-0 p-0" />
          </div>
          <div class="col-md-12 m-0 p-0 py-2 mb-5" style="height:200px;overflow-y:auto;overflow-x:hidden;">
              <div class="row">
                <?php
                    foreach ($city as $cty) {
                    ?>
                      <div class="col-12">
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="city" id="<?php echo $cty->city_name;?>" <?php if($this->input->get('city')== str_replace(" ","-",strtolower($cty->city_name))){ echo "checked";}?> value="<?php echo current_url().'?';?>gender=<?php echo $this->input->get('gender');?>&city=<?php echo str_replace(" ","-",strtolower($cty->city_name));?>&city_id=<?php echo $cty->id;?>&order=<?php echo (empty($this->input->get('order')) ? 'desc':$this->input->get('order'));?>" />
                          <label class="form-check-label" for="<?php echo $cty->city_name;?>"><?php echo $cty->city_name;?></label>
                        </div>
                      </div>

                    <?php
                  }
                ?>
              </div>

              <script>
                $('input[name="city"]').on('click', function() {
                     window.location = $(this).val();
                });
              </script>
            </div>

      </div>

    </div>
      <div class="col-md-8 mb-5">
        <div class="row">

            <?php
            if(count($users)==0){
              ?>
              <div class="col-md-12">
                Sorry, we do not have any options in this category currently. If you provide such service or were looking for this service, please <a href="<?php echo base_url();?>/contact-us.php" class="btn btn-primary">Contact Us</a>
              </div>
              <?php
            }
            //print_r($users);
            foreach ($users as $usr) {
              ?>
              <div class="col-md-12">
                <div class="row mb-5 shadow p-2">
                  <div class="col-12 text-green">
                    <h6 class="font-weight-bold">
                      <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;
                      <a class="text-green" href="<?php echo base_url('search/'.str_replace(" ","-", strtolower($usr->types)));?>">
                        <?php echo $usr->types;?>
                      </a>
                      <?php echo ($this->agent->is_mobile()) ? "<br/>":"";?>
                      <a class="text-green" href="<?php echo base_url('search/'.str_replace(" ","-", strtolower($usr->types.'/'.$usr->category)));?>">
                        <?php echo (($usr->category != "")? '<i class="fas fa-angle-double-right"></i> '.$usr->category : "");?>
                      </a>
                        <?php echo ($this->agent->is_mobile()) ? "<br/>":"";
                          if($usr->category_type == "subSubCatId"){
                        ?>
                        <a class="text-green" href="<?php echo base_url('search/'.str_replace(" ","-", strtolower($usr->types.'/'.$usr->category.'/'.$this->COM->get_category_sub_name_by_id($usr->category_sub_id))));?>">
                          <?php echo ($usr->category_sub_id != 0)? '<i class="fas fa-angle-double-right"></i> '.$this->COM->get_category_sub_name_by_id($usr->category_sub_id) : "";?>
                        </a>
                      <?php }?>
                    <hr class="border-bottom-green w-100" />
                  </div>
                  <div class="col-md-2 mb-3">
                    <?php
                    if($usr->apr !=="" || $usr->bpr !==""){
                        if($usr->types_id==4 || $usr->types_id==3){
                          if($this->common->file_check('assets/uploads/content/user/'.str_replace('.','.',$usr->profile_image),$usr->apr)){?>
                            <img class="w-100" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','.',$usr->apr)) ;?>" />
                          <?php }else{?>
                            <img src="<?php echo base_url('assets/images/default.jpg');?>" />
                          <?php }
                        }else{
                          if($this->common->file_check('assets/uploads/content/user/'.str_replace('.','_crop.',$usr->bpr),$usr->bpr)){?>
                            <img class="w-100" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','_crop.',$usr->bpr)) ;?>" />
                          <?php }else if($this->common->file_check('assets/uploads/content/user/'.$usr->bpr,$usr->bpr)){?>
                            <img class="w-100" src="<?php echo base_url('assets/uploads/content/user/'.$usr->bpr) ;?>" />
                          <?php }else if($this->common->file_check('assets/uploads/content/user/'.str_replace('.','_crop.',$usr->apr),$usr->apr)){?>
                            <img class="w-100" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','_crop.',$usr->apr)) ;?>" />
                          <?php }else if($this->common->file_check('assets/uploads/content/user/'.$usr->apr,$usr->apr)){?>
                            <img class="w-100" src="<?php echo base_url('assets/uploads/content/user/'.$usr->apr) ;?>" />
                          <?php }else{
                            ?>
                            <img src="<?php echo base_url('assets/images/default.jpg');?>" />
                          <?php }
                        }

                      }else{
                        ?>
                            <img src="<?php echo base_url('assets/images/default.jpg');?>" />
                          <?php
                      }?>
                  </div>
                  <div class="col-md-7">
                    <div class="row">
                      <div class="col-md-12">
                        <h6><span class="text-red intcap font-weight-bold">
                          <?php if($usr->types == "Locations"){
                            echo ucwords(strtolower($usr->proeprty_name))."<br />";
                          }else if($usr->types == "Services"){
                            echo ucwords(strtolower($usr->inventory_name))."<br />";
                          }else{
                            echo ucwords(strtolower($usr->first_name.' '.$usr->last_name));
                          }?>
                          <span class="text-green">|</span> <span class="text-yellow"><i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i></span> <span class="text-green">|</span> <span class="text-drak">0 Reviews</span></h6>
                        <hr class="border-bottom-green w-50" align="left" />
                        <?php if($usr->types == "Talent" || $usr->types == "Crew" || $usr->types == "Services"){if(!empty($usr->description) && strlen($usr->description)>141){ echo substr($usr->description, 0, strpos($usr->description, ' ', 141));}else{echo $usr->description;} if(strlen($usr->description)>141){ echo " <a href=''>Read More....</a>";}}?>
                        <?php if($usr->types == "Locations"){if(!empty($usr->about_location) && strlen($usr->about_location)>141){ echo substr($usr->about_location, 0, strpos($usr->about_location, ' ', 141));}else{echo $usr->about_location;} if(strlen($usr->about_location)>141){ echo " <a href=''>Read More....</a>";}}?>
                      </div>


                    </div>

                  </div>
                  <div class="col-md-3 text-center">
                    <?php
                    if($usr->types == "Talent"){
                      if($usr->category_type == "subCatId"){
                        $url = base_url('search/'.$this->uri->segment(2).'/'.str_replace(" ","-",strtolower($usr->category)).'/details/'.$usr->user_id.'/'.$usr->pr_id.'/'.$usr->types_id.'/'.$usr->category_id.'/0/'.url_title(strtolower($usr->first_name.'-'.$usr->last_name)));
                        ?>
                        <a target="_blank" href="<?php echo $url;?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                        <?php
                      }else if($usr->category_type == "subSubCatId"){
                        $url = base_url('search/'.$this->uri->segment(2).'/'.str_replace(" ","-",strtolower($usr->category.'/'.url_title($this->COM->get_category_sub_name_by_id($usr->category_sub_id)))).'/details/'.$usr->user_id.'/'.$usr->pr_id.'/'.$usr->types_id.'/'.$usr->category_id.'/'.$usr->category_sub_id.'/'.url_title(strtolower($usr->first_name.'-'.$usr->last_name)));
                        ?>
                        <a target="_blank" href="<?php echo $url;?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                        <?php
                      }
                    }else if($usr->types == "Crew"){
                      if($usr->category_sub_id == 0 || $usr->category_type == "subCatid"){
                        $url = base_url('search/'.$this->uri->segment(2).'/'.str_replace(" ","-",strtolower($usr->category)).'/details/'.$usr->user_id.'/'.$usr->pr_id.'/'.$usr->types_id.'/'.$usr->category_id.'/'.$usr->category_sub_id.'/'.url_title(strtolower($usr->first_name.'-'.$usr->last_name)));
                        ?>
                        <a target="_blank" href="<?php echo $url;?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                        <?php
                      }else if($usr->category_type == "subSubCatId"){
                        $url = base_url('search/'.$this->uri->segment(2).'/'.str_replace(" ","-",strtolower($usr->category.'/'.url_title($this->COM->get_category_sub_name_by_id($usr->category_sub_id)))).'/details/'.$usr->user_id.'/'.$usr->pr_id.'/'.$usr->types_id.'/'.$usr->category_id.'/'.$usr->category_sub_id.'/'.url_title(strtolower($usr->first_name.'-'.$usr->last_name)));
                        ?>
                        <a target="_blank" href="<?php echo $url;?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                        <?php
                      }
                    }else if($usr->types == "Locations"){
                      if($usr->category_sub_id == 0 || $usr->category_type == "subCatid"){
                        $url = base_url('search/'.$this->uri->segment(2).'/'.str_replace(" ","-",strtolower($usr->category)).'/details/'.$usr->user_id.'/'.$usr->pr_id.'/'.$usr->types_id.'/'.$usr->category_id.'/'.$usr->category_sub_id.'/'.url_title(strtolower($usr->first_name.'-'.$usr->last_name)));
                        ?>
                        <a target="_blank" href="<?php echo $url;?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                        <?php
                      }else if($usr->category_type == "subSubCatId"){
                        $url = base_url('search/'.$this->uri->segment(2).'/'.str_replace(" ","-",strtolower($usr->category.'/'.url_title($this->COM->get_category_sub_name_by_id($usr->category_sub_id)))).'/details/'.$usr->user_id.'/'.$usr->pr_id.'/'.$usr->types_id.'/'.$usr->category_id.'/'.$usr->category_sub_id.'/'.url_title(strtolower($usr->first_name.'-'.$usr->last_name)));
                        ?>
                        <a target="_blank" href="<?php echo $url;?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                        <?php
                      }
                    }else if($usr->types == "Services"){
                      if($usr->category_sub_id == 0 || $usr->category_type == "subCatid"){
                        $url = base_url('search/'.$this->uri->segment(2).'/'.str_replace(" ","-",strtolower($usr->category)).'/details/'.$usr->user_id.'/'.$usr->pr_id.'/'.$usr->types_id.'/'.$usr->category_id.'/'.$usr->category_sub_id.'/'.url_title(strtolower($usr->first_name.'-'.$usr->last_name)));
                        ?>
                        <a target="_blank" href="<?php echo $url;?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                        <?php
                      }else if($usr->category_type == "subSubCatId"){
                        $url = base_url('search/'.$this->uri->segment(2).'/'.str_replace(" ","-",strtolower($usr->category.'/'.url_title($this->COM->get_category_sub_name_by_id($usr->category_sub_id)))).'/details/'.$usr->user_id.'/'.$usr->pr_id.'/'.$usr->types_id.'/'.$usr->category_id.'/'.$usr->category_sub_id.'/'.url_title(strtolower($usr->first_name.'-'.$usr->last_name)));
                        ?>
                        <a target="_blank" hrebf="<?php echo $url;?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                        <?php
                      }
                    }
                    ?>
                    <span class="btn btn-group btn-group-sm mt-3">
                      <a href="" class="btn btn-success btn-sm">
                        <i class="fas fa-share-square"></i>
                      </a>
                        <a href="" class="btn btn-success btn-sm">
                          <i class="fab fa-facebook-square"></i>
                        </a>
                          <a href="" class="btn btn-success btn-sm">
                            <i class="fab fa-linkedin"></i>
                          </a>
                            <a href="" class="btn btn-success btn-sm">
                              <i class="fab fa-twitter-square"></i>
                            </a>




                    </span>
                  </div>

              </div>
              </div>
              <?php
            }?>
          </div>
          <div class="row">
            <?php if(isset($pagination_des)){?>
                              <div class="col-md-6 py-2 text-dark text-center">
                                  <?php
                                  echo $pagination_des;
                                  ?>
                              </div>
                              <div class="col-md-6 py-1 text-right">
                                  <?php
                                  echo $links;
                                  ?>
                              </div>
                              <?php }?>
          </div>
      </div>

  </div>
</section>
