<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
    $(document).ready(function(){
        $(window).on("load",function(){
            $("#collapseOne").mCustomScrollbar({
              setHeight:300,
					         theme:"dark-3"
            });
            $("#collapseOne1").mCustomScrollbar({
                setHeight:300,
  					         theme:"dark-3"
              });
        });
    });
</script>
<section class="container-fluid mb-5 p-0 m-0 border-top-green" style="background: #ccc;">
  <div class="row m-0 pt-2 justify-content-center bg-white">
    <div class="col-md-2 mr-2 pr-2 mb-3 m-0 p-0">
      <a class="btn btn-success bg-green form-control border-0 rounded-0 text-white">Get Yourself Listed</a>
    </div>
    <div class="col-md-6 mb-3">

      <div class="input-group mb-3">
        <div class="input-group-prepend">

            <?php
            $types = array();
              foreach ($type as $ty) {
                $types[base_url('search/'.strtolower($ty->category_name))] = $ty->category_name;
                //echo '<option value="'.base_url('search/'.strtolower($ty->category_name)).'">'.$ty->category_name.'</option>';
              }
              $vty_atr = ' id="type" required="yes" data-error="Please Select Type" class="selectpicker show-tick form-control custom-select" data-live-search="true" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"';
                         echo form_dropdown('type', $types, current_url(), $vty_atr);
            ?>
        </div>
          <select class="custom-select rounded-0" id="inputGroupSelect01">
            <option selected>Choose...</option>
            <?php
              foreach ($type as $ty) {
                echo '<option value="'.$ty->category_name.'">'.$ty->category_name.'</option>';
              }
            ?>
          </select>

        <div class="input-group-append">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary rounded-0" type="button"><i class="fas fa-search"></i> Search</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-2 mb-3">

          <select class="custom-select rounded-0" id="inputGroupSelect01">
            <option selected>Sort By...</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
    </div>
    <div class="col-md-2 mr-2 pr-2 m-0 p-0 shadow">
      <div class="row m-0 p-0">
        <div class="col-md-12 m-0 p-0">
        <hr class="border-bottom-green m-0 p-0" />
          <h5 class="text-red m-0 p-0 my-2">
          <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Category
          </h5>
          <hr class="border-bottom-green m-0 p-0" />
        </div>
        <div class="col-md-12 m-0 p-0 py-2" style="height:200px;overflow-y:auto;overflow-x:hidden;">
          <div class="row">
            <?php
              foreach ($category as $sct) {
                ?>

                <div class="col-1">
                  <input type="checkbox" />
                </div>
                <div class="col"> <?php echo $sct->category_name;?></div>
                <div class="w-100"></div>
                <?php
              }
            ?>
          </div>
        </div>
        <div class="col-md-12 m-0 p-0">
        <hr class="border-bottom-green m-0 p-0" />
          <h5 class="text-red m-0 p-0 my-2">
          <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Sub Category
          </h5>
          <hr class="border-bottom-green m-0 p-0" />
        </div>
        <div class="col-md-12 m-0 p-0 py-2" style="height:200px;overflow-y:auto;overflow-x:hidden;">
            <div class="row">
              <?php
                foreach ($category_sub as $sct) {
                  ?>

                  <div class="col-1">
                    <input type="checkbox" />
                  </div>
                  <div class="col"> <?php echo $sct->category_name;?></div>
                  <div class="w-100"></div>
                  <?php
                }
              ?>
            </div>
          </div>
          <div class="col-md-12 m-0 p-0">
          <hr class="border-bottom-green m-0 p-0" />
            <h5 class="text-red m-0 p-0 my-2">
            <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Gender
            </h5>
            <hr class="border-bottom-green m-0 p-0" />
          </div>
          <div class="col-md-12 m-0 p-0 py-2">
              <div class="row">


                    <div class="col-1">
                      <input type="checkbox" />
                    </div>
                    <div class="col"> Male</div>
                    <div class="w-100"></div>

                    <div class="col-1">
                      <input type="checkbox" />
                    </div>
                    <div class="col"> Female</div>
                    <div class="w-100"></div>

                    <div class="col-1">
                      <input type="checkbox" />
                    </div>
                    <div class="col"> Others</div>
                    <div class="w-100"></div>
              </div>
            </div>
          <div class="col-md-12 m-0 p-0">
            <h5 class="text-red m-0 p-0 my-2">
            <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Location
            </h5>
            <hr class="border-bottom-green m-0 p-0" />
          </div>
          <div class="col-md-12 m-0 p-0 py-2" style="height:200px;overflow-y:auto;overflow-x:hidden;">
              <div class="row">
                <?php
                    foreach ($city as $cty) {
                    ?>

                    <div class="col-1">
                      <input type="checkbox" />
                    </div>
                    <div class="col"> <?php echo $cty->city_name;?></div>
                    <div class="w-100"></div>
                    <?php
                  }
                ?>
              </div>
            </div>

      </div>

    </div>
      <div class="col-md-8 mb-5">

          <?php

          if(count($users)==0){
            ?>
            Sorry, we do not have any options in this category currently. If you provide such service or were looking for this service, please <a href="<?php echo base_url();?>/contact-us.php" class="btn btn-primary">Contact Us</a>
            <?php
          }
          foreach ($users as $usr) {
            //print_r($usr);
            $prf = $this->COM->get_user_profile($usr->id, $usr->category_id);
            //print_r($prf);
            if(!empty($prf->description)){
            ?>
            <div class="row mb-5 shadow p-2">
              <div class="col-12 text-green">
                <h6 class="font-weight-bold"><i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;<?php echo $usr->type;?><?php echo ($this->agent->is_mobile()) ? "<br/>":"";?> <?php if(!empty($usr->category)){echo (($usr->category != "")? '<i class="fas fa-angle-double-right"></i> '.$usr->category : "");?><?php echo ($this->agent->is_mobile()) ? "<br/>":"";}?> <?php if(!empty($usr->category_sub)){echo ($usr->category_sub != "")? '<i class="fas fa-angle-double-right"></i> '.$usr->category_sub : "";}?></h6>
                <hr class="border-bottom-green w-100" />
              </div>
              <div class="col-md-2 mb-3">
                <?php
                  $img = $this->COM->file_check('assets/uploads/content/user/'.$usr->profile_image,$usr->profile_image);
                ?>
                <img src="<?php echo $img; ?>" alt="" class="img-responsive w-100">
              </div>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-12">
                    <h6><span class="text-red intcap font-weight-bold"><?php echo $usr->first_name.' '.$usr->last_name;?> <span class="text-green">|</span> <span class="text-yellow"><i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i></span> <span class="text-green">|</span> <span class="text-drak">0 Reviews</span></h6>
                    <hr class="border-bottom-green w-50" align="left" />
                    <p><?php if(!empty($prf->description) && strlen($prf->description)>141){ echo substr($prf->description, 0, strpos($prf->description, ' ', 141));}else{echo $prf->description;} if(strlen($prf->description)>141){ echo " <a href=''>Read More....</a>";} ?></p>

                  </div>
                  <div class="col-md-12">
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <a href="<?php echo base_url();?>" class="btn btn-danger btn-sm mt-3">Call For Audition</a>
                        <!--<a href="<?php echo base_url();?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">Book Now</a>-->
                            <a href="<?php echo base_url();?>" class="btn btn-danger btn-sm bg-green border-0 mt-3">View Profile</a>
                    </div>
                  </div>
                </div>

              </div>

            </div>
            <?php
            }
          }?>
          <div class="row">
            <?php if(isset($pagination_des)){?>
                              <div class="col-md-6 py-2 text-dark text-center">
                                  <?php
                                  echo $pagination_des;
                                  ?>
                              </div>
                              <div class="col-md-6 py-1 text-right">
                                  <?php
                                  echo $links;
                                  ?>
                              </div>
                              <?php }?>
          </div>
      </div>

  </div>
</section>
