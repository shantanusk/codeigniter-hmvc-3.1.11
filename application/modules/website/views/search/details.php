<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
    /**
     * Simple fade transition,
     */
    .mfp-fade.mfp-bg {
      opacity: 0;
      -webkit-transition: all 0.15s ease-out;
      -moz-transition: all 0.15s ease-out;
      transition: all 0.15s ease-out;
    }
    .mfp-fade.mfp-bg.mfp-ready {
      opacity: 0.8;
    }
    .mfp-fade.mfp-bg.mfp-removing {
      opacity: 0;
    }

    .mfp-fade.mfp-wrap .mfp-content {
      opacity: 0;
      -webkit-transition: all 0.15s ease-out;
      -moz-transition: all 0.15s ease-out;
      transition: all 0.15s ease-out;
    }
    .mfp-fade.mfp-wrap.mfp-ready .mfp-content {
      opacity: 1;
    }
    .mfp-fade.mfp-wrap.mfp-removing .mfp-content {
      opacity: 0;
    }
    .card-primary.card-outline {
      border-top: 3px solid #00ada8 ;
      }
      .profile-user-img {
          border: 3px solid #adb5bd;
          margin: 0 auto;
          padding: 3px;
          width: 180px !important;
          height: 180px;
      }
      .nav-tabs {
          border-bottom: 1px solid #00ada8 !important;
      }
  </style>
<section class="container-fluid p-0 m-0 border-top-green">
  <div class="row justify-content-center bg-secondary m-0 p-0">
    <?php if($user_profile->cover_pic){?>
      <div class="col-md-12 p-0">
        <?php

        if($user_profile->cover_pic){
            if($this->COM->file_check('assets/uploads/content/user/'.str_replace('.','_crop.',$user_profile->cover_pic),$user_profile->cover_pic)){
          ?>
          <img class="w-100" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','_crop.',$user_profile->cover_pic)) ;?>" />
        <?php }else if($this->COM->file_check('assets/uploads/content/user/'.str_replace('.','.',$user_profile->cover_pic),$user_profile->cover_pic)){
        ?>
        <img class="w-100" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','.',$user_profile->cover_pic)) ;?>" />
        <?php }else{?>
        <img src="<?php echo base_url('assets/images/cover-photo.png');?>" />
        <?php }
          }else{?>
          <img src="<?php echo base_url('assets/images/cover-photo.png');?>" />
        <?php }?>
      </div>
    <?php }?>
          <div class="col-md-3 mx-2 my-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body">
                <div class="text-center">
                  <?php if($user_profile->profile_image !==""){

                    if($this->COM->file_check('assets/uploads/content/user/'.str_replace('.','_crop.',$user_profile->profile_image),$user_profile->profile_image)){?>
                    <img class="profile-user-img img-fluid img-circle rounded-circle" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','_crop.',$user_profile->profile_image)) ;?>" />
                  <?php }else if($this->COM->file_check('assets/uploads/content/user/'.str_replace('.','.',$user_profile->profile_image),$user_profile->profile_image)){?>
                  <img class="profile-user-img img-fluid img-circle rounded-circle" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','.',$user_profile->profile_image)) ;?>" />
                <?php }else{?>
                    <img class="profile-user-img img-fluid img-circle rounded-circle" src="<?php echo base_url('assets/images/default.jpg');?>" />
                  <?php }
                    }else{?>
                    <img class="profile-user-img img-fluid img-circle rounded-circle" src="<?php echo base_url('assets/images/default.jpg');?>" />
                  <?php }?>
                </div>

                <h5 class="profile-username text-center mt-2"><span class="text-green intcap"><?php echo $user_profile->first_name.' '.$user_profile->last_name;?></span></h5>
                <hr class="border-bottom-green" />
                <p class="font-weight-bold text-center">
                  <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;<?php echo $user_profile->types;?><?php echo ($this->agent->is_mobile()) ? "<br/>":"";?> <?php if(!empty($user_profile->category)){echo (($user_profile->category != "")? '<i class="fas fa-angle-double-right"></i> '.$user_profile->category : "");?><?php echo ($this->agent->is_mobile()) ? "<br/>":"";}?> <?php if(!empty($user_profile->category_sub)){echo ($user_profile->category_sub != "")? '<i class="fas fa-angle-double-right"></i> '.$user_profile->category_sub : "";}?>
                </p>
                <hr class="border-bottom-green" />
                <p class="font-weight-bold text-center"><span class="text-yellow"><i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i></span></p>
                <hr class="border-bottom-green" />
                <b>&nbsp;&nbsp;Reviews</b> <a class="float-right">1,322&nbsp;&nbsp;</a>
                <hr class="border-bottom-green" />
                <b>&nbsp;&nbsp;Visits</b> <a class="float-right">543&nbsp;&nbsp;</a>
                <hr class="border-bottom-green" />
                <b>&nbsp;&nbsp;Related Jobs</b> <a class="float-right">13,287&nbsp;&nbsp;</a>
                <hr class="border-bottom-green" />

                <div class="btn btn-group btn-sm rounded-0 text-center">
                  <a class="btn btn-success btn-sm" id="socialShareNotifySellerFacebook" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>">
                    <i class="fab fa-facebook-square fa-2x"></i>
                  </a>
                  <a class="btn btn-success btn-sm" id="socialShareNotifySellerTwitter" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="http://twitter.com/share?text=Check out this <?php echo $user_profile->category.' '.$user_profile->first_name." ".$user_profile->last_name; ?> on Filmboard Movies&url=<?php echo current_url(); ?>&hashtags=#<?php echo $user_profile->types; ?>#<?php echo $user_profile->category; ?>">
                    <i class="fab fa-twitter-square fa-2x"></i>
                  </a>
                  <a class="btn btn-success btn-sm" id="socialShareNotifySellerWhatsapp" class="whatsapp-link" href="" target="_blank">
                    <i class="fab fa-whatsapp-square fa-2x"></i>
                  </a>
                  <a class="btn btn-success btn-sm" id="socialShareNotifySellerLinkedin" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo current_url(); ?>&title=Check out <?php echo $user_profile->category.' '.$user_profile->first_name." ".$user_profile->last_name; ?> on Filmboard Movies&summary=&source=">
                    <i class="fab fa-linkedin fa-2x"></i>
                  </a>
                  <a class="btn btn-success btn-sm" id="socialShareNotifySellerGoogle" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="https://plus.google.com/share?url=<?php echo current_url(); ?>">
                    <i class="fab fa-google-plus-square fa-2x"></i>
                  </a>
                  <a class="btn btn-success btn-sm" id="socialShareNotifySellerGoogle" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="https://plus.google.com/share?url=<?php echo current_url(); ?>">
                    <i class="fas fa-phone-square fa-2x"></i>
                  </a>
                </div>

                                <hr class="border-bottom-green" />
                <strong><i class="fas fa-book mr-1"></i> Education</strong>

                <p class="text-muted">&nbsp;&nbsp;&nbsp;
                  <?php if($user_profile->qualifications !==0 && $user_profile->qualifications !==""){?>

                    <?php echo ucwords(strtolower(($user_profile->qualifications == "" ? "NA":$user_profile->qualifications)));}?>
                </p>

                <hr class="border-bottom-green" />

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted">&nbsp;&nbsp;&nbsp;NA</p>

                <hr class="border-bottom-green" />

                <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>

                <p class="text-muted">
&nbsp;&nbsp;&nbsp;
                  <span class="badge badge-danger"><?php echo ucwords(strtolower($user_profile->category));?></span>
                  <?php if($user_profile->category_type == "subSubCatId"){?>
                    <span class="badge badge-success"><?php echo ucwords(strtolower($user_profile->category_sub));?></span>
                  <?php }?>
                </p>

                                <hr class="border-bottom-green" />
                <h6 class="intcap font-weight-bold">Interested In</h6>
                <?php
                  foreach ($user_interest as $int) {
                    echo '&nbsp;&nbsp;&nbsp;<i class="text-muted fas fa-angle-double-right"></i> '.$int->interested_in."<br/>";
                  }
                ?>
                                <hr class="border-bottom-green" />
                <h6 class="intcap font-weight-bold">Languages Known</h6>

                  <?php
                    foreach ($user_languages as $lang) {
                      echo '&nbsp;&nbsp;&nbsp;<i class="text-muted fas fa-angle-double-right"></i> '.$lang->languages_known."<br/>";
                    }
                  ?>
                                  <hr class="border-bottom-green" />

                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

                <p class="text-muted">&nbsp;&nbsp;&nbsp;NA</p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-8 mx-2 my-4" id="nav-tab">
            <?php
              //print_r($user);
              //echo "<pre>";
                //print_r($user_profile);
            ?>
            <div class="card">
              <div class="card-header border-bottom-0 p-2" id="nav-tab">
                <ul class="nav nav-tabs text-center" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="true">
                      <i class="fas fa-user fa-2x"></i><br />About
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="credentials-tab" data-toggle="tab" href="#credentials" role="tab" aria-controls="credentials" aria-selected="false">
                      <i class="far fa-address-card fa-2x"></i><br />Credentials
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" id="portfolio-tab" data-toggle="tab" href="#portfolio" role="tab" aria-controls="portfolio" aria-selected="false">
                      <i class="fas fa-images fa-2x"></i><br />Portfolio
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">
                      <i class="fas fa-star fa-2x"></i><br />Reviews
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="extra-tab" data-toggle="tab" href="#extra" role="tab" aria-controls="extra" aria-selected="false">
                      <i class="fas fa-rupee-sign fa-2x"></i><br />Extra
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="terms-tab" data-toggle="tab" href="#terms" role="tab" aria-controls="terms" aria-selected="false">
                      <i class="fas fa-star-of-life fa-2x"></i><br />T&C
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="booking-tab" data-toggle="tab" href="#booking" role="tab" aria-controls="booking" aria-selected="false">
                      <i class="fas fa-phone fa-2x"></i><br />Contact
                    </a>
                  </li>
                </ul>

              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade" id="about" role="tabpanel" aria-labelledby="about-tab">
                    <div class="row p-2 text-justify">
                      <div class="col-md-8">
                        <?php if($user_profile->description !==0 && $user_profile->description !==""){?>
                          <h6 class="intcap font-weight-bold">Description</h6>
                          <p><?php echo $user_profile->description;?></p>
                          <hr class="border-bottom-dotted-green" />
                        <?php }?><?php if($user_profile->latest_projects !==0 && $user_profile->latest_projects !==""){?>
                          <h6 class="intcap font-weight-bold">Latest Projects</h6>
                          <p><?php echo $user_profile->latest_projects;?></p>
                          <hr class="border-bottom-dotted-green" />
                        <?php }?><?php if($user_profile->awards !==0 && $user_profile->awards !==""){?>
                          <h6 class="intcap font-weight-bold">Awards / Nominations</h6>
                          <p><?php echo $user_profile->awards;?></p>
                          <hr class="border-bottom-dotted-green" />
                        <?php }?><?php if($user_profile->credits !==0 && $user_profile->credits !==""){?>
                          <h6 class="intcap font-weight-bold">Credits</h6>
                          <p><?php echo $user_profile->credits;?></p>
                          <hr class="border-bottom-dotted-green" />
                        <?php }?>
                      </div>
                      <div class="col-md-4 px-2">
                        <?php if(!$user_profile->union_card_holder ==0 && !$user_profile->union_card_holder ==""){?>
                          <h6 class="intcap font-weight-bold">union card holder</h6>
                          <p>&nbsp;&nbsp;&nbsp;<?php echo ($user_profile->union_card_holder==1 ? "Yes":"No");?></p>
                          <hr class="border-bottom-dotted-green" />
                        <?php }?>
                          <?php if(!$user_profile->passport ==0 && !$user_profile->passport ==""){?>
                            <h6 class="intcap font-weight-bold">Passport</h6>
                            <p>&nbsp;&nbsp;&nbsp;<?php echo ($user_profile->passport==1 ? "Yes":"No");?></p>
                            <hr class="border-bottom-dotted-green" />
                          <?php }?>
                            <?php if(!$user_profile->gender ==0 && !$user_profile->gender ==""){?>
                              <h6 class="intcap font-weight-bold">Gender</h6>
                              <p>&nbsp;&nbsp;&nbsp;
                                <?php
                                if($user_profile->gender==1){
                                  echo "Male";
                                }else if($user_profile->gender==2){
                                  echo "Female";
                                }else{
                                  echo "Others";
                                }
                              ?></p>
                              <hr class="border-bottom-dotted-green" />
                            <?php }?>
                              <?php if($user_profile->working_since !==0 && $user_profile->working_since !==""){?>
                                <h6 class="intcap font-weight-bold">working since</h6>
                                <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->working_since;?></p>
                                <hr class="border-bottom-dotted-green" />
                              <?php }?>
                                <?php
                                if($user_profile->types == "Talent" || $user_profile->types == "Crew"){

                                  //$this->data['user_languages'] = $this->COM->get_user_languages_by_user_id();
                                  //$this->data['user_interest'] = $this->COM->get_user_interest_by_user_id();
                                  //$this->data['user_genres'] = $this->COM->get_user_genres_by_user_id();
                                  //$this->data['user_worked_for'] = $this->COM->get_user_worked_for_by_user_id();
                                  ?>
                                    <h6 class="intcap font-weight-bold">Worked For</h6>
                                      <?php
                                        foreach ($user_worked_for as $worked) {
                                          echo '&nbsp;&nbsp;&nbsp;<i class="fas fa-angle-double-right"></i> '.$worked->worked_for."<br/>";
                                        }
                                      ?>
                                    <hr class="border-bottom-dotted-green" />

                                      <h6 class="intcap font-weight-bold">Previously Worked As</h6>

                                        <?php
                                          foreach ($user_roles_played as $worked) {
                                            echo '&nbsp;&nbsp;&nbsp;<i class="fas fa-angle-double-right"></i> '.$worked->roles_played."<br/>";
                                          }
                                        ?>
                                      <hr class="border-bottom-dotted-green" />
                                    <h6 class="intcap font-weight-bold">Genres</h6>
                                      <?php
                                        foreach ($user_genres as $genre) {
                                          echo '&nbsp;&nbsp;&nbsp;<i class="fas fa-angle-double-right"></i> '.$genre->genre."<br/>";
                                        }
                                      ?>
                                      <hr class="border-bottom-dotted-green" />


                                            <h6 class="intcap font-weight-bold">Preferred Work Locations</h6>
                                              <ul class="list-group border-0">
                                              <?php
                                                //print_r($user_service_locations);
                                                /*
                                                if($location_row['service_location']=='777777'){
                  																$arr[] = "Worldwide";
                  															} else if($location_row['service_location']=='888888'){
                  																$arr[] = "Pan India";
                  															} else {

                  																$city_data = $functions->fetch($functions->query("select * from ".PREFIX."city_master where id='".$location_row['service_location']."' "));

                  																if(!empty($city_data['city_name'])){
                  																	$arr[]= $city_data['city_name'];
                  																}

                  															}*/
                                                /*foreach ($user_service_locations as $loc) {
                                                  echo '<li class="list-group-item border-0"><i class="fas fa-angle-double-right"></i> '.$lang->languages_known."</li>";
                                                }*/
                                              ?>
                                              </ul>
                                              <hr class="border-bottom-dotted-green" />
                                          <?php
                                }
                                 ?>
                      </div>

                    </div>
                  </div>
                  <div class="tab-pane fade" id="credentials" role="tabpanel" aria-labelledby="credentials-tab">
                    <div class="row">
                      <div class="col-md-8">

                        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css'>

                        <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js'></script>
                        <div class="row">
                          <div class="col-md-12 font-weight-bold">
                            <i class="fas fa-volume-up"></i> AUDIO

                          </div>
                          <?php foreach ($user_audio_links as $audio) {
                            $url=$audio->audio_links;
                            $class="";
          													if(!strpos($url, 'youtube.com') && !strpos($url, 'youtu.be')){
                                      $class = "popup-youtube";
          														$videoThumbnail = base_url('assets/images/audio_links.png');
          														$videoTitle 	= '';
          													} else { //for youtube videos
                                      $class = "";
          														$youtube_id 	= $this->SRCH->get_youtube_id($url);
          														$videoTitle 	= $this->SRCH->get_youtube_title($youtube_id);
          														$videoThumbnail = 'https://img.youtube.com/vi/'.$youtube_id.'/hqdefault.jpg';
          													}
                            ?>
                            <div class="col-md-3 text-center">
                              <a <?php if($class !==""){?>class="popup-youtube"<?php }else{ echo "target='_blank'";}?> href="<?php echo $url; ?>"><img class="w-100" src="<?php echo $videoThumbnail; ?>"></a>
          										<p><?php echo $videoTitle; ?></p>
                            </div>
                            <?php
                            }?>
                            <div class="col-md-12 my-3 font-weight-bold">
                              <hr class="border-bottom-dotted-green" />
                              <i class="fas fa-video"></i> VIDEO

                            </div>
                            <?php foreach ($user_video_links as $audio) {
                              $url=$audio->video_link;
                              $class = "";
                                        if(!strpos($url, 'youtube.com') && !strpos($url, 'youtu.be')){
                                          $class = "popup-youtube";
                                          $videoThumbnail = base_url('assets/images/audio_links.png');
                                          $videoTitle 	= '';
                                        } else { //for youtube videos
                                          $class = "";
                                          $youtube_id 	= $this->SRCH->get_youtube_id($url);
                                          $videoTitle 	= $this->SRCH->get_youtube_title($youtube_id);
                                          $videoThumbnail = 'https://img.youtube.com/vi/'.$youtube_id.'/hqdefault.jpg';
                                        }
                                ?>
                                <div class="col-md-4 text-center">
                                  <a <?php if($class == ""){?>class="popup-youtube"<?php }else{ echo "target='_blank'";}?> href="<?php echo $url; ?>"><img class="w-100" src="<?php echo $videoThumbnail; ?>"></a>
                                  <p><?php echo $videoTitle; ?></p>
                              </div>
                              <?php
                              }?>
                                <script type="text/javascript">
                                  $(document).ready(function() {
                                    $('.popup-youtube').magnificPopup({
                                      disableOn: 700,
                                      type: 'iframe',
                                      mainClass: 'mfp-fade',
                                      removalDelay: 160,
                                      preloader: false,

                                      fixedContentPos: false
                                    });
                                  });
                                </script>


                        </div>
                          <?php
                          //print_r($user_photos);
                          /*$this->data['user_audio_links'] = $this->SRCH->get_user_audio_links_by_user_id($id);
                          $this->data['user_video_links'] = $this->SRCH->get_user_video_links_by_user_id($id);
                          $this->data['user_photos'] = $this->SRCH->get_user_photos_by_user_id($id);*/
                          ?>
                      </div>
                      <div class="col-md-4">
                        <?php if($user_profile->height !==0 && $user_profile->height !==""){?>
                          <h6 class="intcap font-weight-bold">height</h6>
                          <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->height;?> Centimeters</p>
                          <hr class="border-bottom-dotted-green" />
                        <?php }?>
                          <?php if($user_profile->weight !==0 && $user_profile->weight !==""){?>
                            <h6 class="intcap font-weight-bold">weight</h6>
                            <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->weight;?>  KG</p>
                            <hr class="border-bottom-dotted-green" />
                          <?php }?>
                            <?php if($user_profile->chest !==0 && $user_profile->chest !==""){?>
                              <h6 class="intcap font-weight-bold">Chest / Bust</h6>
                              <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->chest;?> Inches</p>
                              <hr class="border-bottom-dotted-green" />
                            <?php }?>
                              <?php if($user_profile->waist !==0 && $user_profile->waist !==""){?>
                                <h6 class="intcap font-weight-bold">waist</h6>
                                <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->waist;?> Inches</p>
                                <hr class="border-bottom-dotted-green" />
                              <?php }?>
                                <?php if($user_profile->hips !==0 && $user_profile->hips !==""){?>
                                  <h6 class="intcap font-weight-bold">Hips</h6>
                                  <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->hips;?> Inches</p>
                                  <hr class="border-bottom-dotted-green" />
                                <?php }?>
                                  <?php if($user_profile->shoe_size !==0 && $user_profile->shoe_size !==""){?>
                                    <h6 class="intcap font-weight-bold">Shoes Size</h6>
                                    <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->shoe_size;?> Centimeters</p>
                                    <hr class="border-bottom-dotted-green" />
                                  <?php }?>
                                    <?php if($user_profile->eye_color !==0 && $user_profile->eye_color !==""){?>
                                      <h6 class="intcap font-weight-bold">Eye Colour</h6>
                                      <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->eye_color;?></p>
                                      <hr class="border-bottom-dotted-green" />
                                    <?php }?>
                                      <?php if($user_profile->hair_type !==0 && $user_profile->hair_type !==""){?>
                                        <h6 class="intcap font-weight-bold">Hair Type</h6>
                                        <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->hair_type;?></p>
                                        <hr class="border-bottom-dotted-green" />
                                      <?php }?>
                                        <?php if($user_profile->hair_color !==0 && $user_profile->hair_color !==""){?>
                                          <h6 class="intcap font-weight-bold">Hair Colour</h6>
                                          <p>&nbsp;&nbsp;&nbsp;<?php echo $user_profile->hair_color;?></p>
                                          <hr class="border-bottom-dotted-green" />
                                        <?php }?>
                      </div>

                    </div>
                  </div>
                  <div class="tab-pane fade show active" id="portfolio" role="tabpanel" aria-labelledby="portfolio-tab">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="card-columns popup-gallery">

                          <?php foreach ($user_photos as $photo) {
                            ?>
                            <div class="card">
                              <a href="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','_large.',$photo->new_image)) ;?>"  title="<?php echo ucwords(strtolower($user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name));?>">
                                <img class="card-img-top w-100" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','_crop.',$photo->new_image)) ;?>" alt="<?php echo ucwords(strtolower($user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name));?>">
                              </a>

                            </div>
                            <?php
                            }?>
                            <script type="text/javascript">
                                  $(document).ready(function() {
                                    $('.popup-gallery').magnificPopup({
                                      delegate: 'a',
                                      type: 'image',
                                      tLoading: 'Loading image #%curr%...',
                                      mainClass: 'mfp-img-mobile',
                                      gallery: {
                                        enabled: true,
                                        navigateByImgClick: &nbsp;true,
                                        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                                      },
                                      image: {
                                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                                        titleSrc: function(item) {
                                          return item.el.attr('title') + '<small>by <?php echo APP_NAME;?></small>';
                                        }
                                      }
                                    });
                                  });
                                </script>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                    <div class="row">
                      <div class="col-md-4"></div>
                        <div class="col-md-8 pb-5 pt-3">
                          <div class="rating-details">
          										<label>FILTER BY</label>
          										<select class="form-control custom-select" name="filterByStars" id="filterByStars">
          											<option value="">All Stars </option>
          											<option value="5">5 Stars </option>
          											<option value="4">4 Stars </option>
          											<option value="3">3 Stars </option>
          											<option value="2">2 Stars </option>
          											<option value="1">1 Star </option>
          										</select>
          									</div>
                        </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="extra" role="tabpanel" aria-labelledby="extra-tab">
                    <div class="row">
                      <div class="col-md-12 mt-3 intcap font-weight-bold">
                        Conveyance Charges (<i class="fas fa-rupee-sign"></i>) (within Mumbai)<br />
                        From - <i class="fas fa-rupee-sign"> <?php echo $user_profile->min_conveyance_charge;?></i> To - <i class="fas fa-rupee-sign"> <?php echo $user_profile->max_conveyance_charge;?></i>
                        <hr class="border-bottom-dotted-green" />
                      </div>
                        <div class="col-md-12 intcap font-weight-bold">
                          Night Shift Allowances (<i class="fas fa-rupee-sign"></i>)<br/>
                          <i class="fas fa-rupee-sign"> <?php echo $user_profile->night_shift_allowance;?></i>
                          <hr class="border-bottom-dotted-green" />
                        </div>
                          <div class="col-md-12 intcap font-weight-bold mb-5">
                            Before Shoot / Project (Signing Amount) -  <?php echo $user_profile->before_shoot_amount;?> %<br />
                            ON Shoot / Project -  <?php echo $user_profile->on_shot_amount;?> %<br />
                            After Shoot / Project (Credit Period) -  <?php echo $user_profile->after_shoot_amount;?> days

                          </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="terms" role="tabpanel" aria-labelledby="terms-tab">
                    <div class="row">
                      <div class="col-md-12 mt-3">
                        <h6 class="intcap font-weight-bold">Additional Notes</h6>
                        <?php echo $user_profile->additional_notes;?>
                        <hr class="border-bottom-dotted-green" />
                      </div>
                        <div class="col-md-12">
                          <h6 class="intcap font-weight-bold">Cancelation Policy</h6>
                          <?php echo $user_profile->cancellation_policy;?>
                          <hr class="border-bottom-dotted-green" />
                        </div>
                          <div class="col-md-12">
                            <h6 class="intcap font-weight-bold">Terms & Conditions</h6>
                            <?php echo $user_profile->terms;?>
                          </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="booking" role="tabpanel" aria-labelledby="booking-tab">
                    <div class="row">
                      <div class="col-md-12 mt-3 px-4">
                        <?php
                        $form_attr = array('class' => 'row', 'data-toggle' => 'validator', 'role' => 'form');
                          echo form_open(base_url('user/register'), $form_attr);
                        ?>
                        <div class="form-group col-md-6 mt-2">
                          <label for="first_name"><i class="fas fa-id-card-alt"></i> First Name <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
                          <input type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name');?>" required data-error="First Name Required!" class="form-control rounded-0" aria-describedby="first_nameHelp" placeholder="Enter First Name">
                          <span class="help-block mt-1 with-errors"></span>
                          <?php echo form_error('first_name'); ?>
                        </div>
                          <div class="form-group col-md-6 mt-2">
                            <label for="last_name"><i class="fas fa-id-card-alt"></i> Last Name <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
                            <input type="text" name="last_name" id="last_name" value="<?php echo set_value('last_name');?>" required data-error="Last Name Required!" class="form-control rounded-0" aria-describedby="last_nameHelp" placeholder="Enter Last Name">
                            <span class="help-block mt-1 with-errors"></span>
                            <?php echo form_error('last_name'); ?>
                          </div>
                        <div class="form-group col-md-6 mt-2">
                          <label for="mobile"><i class="fas fa-phone-square"></i> Mobile No <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
                            <div class="input-group mb-3 row m-0 p-0">
                                <?php
                                $countries = array(
                                  "" => "Choose....",
                                );
                                  foreach ($this->COM->get_country_list() as $country) {
                                    $countries[$country->country_code] = $country->country_name.' '.$country->country_code;
                                    //$types[strtolower($ty->category_name)] = $ty->category_name;
                                    //echo '<option value="'.base_url('search/'.strtolower($ty->category_name)).'">'.$ty->category_name.'</option>';
                                  }
                                  $vty_atr = ' id="country" required data-error="Country Code Required!" class="selectpicker col-4 m-0 p-0 show-tick form-control custom-select rounded-0" data-live-search="true"';
                                  echo form_dropdown('country', $countries, '+91', $vty_atr);
                                  ?>
                              <div class="input-group-append col-8 m-0 p-0">
                                <input type="number" name="mobile" id="mobile" value="<?php echo set_value('mobile');?>" required data-error="Mobile Number Required!" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Mobile No">

                              </div>
                            </div>
                            <span class="help-block mt-1 with-errors"></span>
                            <?php echo form_error('country'); ?><?php echo form_error('mobile'); ?>
                        </div>
                      <div class="form-group col-md-6 mt-2">
                        <label for="email"><i class="fas fa-envelope"></i> Email ID <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>

                        <input type="email" name="email" id="email" value="<?php echo set_value('email');?>" required data-error="Email ID Required!" class="form-control rounded-0" aria-describedby="emailHelp" placeholder="Enter Emial ID">
                        <span class="help-block mt-1 with-errors"></span>
                        <?php echo form_error('email'); ?>
                      </div>
                      <div class="form-group col-md-6 mt-2">
                        <label for="organisation">Gender <sup><i class=" text-red fas fa-star-of-life"></i></sup></label><br />
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                          <label class="btn btn-secondary active">
                            <input type="radio" name="gender" id="gender" value="1" required data-error="Gender Required!" autocomplete="off" checked> Male
                          </label>
                          <label class="btn btn-secondary">
                            <input type="radio" name="gender" id="gender" value="2" required data-error="Gender Required!" autocomplete="off"> Female
                          </label>
                          <label class="btn btn-secondary">
                            <input type="radio" name="gender" id="gender" value="3" required data-error="Gender Required!" autocomplete="off"> Other
                          </label>
                        </div>
                        <span class="help-block mt-1 with-errors"></span>
                        <?php echo form_error('gender'); ?>
                      </div>

                      <div class="form-group col-md-6 mt-2">
                        <label for="organisation">Are You A Producer?</label><br />
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                          <label class="btn btn-secondary">
                            <input type="radio" name="producer" id="producer" value="Yes" required data-error="Are You Producer? Required!" autocomplete="off"> Yes
                          </label>
                          <label class="btn btn-secondary active">
                            <input type="radio" name="producer" id="producer" value="No" required data-error="Are You Producer? Required!" autocomplete="off" checked> No
                          </label>
                        </div>
                        <span class="help-block mt-1 with-errors"></span>
                        <?php echo form_error('producer'); ?>
                      </div>
                          <div class="form-group col-md-6 mt-2">
                            <label for="user_password"><i class="fas fa-key"></i> Password  <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
                            <input type="password" name="user_password" id="user_password" value="<?php echo set_value('user_password');?>" required data-error="Password Required!" class="form-control rounded-0" aria-describedby="user_passwordHelp" placeholder="Enter Password">
                            <span class="help-block mt-1 with-errors"></span>
                            <?php echo form_error('user_password'); ?>
                          </div>
                              <div class="form-group col-md-6 mt-2">
                                <label for="confirm_password"><i class="fas fa-key"></i> Confirm Password  <sup><i class=" text-red fas fa-star-of-life"></i></sup></label>
                                <input type="password" name="confirm_password" id="confirm_password" required data-error="Confirm Password Required!" class="form-control rounded-0" aria-describedby="confirm_passwordHelp" placeholder="Enter Confirm Password">
                                <span class="help-block mt-1 with-errors"></span>
                                <?php echo form_error('confirm_password'); ?>
                              </div>
                                  <div class="form-group col-md-12 mt-2">
                                    <label for="organisation">Organisation Name</label>
                                    <input type="text" name="organisation" id="organisation" value="<?php echo set_value('organisation');?>" class="form-control rounded-0" aria-describedby="eorganisationHelp" placeholder="Enter Organisation Name">
                                    <span class="help-block mt-1 with-errors"></span>
                                    <?php echo form_error('organisation'); ?>
                                  </div>
                                      <div class="form-group col-md-12 mt-2">

                                        <input type="checkbox" name="terms" id="terms" required data-error="Accept Terms & Conditions" class="rounded-0" aria-describedby="termsHelp">&nbsp;&nbsp;<sup><i class=" text-red fas fa-star-of-life"></i></sup>
                                        <label for="terms">I agree to the <a href="<?php echo base_url('legal/terms-conditions');?>" target="_blank">Term & Conditions</a>.</label>
                                        <span class="help-block mt-1 with-errors"></span>
                                        <?php echo form_error('terms'); ?>
                                      </div>
                                <div class="form-group col-md-12 pb-4 text-right">
                                  <hr class="border-bottom-green" />
                                  <button type="submit" class="btn btn-primary bg-green border-0 rounded-0">SIGN UP</button>
                                </div>

                              </form>
                      </div>
                    </div>
                  </div>
                </div>

              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
</section>
