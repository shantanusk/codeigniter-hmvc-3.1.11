<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css'>

<script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js'></script>

<style type="text/css">
    /**
     * Simple fade transition,
     */
    .mfp-fade.mfp-bg {
      opacity: 0;
      -webkit-transition: all 0.15s ease-out;
      -moz-transition: all 0.15s ease-out;
      transition: all 0.15s ease-out;
    }
    .mfp-fade.mfp-bg.mfp-ready {
      opacity: 0.8;
    }
    .mfp-fade.mfp-bg.mfp-removing {
      opacity: 0;
    }

    .mfp-fade.mfp-wrap .mfp-content {
      opacity: 0;
      -webkit-transition: all 0.15s ease-out;
      -moz-transition: all 0.15s ease-out;
      transition: all 0.15s ease-out;
    }
    .mfp-fade.mfp-wrap.mfp-ready .mfp-content {
      opacity: 1;
    }
    .mfp-fade.mfp-wrap.mfp-removing .mfp-content {
      opacity: 0;
    }
  </style>
<section class="container-fluid p-0 m-0 mb-5">
  <div class="row p-0 m-0 justify-content-center">
    <?php
    if($user_profile->cover_pic){
        if($this->COM->file_check(base_url('assets/uploads/content/user/'.str_replace('.','.',$user_profile->cover_pic)),$user_profile->cover_pic)){
      ?>
      <div class="col-md-12 col-12 m-0 p-0" style="background: url('<?php echo base_url('assets/uploads/content/user/'.str_replace('.','.',$user_profile->cover_pic)) ;?>'); background-repeat: no-repeat;background-size: cover;background-position: center;">
    <?php }else{?>
    <div class="col-md-12 col-12 m-0 p-0" style="background: url('<?php echo base_url('assets/images/cover-bg.jpg') ;?> ');">
  <?php }
      }else{?>
      <div class="col-md-12 col-12 m-0 p-0" style="background: url('<?php echo base_url('assets/images/cover-bg.jpg') ;?> ');">
    <?php }?>

      <div class="row m-0 justify-content-center">
        <div class="col-md-10 py-5" style="height:55vh !important;">

        </div>
      </div>
    </div>
    <div class="col-md-2 border-bottom-green" style="margin-top:-80px !important;">
      <?php if($user_profile->profile_image !==""){
        if($this->COM->file_check(base_url('assets/uploads/content/user/'.str_replace('.','.',$user_profile->profile_image)),$user_profile->profile_image)){?>
        <img class="w-100" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','.',$user_profile->profile_image)) ;?>" />
      <?php }else{?>
        <img src="<?php echo base_url('assets/images/default.jpg');?>" />
      <?php }
        }else{?>
        <img src="<?php echo base_url('assets/images/default.jpg');?>" />
      <?php }?>
    </div>
    <div class="col-md-5 border-bottom-green">
      <h3 class="mt-3"><span class="text-green intcap"><?php echo ucwords(strtolower($user_profile->proeprty_name));?> </h3>
      <h3><span class="text-yellow"><i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i></span> <span class="text-green">|</span> <span class="text-drak">0 Reviews</span></h3>

      <h6 class="font-weight-bold">
        <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;<?php echo $user_profile->types;?><?php echo ($this->agent->is_mobile()) ? "<br/>":"";?> <?php if(!empty($user_profile->category)){echo (($user_profile->category != "")? '<i class="fas fa-angle-double-right"></i> '.$user_profile->category : "");?><?php echo ($this->agent->is_mobile()) ? "<br/>":"";}?> <?php if(!empty($user_profile->category_sub)){echo ($user_profile->category_sub != "")? '<i class="fas fa-angle-double-right"></i> '.$user_profile->category_sub : "";}?></h6>

    </div>
    <div class="col-md-3 border-bottom-green font-weight-bold">
      <p class="mt-3">ID: FB-<?php echo $user_profile->profile_no;?></p>
      <p class="mt-3">
        <i class="fas fa-share-alt-square fa-2x"></i>
        <a id="socialShareNotifySellerFacebook" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>">
          <i class="fab fa-facebook-square fa-2x"></i>
        </a>
        <a id="socialShareNotifySellerTwitter" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="http://twitter.com/share?text=Check out this <?php echo $user_profile->category.' '.$user_profile->first_name." ".$user_profile->last_name; ?> on Filmboard Movies&url=<?php echo current_url(); ?>&hashtags=#<?php echo $user_profile->types; ?>#<?php echo $user_profile->category; ?>">
          <i class="fab fa-twitter-square fa-2x"></i>
        </a>
        <a id="socialShareNotifySellerWhatsapp" class="whatsapp-link" href="" target="_blank">
          <i class="fab fa-whatsapp-square fa-2x"></i>
        </a>
        <a id="socialShareNotifySellerLinkedin" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo current_url(); ?>&title=Check out <?php echo $user_profile->category.' '.$user_profile->first_name." ".$user_profile->last_name; ?> on Filmboard Movies&summary=&source=">
          <i class="fab fa-linkedin fa-2x"></i>
        </a>
        <a id="socialShareNotifySellerGoogle" onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="https://plus.google.com/share?url=<?php echo current_url(); ?>">
          <i class="fab fa-google-plus-square fa-2x"></i>
        </a>
        <strong style="margin-left: 10px;" class="theme-color-gray">
          <a href="tel:+919326257428"><i class="fas fa-phone-square fa-2x"></i><span class="theme-color "><i class="flaticon-phone"></i> </span> CALL TO NEGOTIATE</a>
        </strong>

      </p>

      <p>Managed By: <?php echo $user_profile->manager_name;?><br/>Owner By: <?php echo $user_profile->first_name.' '.$user_profile->last_name;?></p>
    </div>

    <div class="col-md-10 mt-2 py-5 border-bottom-green text-center font-weight-bold">
      <div class="row justify-content-center">

        <?php if(!$user_profile->rate_ad == "" && !$user_profile->rate_ad == 0){?>
          <div class="col-md-4 intcap text-center">
            <i class="text-red fas fa-rupee-sign fa-2x"> <?php echo $user_profile->rate_ad;?> /-</i><br />
            AD / SHORT FILMS
          </div>
        <?php }?>
        <?php if(!$user_profile->rate_tv == "" && !$user_profile->rate_tv == 0){?>
          <div class="col-md-4 intcap text-center">
            <i class="text-red fas fa-rupee-sign fa-2x"> <?php echo $user_profile->rate_tv;?> /-</i><br />
            TV / WEB SERIES
          </div>
        <?php }?>
        <?php if(@$user_profile->rate_featured_films == "" && !$user_profile->rate_featured_films == 0){?>
          <div class="col-md-4 intcap text-center">
            <i class="text-red fas fa-rupee-sign fa-2x"> <?php echo $user_profile->rate_featured_films;?> /-</i><br />
            FEATURE FILMS
          </div>
        <?php }?>
      </div>
      <a class="btn btn-success rounded-0 mt-3" href="">BOOK NOW</a>

    </div>
    <div class="col-md-7 mt-4 border-bottom-green m-0 p-0">
      <div class="row">
        <div class="col-md-12">
          <h6 class="text-red">Description</h6>
          <?php echo $user_profile->about_location;?>
          <hr class="border-bottom-dotted-green" />
        </div>
          <div class="col-md-12">
            <h6 class="text-red">Amenities</h6>
            <?php echo $user_profile->about_location;?>
            <hr class="border-bottom-dotted-green" />
          </div>
          <?php if(!$user_profile->outdoor_view == "" && !$user_profile->outdoor_view==0){?>
            <div class="col-md-4">
              <h6 class="text-red">Outdoor View</h6>
              <?php echo $user_profile->outdoor_view;?>
              <hr class="border-bottom-dotted-green" />
            </div>
          <?php }if(!$user_profile->dimesions == "" && !$user_profile->dimesions==0){?>
              <div class="col-md-4">
                <h6 class="text-red">Dimensions</h6>
                <?php echo $user_profile->dimesions;?>
                <hr class="border-bottom-dotted-green" />
              </div>

              <?php }if(!$user_profile->outdoor_view == "" && !$user_profile->outdoor_view==0){?>
                  <div class="col-md-4">
                    <h6 class="text-red">Outdoor View</h6>
                    <?php echo $user_profile->outdoor_view;?>
                    <hr class="border-bottom-dotted-green" />
                  </div>
                <?php }?>
      </div>


    </div>
    <div class="col-md-3 mt-4 row border-bottom-green m-0 p-0">
      <?php if(!$user_profile->response_time == "" && !$user_profile->response_time==0){?>
      <div class="col-md-12">
        <h6 class="text-red">
        Confirmation in <?php echo $user_profile->response_time;?> hours</h6>

        <hr class="border-bottom-dotted-green" />
      </div>
    <?php }?>
    <?php if(!$user_profile->location_address == "" && !$user_profile->location_address==0){?>
    <div class="col-md-12">
      <h6 class="text-red">Address</h6>
      <p><?php echo $user_profile->location_address;?></p>
      <hr class="border-bottom-dotted-green" />
    </div>
  <?php }?>

  <?php if(!$user_profile->before_shoot_amount == "" && !$user_profile->before_shoot_amount==0){?>
  <div class="col-md-12">
    <h6 class="text-red">Payment Terms</h6>
    <p><?php echo $user_profile->before_shoot_amount;?></p>
    <hr class="border-bottom-dotted-green" />
  </div>
<?php }?>

  <?php if(!$user_profile->taxes == "" && !$user_profile->taxes==0){?>
  <div class="col-md-6">
    <h6 class="text-red">Taxes</h6>
    <p><?php echo $user_profile->taxes;?></p>
    <hr class="border-bottom-dotted-green" />
  </div>
<?php }?>
  <?php if(!$user_profile->shift_duration == "" && !$user_profile->shift_duration==0){?>
  <div class="col-md-6">
    <h6 class="text-red">Shift Durations</h6>
    <p><?php echo $user_profile->shift_duration;?></p>
    <hr class="border-bottom-dotted-green" />
  </div>
<?php }?>
    </div>
    <div class="col-md-10">
      <div class="row">
        <div class="col-md-12 my-3 font-weight-bold">
          <i class="fas fa-images"></i> Portfolio
        </div>
        <div class="col-md-12">
          <div class="card-columns popup-gallery">

            <?php foreach ($this->SRCH->get_user_location_photos_by_user_id() as $photo) {
              ?>
              <div class="card">
                <a href="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','.',$photo->new_image)) ;?>"  title="<?php echo ucwords(strtolower($user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name));?>">
                  <img class="card-img-top w-100" src="<?php echo base_url('assets/uploads/content/user/'.str_replace('.','.',$photo->new_image)) ;?>" alt="<?php echo ucwords(strtolower($user_profile->category.' '.$user_profile->first_name.' '.$user_profile->last_name));?>">
                </a>

              </div>
              <?php
              }?>
              <script type="text/javascript">
                    $(document).ready(function() {
                      $('.popup-gallery').magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        tLoading: 'Loading image #%curr%...',
                        mainClass: 'mfp-img-mobile',
                        gallery: {
                          enabled: true,
                          navigateByImgClick: true,
                          preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                        },
                        image: {
                          tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                          titleSrc: function(item) {
                            return item.el.attr('title') + '<small>by <?php echo APP_NAME;?></small>';
                          }
                        }
                      });
                    });
                  </script>
            </div>
        </div>
      </div>
    </div>

    <div class="col-md-7">
      <div class="row">
        <div class="col-md-12">
            <h6 class="text-red font-weight-bold">Reviews</h6>
            <hr class="border-bottom-red" />
        </div>
            <div class="col">
                <i class="fas fa-star text-yellow"></i>
              <i class="fas fa-star text-yellow"></i>
              <i class="fas fa-star text-yellow"></i>
                <i class="fas fa-star text-yellow"></i>
                  <i class="fas fa-star text-yellow"></i><br/>
                  Star

        </div>
          <div class="col">
            <i class="fas fa-star text-yellow"></i>
              <i class="fas fa-star text-yellow"></i>
                <i class="fas fa-star text-yellow"></i>
                  <i class="fas fa-star text-yellow"></i><br/>
                  Star
          </div>
            <div class="col">
              <i class="fas fa-star text-yellow"></i>
                <i class="fas fa-star text-yellow"></i>
                  <i class="fas fa-star text-yellow"></i><br/>
                  Star
            </div>
              <div class="col">
                <i class="fas fa-star text-yellow"></i>
                  <i class="fas fa-star text-yellow"></i><br/>
                  Star
              </div>
                <div class="col">
                  <i class="fas fa-star text-yellow"></i><br/>
                  Star
                </div>
                <div class="col-md-12 my-4">
                  No Reviews
                </div>
      </div>
    </div>
    <div class="col-md-3">

        <div class="row">
          <div class="col-md-12">
              <h6 class="text-red font-weight-bold">Write A Review</h6>
              <hr class="border-bottom-red" />
              <form class="row">

                  <div class="form-group col-md-12">
                    <label for="exampleInputEmail1">RATE</label>
                      <i class="far fa-star text-yellow"></i>
                        <i class="far fa-star text-yellow"></i>
                          <i class="far fa-star text-yellow"></i>
                            <i class="far fa-star text-yellow"></i>
                              <i class="far fa-star text-yellow"></i>
                  </div>
                <div class="form-group col-md-12">
                  <textarea class="form-control" placeholder="Type Your Review Here"></textarea>
                </div>
                <div class="form-group col-md-12">
                  <label for="exampleInputPassword1">Select Your Profile</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>

                <div class="form-group col-md-6">
                  <label for="exampleInputPassword1">From</label>
                  <input type="date" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputPassword1">To</label>
                  <input type="date" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-check col-md-12 text-right">
                  <button type="submit" class="btn btn-success">POST</button>
                </div>

              </form>
          </div>
        </div>
    </div>
  </div>
</section>
