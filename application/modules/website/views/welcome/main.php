<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
      <section class="container mb-5">
        <div class="row mt-3 m-0">
          <div class="col-md-8 col-12" id="nav-tab">
            <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
              <?php
              $ct=1;
                foreach ($type as $cat) {
                  if($cat->id == 4){
                    ?>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Locations</a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" id="pills-hub-tab" data-toggle="pill" role="tab" href="#pills-hub">SELECT HUB</a>
                        <a class="dropdown-item" id="pills-category-tab" data-toggle="pill" role="tab" href="#pills-category">SELECT CATEGORY</a>
                      </div>
                    </li>
                    <?php
                  }else{
                    if($ct==1){
                      ?><li class="nav-item">
                        <a class="nav-link active" id="pills-<?php echo strtolower($cat->category_name);?>-tab" data-toggle="pill" href="#pills-<?php echo strtolower($cat->category_name);?>" role="tab" aria-controls="pills-<?php echo strtolower($cat->category_name);?>" aria-selected="true" title="<?php echo ucwords(strtolower($cat->meta_title));?>"><?php echo ucwords(strtolower($cat->category_name));?></a>
                      </li>
                      <?php
                      $ct++;
                    }else{
                    ?>
                      <li class="nav-item">
                        <a class="nav-link" id="pills-<?php echo strtolower($cat->category_name);?>-tab" data-toggle="pill" href="#pills-<?php echo strtolower($cat->category_name);?>" role="tab" aria-controls="pills-<?php echo strtolower($cat->category_name);?>" aria-selected="true" title="<?php echo ucwords(strtolower($cat->meta_title));?>"><?php echo ucwords(strtolower($cat->category_name));?></a>
                      </li>
                    <?php
                  }
                }
              }
              ?>

            </ul>

            <div class="tab-content" id="pills-tabContent">
              <style>
              .dropdown:hover>.dropdown-menu {
                display: block;
              }
              </style>
              <?php
              $ct=1;
                foreach ($type as $typ) {
                  if($ct==1){
                    ?>
                      <div class="tab-pane fade show active" id="pills-<?php echo strtolower($typ->category_name);?>" role="tabpanel" aria-labelledby="pills-<?php echo strtolower($typ->category_name);?>-tab">
                        <div class="row justify-content-start">
                          <?php $category = $this->COM->get_active_category_by_id($typ->id);
                            foreach ($category as $cat) {
                              ?>
                              <div class="col-md-4">
                                <div class="thumbnail bg-dark m-2">
                                  <a href="<?php echo base_url('search/'.strtolower(str_replace(" ","-",$typ->category_name.'/'.$cat->category_name)))?>">
                                    <img class="w-100" src="<?php echo base_url('assets/uploads/category/'.str_replace('.','_crop.',$cat->image_name));?>" alt="<?php echo $cat->meta_title;?>">
                                  </a>

                                  <?php
                                  $category_sub = $this->COM->get_active_sub_category_by_id($cat->id);
                                    if(count($category_sub)>0){
                                      ?>
                                      <div class="caption text-center text-white">

                                        <div class="dropdown">
                                          <button class="dropdown-toggle bg-dark text-white py-2 border-0" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <?php echo "".$cat->category_name."";?>
                                          </button>
                                          <div class="dropdown-menu" style="margin-top:-5px !important;" aria-labelledby="about-us">
                                            <?php foreach ($category_sub as $sub) {?>
                                              <a class="dropdown-item" href="<?php echo base_url('search/'.strtolower(str_replace(" ","-",$typ->category_name.'/'.$cat->category_name.'/'.$sub->category_name)))?>"><?php echo $sub->category_name;?></a>
                                            <?php }?>

                                          </div>
                                        </div>
                                      </div>
                                  <?php
                              }else{
                                echo "<a href='".base_url('search/'.strtolower(str_replace(" ","-",$typ->category_name."/".$cat->category_name)))."'><h6 class='py-2 text-center text-white'>".$cat->category_name.'</h6></a>';
                              }?>

                            </div>

                          </div>
                              <?php
                            }
                          ?>
                        </div>
                      </div>
                    <?php
                    $ct++;
                  }else{
                  ?>
                  <div class="tab-pane fade" id="pills-<?php echo strtolower($typ->category_name);?>" role="tabpanel" aria-labelledby="pills-<?php echo strtolower($typ->category_name);?>-tab">
                    <div class="row justify-content-start">
                      <?php $category = $this->COM->get_active_category_by_id($typ->id);
                        foreach ($category as $cat) {
                          ?>
                          <div class="col-md-4">
                            <div class="thumbnail bg-dark m-2">
                              <a href="<?php echo base_url('search/'.strtolower(str_replace(" ","-",$typ->category_name)))?>">
                                <img class="w-100" src="<?php echo base_url('assets/uploads/category/'.str_replace('.','_crop.',$cat->image_name));?>" alt="<?php echo $cat->meta_title;?>">
                              </a>
                              <?php
                              $category_sub = $this->COM->get_active_sub_category_by_id($cat->id);
                                if(count($category_sub)>0){
                                  ?>
                                  <div class="caption text-center text-white">

                                    <div class="dropdown">
                                      <button class="dropdown-toggle bg-dark text-white py-2 border-0" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <?php echo "".$cat->category_name."";?>
                                      </button>
                                      <div class="dropdown-menu" style="margin-top:-5px !important;" aria-labelledby="about-us">
                                        <?php foreach ($category_sub as $sub) {?>
                                          <a class="dropdown-item" href="<?php echo base_url('search/'.strtolower(str_replace(" ","-",$typ->category_name.'/'.$cat->category_name.'/'.$sub->category_name)))?>"><?php echo $sub->category_name;?></a>
                                        <?php }?>

                                      </div>
                                    </div>
                                  </div>
                              <?php
                          }else{
                            echo "<a href='".base_url('search/'.strtolower(str_replace(" ","-",$typ->category_name."/".$cat->category_name)))."'><h6 class='py-2 text-center text-white'>".$cat->category_name.'</h6></a>';
                          }?>

                        </div>

                      </div>
                          <?php
                        }
                      ?>
                    </div>
                  </div>
                <?php
                }
              }
              ?>
              <div class="tab-pane fade" id="pills-hub" role="tabpanel" aria-labelledby="pills-hub-tab">
                <div class="row justify-content-start">
                  <?php $region = $this->COM->get_city();
                    foreach ($region as $reg) {
                      ?>
                      <div class="col-md-4">
                        <div class="thumbnail bg-dark m-2">
                          <img class="w-100" src="<?php echo base_url('assets/uploads/category/'.str_replace('.','_crop.',$reg->image_name));?>" alt="<?php echo $reg->city_name;?>">

                              <div class="caption text-center text-white">

                                <div class="dropdown">
                                  <button class="dropdown-toggle bg-dark text-white py-2 border-0" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <?php echo "".$reg->city_name."";?>
                                  </button>
                                  <div class="dropdown-menu rounded-0" style="margin-top:-5px !important;" aria-labelledby="about-us">

                                      <a class="dropdown-item" href="<?php echo base_url('search/'.strtolower(str_replace(" ","-", 'locations/bungalows?gender=&city='.$reg->city_name.'&city_id='.$reg->id.'&order=desc')))?>">Bunglows</a>
                                      <a class="dropdown-item" href="<?php echo base_url('search/'.strtolower(str_replace(" ","-", 'locations/live-locations?gender=&city='.$reg->city_name.'&city_id='.$reg->id.'&order=desc')))?>">Live Locations</a>
                                      <a class="dropdown-item" href="<?php echo base_url('search/'.strtolower(str_replace(" ","-", 'locations/ready-set-locations?gender=&city='.$reg->city_name.'&city_id='.$reg->id.'&order=desc')))?>">Ready Set Locations</a>
                                      <a class="dropdown-item" href="<?php echo base_url('search/'.strtolower(str_replace(" ","-", 'locations/studio-floors?gender=&city='.$reg->city_name.'&city_id='.$reg->id.'&order=desc')))?>">Studio Floors</a>

                                  </div>
                                </div>
                              </div>

                        </div>

                      </div>
                      <?php
                    }
                  ?>
                </div>
              </div>
              <div class="tab-pane fade" id="pills-category" role="tabpanel" aria-labelledby="pills-category-tab">
                <div class="row justify-content-start">
                  <?php $sub_category = $this->COM->get_active_category_by_id(4);

                    foreach ($sub_category as $sub) {
                      ?>
                      <div class="col-md-4">
                        <div class="thumbnail bg-dark m-2">
                          <img class="w-100" src="<?php echo base_url('assets/uploads/category/'.str_replace('.','_crop.',$sub->image_name));?>" alt="<?php echo $sub->meta_title;?>">

                              <?php
                              $category_sub = $this->COM->get_active_sub_category_by_id($sub->id);
                                if(count($category_sub)>0){
                                  ?>
                                  <div class="caption text-center text-white">

                                    <div class="dropdown">
                                      <button class="dropdown-toggle bg-dark text-white py-2 border-0" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <?php echo "".$sub->category_name."";?>
                                      </button>
                                      <div class="dropdown-menu" style="margin-top:-5px !important;" aria-labelledby="about-us">
                                        <?php foreach ($category_sub as $sub_sub) {?>
                                          <a class="dropdown-item" href="<?php echo base_url('search/'.strtolower(str_replace(" ","-",$typ->category_name.'/'.$sub->category_name.'/'.$sub_sub->category_name)))?>"><?php echo $sub_sub->category_name;?></a>
                                        <?php }?>

                                      </div>
                                    </div>
                                  </div>
                              <?php
                          }else{
                            echo "<a href='".base_url('search/'.strtolower(str_replace(" ","-",$typ->category_name."/".$cat->category_name)))."'><h6 class='py-2 text-center text-white'>".$cat->category_name.'</h6></a>';
                          }?>
                        </div>

                      </div>
                      <?php
                    }
                  ?>
                </div>
              </div>
            </div>

          </div>
          <div class="col-md-4 col-12">

              <h4 class="text-green">Top Reviews</h4>
              <hr class="border-default" />
            <div class="row">
                <div class="col-12 mt-3">
                  <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <?php
                        $rcount=1;
                       foreach ($this->COM->reviews() as $review) {
                        if($rcount==1){
                          $rcount++;
                          ?>
                          <div class="carousel-item active">
                            <div class="row">
                              <div class="col-md-4 text-center">
                                <img class="d-block w-100" src="<?php echo base_url('assets/uploads/user/'.$review->seller_image)?>" alt="<?php echo $review->reviewer_type;?>">
                                <?php for($i=1;$i<=$review->star_rating;$i++){
                                  echo '<i class="text-yellow fas fa-star mr-1 mt-2"></i>';
                                }?>
                              </div>
                              <div class="col-md-8">
                                <h5 class="text-red"><?php echo $review->first_name.' '.$review->last_name;?></h5>
                                <hr class="border-dark" />
                                <p><?php echo substr($review->review_msg,0,75); if(strlen($review->review_msg) > 75){ echo "..."; }?></p>
                              </div>
                            </div>
                          </div>
                          <?php
                        }else{
                          ?>
                          <div class="carousel-item">
                            <div class="row">
                              <div class="col-md-5 text-center">
                                <img class="d-block w-100" src="<?php echo base_url('assets/uploads/user/'.$review->seller_image)?>" alt="<?php echo $review->reviewer_type;?>">
                                <?php for($i=1;$i<=$review->star_rating;$i++){
                                  echo '<i class="text-yellow fas fa-star mr-1 mt-2"></i>';
                                }?>
                              </div>
                              <div class="col-md-7">
                                <h5 class="text-red"><?php echo $review->first_name.' '.$review->last_name;?></h5>
                                <hr class="border-dark" />
                                <p><?php echo substr($review->review_msg,0,75); if(strlen($review->review_msg) > 75){ echo "..."; }?></p>
                              </div>
                            </div>
                          </div>
                          <?php
                        }
                      }?>

                    </div>
                  </div>
                </div>
                <div class="col-12 mt-3">
                  <h4 class="text-green mt-3">Requirement Listing</h4>
                  <hr class="border-default" />
                </div>
              <?php foreach ($this->JOB->get_requirement_list(5) as $job) {
                ?>
                <div class="col-12 mt-2 row">
                  <div class="col-md-12">
                    <h6 class="text-green font-weight-bold">
                      <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;
                      <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($job->types)));?>"><?php echo $job->types;?></a>
                      <?php echo ($this->agent->is_mobile()) ? "<br/>":"";?>
                      <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($job->types.'/'.$job->category)));?>">
                        <?php echo (($job->category != "")? '<i class="fas fa-angle-double-right"></i> '.$job->category : "");?>
                      </a>
                        <?php echo ($this->agent->is_mobile()) ? "<br/>":"";
                          if(!$job->category_sub == ""){
                        ?>
                        <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($job->types.'/'.$job->category.'/'.$job->category_sub)));?>">
                          <?php echo ($job->category_sub != "")? '<i class="fas fa-angle-double-right"></i> '.$job->category_sub : "";?>
                        </a>
                      <?php }?>
                      </h6>
                  </div>
                  <div class="col-md-12">
                    <a href="<?php echo base_url('requirements/details/'.$job->id.'/'.$job->category_id.'/'.$job->sub_category_id.'/'.$job->sub_sub_category_id.'/'.str_replace(" ", "-",strtolower($job->types.'/'.$job->category.'/'.($job->category_sub == "" ? 'NA':$job->category_sub).'/'.$job->project_name)));?>">
                      <h6 class="text-red">
                        Required <?php echo $job->no_of_openings_qty.' '.$job->gender_type.' '.((!$job->category_sub == 0) ? $job->category_sub : $job->category).' For '.$job->project_type;?> For <?php echo $job->project_name;?>

                      </h6>

                      <h6 class="text-dark">Shoot Location: <?php echo $this->COM->get_project_location($job->project_locations);?></h6>

                    </a>
                  </div>
                  <div class="col-md-12 text-right">
                    <a class="btn btn-success btn-sm" href="<?php echo base_url('requirements/details/'.$job->id.'/'.$job->category_id.'/'.$job->sub_category_id.'/'.$job->sub_sub_category_id.'/'.str_replace(" ", "-",strtolower($job->types.'/'.$job->category.'/'.($job->category_sub == "" ? 'NA':$job->category_sub).'/'.$job->project_name)));?>">
                      View Details
                    </a>
                    <hr class="border-dark" />
                  </div>
                </div>
                <?php
              }?>
              <div class="col-12 mr-5 pr-5 text-right">
                <a href="<?php echo base_url('requirements');?>" class="btn btn-success btn-sm">View All ...</a>
              </div>
            </div>

          </div>
        </div>
      </section>


            <script type="text/javascript">
                  $(document).ready(function() {
                      $(".dropdown-menu").hover(function() {
                          var dropdownMenu =
                              $(this).children(".dropdown-menu");
                          if (dropdownMenu.is(":visible")) {
                              dropdownMenu.parent().toggleClass("open");
                          }
                      });
                  });
              </script>
