<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
.modal-lg{
  max-width: 80% !important;
}
</style>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 justify-content-center">
    <div class="col-md-8 col-12 m-0 p-0">
      <div class="row m-0 py-3 justify-content-center">
        <?php foreach ($strategic_advisors as $sa) {

          ?>

        <div class="col-md-4 p-4 border-bottom-green">
          <a data-toggle="modal" data-target="#<?php echo str_replace(" ", "-",strtolower($sa->advisor_name));?>Modal">
            <img src="<?php echo base_url('assets/uploads/strategic_advisors/'.str_replace('.','_crop.',$sa->image_name)) ;?>" alt="<?php echo $sa->advisor_name;?>" class="img-thumbnail">
            <div class="px-2">
              <h5 class="text-green mt-2 intcap"><?php echo strtolower($sa->advisor_name);?></h5>
              <h6 class="text-dark mt-3"><?php echo $sa->title;?></h6><br />

              <?php
              if(strlen($sa->quote) > 157){

    						echo substr($sa->quote, 0, 155)."... ";
    					} else {
    						echo $sa->quote;
    					}
              ?>
            </div>
          </a>
        </div>

            <!-- Modal -->
            <div class="modal fade" id="<?php echo str_replace(" ", "-",strtolower($sa->advisor_name));?>Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-body row text-justify">
                    <div class="col-md-3 p-4">
                      <img src="<?php echo base_url('assets/uploads/strategic_advisors/'.str_replace('.','_crop.',$sa->image_name)) ;?>" alt="<?php echo $sa->advisor_name;?>" class="img-thumbnail">
                      <h5 class="text-green mt-2 intcap"><?php echo strtolower($sa->advisor_name);?></h5>
                      <h6 class="text-dark mt-3"><?php echo $sa->title;?></h6><br />

                    </div>
                    <div class="col-md-9 p-4">
                      <?php echo $sa->description;?>
                      <hr class="border-bottom-green" />
                      <i class="fas fa-quote-left"></i>
                      <span class="text-green">
                        <?php echo $sa->quote;?>
                      </span>
                      <i class="fas fa-quote-right"></i>
                      <hr class="border-bottom-green" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
        <?php }
      ?>
      </div>
    </div>


  </div>
</section>
