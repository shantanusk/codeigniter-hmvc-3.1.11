<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid mb-5 border-top-green">
  <div class="row mt-5 m-0 justify-content-center">
    <div class="col-md-3">
    </div>
      <div class="col-md-9 mb-5">

          <?php foreach ($jobs as $jd) {
            ?>
            <div class="row mb-5 shadow p-2">
              <div class="col-12 text-green">
                <h5><i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;<?php echo $jd->main_category_name;?><?php echo ($this->agent->is_mobile()) ? "<br/>":"";?> <?php echo (($jd->sub_category_name != "")? '<i class="fas fa-angle-double-right"></i> '.$jd->sub_category_name : "");?><?php echo ($this->agent->is_mobile()) ? "<br/>":"";?> <?php echo ($jd->sub_sub_category_name != "")? '<i class="fas fa-angle-double-right"></i> '.$jd->sub_sub_category_name : "";?>
                <hr class="border-bottom-green w-100" />
              </div>
              <div class="col-md-2 col-2">
                <img src="<?php echo base_url(); ?>assets/uploads/placeholders/<?php echo $jd->main_category_name; ?>.png" alt="" class="img-responsive w-100">
              </div>
              <div class="col-md-10 col-10">
                <h5><span class="text-red intcap"><?php echo $jd->project_name;?></h5>
                <hr class="border-bottom-green w-25" align="left" />
                  <?php echo $jd->project_description;?>
                <p>Shoot Locations: <?php echo $this->COM->get_project_location($jd->project_locations);?></p>
              </div>
              <div class="col-md-4 bg-secondary pb-1 pt-2 text-white font-weight-bold">
                <?php
                if(!empty($jd->company_name)){
                  $producerName = $jd->company_name;
                } else {
                  $producerName = $jd->first_name.' '.$jd->last_name;
                }
                ?>
                <i class="fas fa-arrow-alt-circle-up"></i> Posted by:<a href="#" class="text-white" target="_blank">
                  <?php echo $producerName; ?>
                </a>
              </div>
              <div class="col-md-4 text-left bg-secondary pb-1 pt-2 text-white font-weight-bold">
                Posted on: <?php echo date("d-m-Y",strtotime($jd->created)); ?>
              </div>
              <div class="col-md-4 text-right bg-secondary py-1">
                <a class="btn btn-success bg-green border-0 rounded-0 text-white">View Details</a>
              </div>
            </div>
            <?php
          }?>

      </div>
  </div>
</section>
