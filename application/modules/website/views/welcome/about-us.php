<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid mb-5 border-top-green">
  <div class="row mt-5 m-0 justify-content-center">
    <div class="col-md-8 col-10 videoDiv">
      <iframe width="100%" height="400" src="https://www.youtube.com/embed/<?php echo $before->video_embed_url; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      <h3 class="bg-secondary text-white text-center p-3 pt-4"><?php echo $before->header_text; ?></h3>
    </div>
      <div class="col-md-8 col-12 mt -5 border filmboardLast">
        <div class="inner-last">
          <div class="title-ribbon ribbon-lab-y">
						<h3>BEFORE FILMBOARD</h3>
					</div>
          <div class="text-justify p-3 bg-white"><?php echo $before->short_desc;?></div>
        </div>
      </div>
        <div class="col-md-8 col-12 border filmboardLast">
          <div class="row">
            <div class="col-md-12 col-12">
              <div class="inner-last">
                <div class="title-ribbon ribbon-lab-t">
      						<h3>AFTER FILMBOARD</h3>
      					</div>
              </div>
            </div>

            <?php foreach ($after as $aft) {
              ?>
              <div class="col-md-3 pt-3 text-center bg-white">
                <img src="<?php echo base_url('assets/uploads/after_filmboard_icons/'.str_replace('.','_crop.',$aft->icon_image));?>" />
                <div class="text-justify mt-5"><?php echo $aft->short_desc;?></div>
              </div>
              <?php
            }?>
          </div>

        </div>
  </div>
</section>
