<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 p-0 justify-content-center">
    <div class="col-md-12 col-12 m-0 p-0" style="background: url('<?php echo base_url('assets/uploads/news_media/'.str_replace('.','_crop.',$media->banner_image)) ;?> ');">
      <div class="row m-0 py-3 justify-content-center">
        <div class="col-md-10 py-5">
          <h3 class="text-white intcap"><?php echo $media->header_text1;?></h3>
          <h3 class="text-white intcap"><?php echo $media->header_text2;?></h3>
        </div>
      </div>
    </div>
      <div class="col-md-10 col-12 m-0 p-0 mb-5 text-justify">
        <h5 class="text-green font-weight-bold mt-3">News & Media</h5>
        <small>Filmboard Movies:</small>
        <hr class="border-bottom-green" />
        <div class="row px-4">

          <div class="col-12">
            <div id="accordion"><?php
              $count=1;
                foreach ($news as $event) {

              ?>
              <div class="card border-0 border-bottom-red rounded-0 shadow mb-4">
                <div class="card-header" id="headingOne<?php echo $event->id; ?>">
                  <div class="row pt-2">
                    <div class="col-md-2 p-5">
                      <span><img src="<?php echo base_url('assets/images/icons/reel.svg')?>" /></span>
                    </div>
                    <div class="col-md-3">
                      <img src="<?php echo base_url('assets/uploads/news_media/'.str_replace('.','_crop.',$event->image_name)) ;?>" />
                    </div>
                    <div class="col-md-7">
                      <h5 class="text-green font-weight-bold"><?php echo $event->news_event_name; ?></h5>
                      <p><?php echo $event->short_desc; ?></p>
                      <button class="btn btn-link float-right text-dark" data-toggle="collapse" data-target="#collapseOne<?php echo $event->id; ?>" aria-expanded="false" aria-controls="collapseOne<?php echo $event->id; ?>">
                        Read More >>
                      </button>
                    </div>
                  </div>
                </div>

                <div id="collapseOne<?php echo $event->id; ?>" class="collapse hide" aria-labelledby="headingOne<?php echo $event->id; ?>" data-parent="#accordion">
                  <div class="card-body">

          					<div class="text-justify">
          						<?php echo $event->long_desc;
          						?>
          					</div>
                  </div>
                </div>
              </div>
              <?php
              $count++;
              }
              ?>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
</section>
