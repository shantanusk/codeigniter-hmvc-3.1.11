<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 mt-5 p-0 justify-content-center p-5">
    <div class="col-md-5" style="border-left:2px solid #fecf68;border-top:2px solid #fecf68;">

      <h4 class="text-green text-right bg-white float-left" align="right" style="font-weight: bold;margin-top: -55px;right: -22px !important;position: relative;">
        <i class="fas fa-quote-left fa-3x text-yellow"></i> <?php echo strtoupper($career->header); ?></h4>
    </div>
    <div class="col-md-5 px-5 text-justify" style="border-right:2px solid #fecf68;">
      <?php echo $career->description; ?>
			<span><i class="fas fa-quote-right fa-3x text-yellow"></i> </span>
			<h5 class="text-green text-right font-weight-bold cap">- <?php echo $career->written_by; ?></h5>
    </div>
    <div class="col-md-5 px-5 text-justify" style="background: url('<?php echo base_url('assets/uploads/career/'.str_replace('.','_crop.',$career->image_name2)) ;?>')">

			<h5 class="text-white text-right intcap py-4">- <?php echo $career->text1; ?></h5>

			<span><i class="fas fa-quote-right fa-3x text-yellow"></i> </span>
    </div>
    <div class="col-md-5 bg-red py-4 pr-0 text-white">
				<h4><?php echo ucfirst($career->text2); ?></h4>
				<div class="spe-text">
					<div class="tag-box">
						<span><?php echo strtoupper($career->text3); ?></span>
					</div>
					<p class="font-weight-bold"><?php echo ucfirst($career->text4); ?></p>
				</div>
				<div class="spe-text">
					<div class="tag-box">
						<span><?php echo strtoupper($career->text5); ?></span>
					</div>
					<p class="font-weight-bold"><?php echo ucfirst($career->text6); ?></p>
				</div>
				<div class="spe-text">
					<div class="tag-box">
						<span><?php echo strtoupper($career->text7); ?></span>
					</div>
					<p class="font-weight-bold"><?php echo ucfirst($career->text8); ?></p>
				</div>
    </div>
    <div class="col-md-5 p-4 mt-5" style="border-left:2px solid #fecf68;border-top:2px solid #fecf68;border-bottom:2px solid #fecf68;">
      <h5 class="text-green font-weight-bold">APPLY HERE</h5>
      <form class="px-4">
          <div class="form-group mt-3">
            <label for="exampleInputEmail1"><i class="fas fa-id-card-alt icon-size-19"></i> Full Name</label>
            <input type="text" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Full Name">
          </div>
            <div class="form-group mt-4">
              <label for="exampleInputEmail1"><i class="fas fa-envelope icon-size-19"></i> Email</label>
              <input type="email" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
              <div class="form-group mt-4">
                <label for="exampleInputEmail1"><i class="fas fa-phone-square icon-size-19"></i> Mobile</label>
                <input type="number" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Mobile No">
              </div>
                <div class="form-group mt-4">
                  <label for="exampleInputEmail1"><i class="fas fa-map-marker-alt icon-size-19"></i> Address</label>
                  <textarea class="form-control rounded-0" placeholder="Address"></textarea>
                </div>

                  <div class="form-group mt-4">
                    <label for="exampleInputEmail1">Applying For</label>
                    <select class="form-control custom-select" name="job_position">
												<option value="">Select the Position</option>
												<?php
													foreach ($vacancy as $job) {

												?>
														<option value="<?php echo $job->id; ?>"><?php echo $job->position; ?></option>
												<?php
													}
												?>
											</select>
                  </div>
                    <div class="form-group mt-4">
                      <label for="exampleInputEmail1">Experience</label>
                      <input type="number" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Mobile No">
                    </div>
                      <div class="form-group mt-4">
                        <label for="exampleInputEmail1"><i class="fas fa-file-upload icon-size-19"></i> Upload Resume</label><br />
                        <input type="file" class="btn btn-primary bg-green border-0 rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Mobile No">
                      </div>
                  <div class="form-group text-right">
                    <hr class="border-bottom-green" />
                    <button type="submit" class="btn btn-primary bg-green border-0 rounded-0">Send</button>
                  </div>

        </form>

    </div>
    <div class="col-md-5 p-4 mt-5" style="border-left:2px solid #ccc;border-right:2px solid #fecf68;border-top:2px solid #fecf68;border-bottom:2px solid #fecf68;">
      <h5 class="text-green font-weight-bold">CURRENT OPPORTUNITIES</h5>
      <div class="row px-4">

        <div class="col-12" style="height:700px !important;overflow-y:auto;">
          <div id="accordion"><?php
            $count=1;
              foreach ($vacancy as $job) {

            ?>
            <div class="card border-0 border-bottom-red rounded-0">
              <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                  <h6 class="text-red"><img src="<?php echo base_url();?>assets/images/icons/art-director.svg" width="30px"><?php echo $job->position; ?></h6>
                  <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Experience</span>: <?php echo $job->experience; ?></h6>
                  <button class="btn btn-link float-right text-red" data-toggle="collapse" data-target="#collapseOne<?php echo $count;?>" aria-expanded="false" aria-controls="collapseOne<?php echo $count;?>">
                    Read More..
                  </button>
                </h5>
              </div>

              <div id="collapseOne<?php echo $count;?>" class="collapse hide" aria-labelledby="headingOne<?php echo $count;?>" data-parent="#accordion">
                <div class="card-body">
                  <p><span>Responsibilities</span>
        					<div class="text-justify">
        						<?php echo $job->description;
        						?>
        					</div>
                </div>
              </div>
            </div>
            <?php
            $count++;
            }
            ?>
          </div>

        </div>

      </div>
    </div>
  </div>
</section>
