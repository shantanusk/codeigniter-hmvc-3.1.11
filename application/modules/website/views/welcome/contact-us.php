<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 p-0 justify-content-center">
    <div class="col-md-12 col-12 m-0 p-0" style="background: url('<?php echo base_url('assets/images/contact_us/'.str_replace('.','_crop.',$contact->banner_image)) ;?>');">
      <div class="row m-0 py-3 justify-content-center">
        <div class="col-md-10 pt-5 pb-4">
          <h3 class="text-white intcap"><?php echo $contact->banner_text1;?></h3>
          <h3 class="text-white intcap"><?php echo $contact->banner_text2;?></h3>
        </div>
      </div>
    </div>
      <div class="col-md-7 col-12 m-0 p-0 text-justify">
        <h6 class="mt-5"><?php echo $contact->header;?></h6>
        <hr class="border-bottom-green" />
        <div class="row my-5">
          <div class="col-md-6 pr-5">
            <h3 class="text-green font-weight-bold mb-5">HEAD OFFICE</h3>
            <?php echo $contact->address;?><br /><br />
            <a href="#0"><i class="text-danger fas fa-phone-volume"></i><span class="text-dark"> <?php echo $contact->contact_number;?></span></a><br />
            <a href="mailto:<?php echo $contact->email;?>"><i class="text-danger fas fa-envelope"></i> <span class="text-dark"><?php echo $contact->email;?></span></a>
            <h5 class="mt-5">FOLLOW US</h5>
              <?php
                foreach($this->COM->get_social_link() as $social) {
                  if($social->icon_link == "https://www.facebook.com/filmboardmovies"){
                    ?>

                    <a href="<?php echo $social->icon_link;?>" target="_blank">
                      <i class="text-red fab fa-facebook-square fa-2x"></i>
                    </a>

              <?php
            }elseif($social->icon_link == "https://twitter.com/filmboardmovies"){?>

                    <a href="<?php echo $social->icon_link;?>" target="_blank">
                      <i class="ml-2 text-red fab fa-twitter-square fa-2x"></i>
                    </a>

              <?php
            }elseif($social->icon_link == "https://www.linkedin.com/company/filmboard-movies-pvt-ltd/"){
                    ?>

                    <a href="<?php echo $social->icon_link;?>" target="_blank">
                      <i class="ml-2 text-red fab fa-linkedin-in fa-2x"></i>
                    </a>

              <?php
            }elseif($social->icon_link == "https://www.instagram.com/filmboardmovies/"){
                    ?>

                    <a href="<?php echo $social->icon_link;?>" target="_blank">
                        <i class="ml-2 text-red fab fa-instagram fa-2x"></i>
                    </a>

              <?php
            }
            }?>
          </div>
            <div class="col-md-6">
              <h3 class="text-green font-weight-bold mb-5">SEND US A MESSAGE</h3>
              <form>
                <div class="form-group mt-3">
                  <label for="exampleInputEmail1"><i class="fas fa-id-card-alt"></i> Full Name</label>
                  <input type="text" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Full Name">
                </div>
                  <div class="form-group mt-4">
                    <label for="exampleInputEmail1"><i class="fas fa-envelope"></i> Email</label>
                    <input type="email" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                  </div>
                    <div class="form-group mt-4">
                      <label for="exampleInputEmail1"><i class="fas fa-phone-square"></i> Mobile</label>
                      <input type="number" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Mobile No">
                    </div>
                      <div class="form-group mt-4">
                        <label for="exampleInputEmail1"><i class="fas fa-comment-alt"></i> Message / Comments</label>
                        <textarea class="form-control rounded-0" placeholder="Message / Comments"></textarea>
                      </div>

                        <div class="form-group mt-4">
                          <label for="exampleInputEmail1">Captcha: </label>
                          <p id="captImg"><?php echo $captchaImg; ?></p>
                          <p>Can't read the image? click <a href="javascript:void(0);" class="refreshCaptcha">here</a> to refresh.</p>
                          Enter the code :
                          <input type="text" name="captcha" value=""/>
                          <script>
                            $(document).ready(function(){
                                $('.refreshCaptcha').on('click', function(){
                                    $.get('<?php echo base_url().'website/welcome/refresh'; ?>', function(data){
                                        $('#captImg').html(data);
                                    });
                                });
                            });
                            </script>
                        </div>
                        <div class="form-group text-right">
                          <button type="submit" class="btn btn-primary bg-green border-0 rounded-0">Send</button>
                        </div>

              </form>

            </div>
        </div>

      </div>
    </div>
  </div>
</section>
