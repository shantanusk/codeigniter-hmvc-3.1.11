<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$c = 1;

?>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 p-0 justify-content-center">
    <div class="col-md-12 col-12 m-0 p-0">
      <div class="row m-0 py-3 justify-content-center">
        <?php foreach ($founder as $found) {
          if($c > 2){
          	$c = 1;
          }

          if($c % 2 == 0){
          	$side = 'Right';
          } else {
          	$side = 'Left';
          }
          $c++;
          ?>

        <div class="col-md-3 pt-5 pb-4 bg-secondary m-5">
          <div class="portContainer">
						<div class="portInner profile<?php echo $side; ?> " >
							<h4 class="toggle" profile='<?php echo $c; ?>'><?php echo $found->founder_name; ?></h4><br />
							<img class="toggle" profile='<?php echo $c; ?>' src="<?php echo base_url('assets/uploads/founders/'.str_replace('.','_crop.',$found->image_name)) ;?>" alt="<?php echo $found->founder_name; ?>" >
							<?php
								if(!empty($found->linkedin_url)){
								?>
									<span>
										<a href="<?php echo $found->linkedin_url; ?>" target="_blank" >
											<i class="fab fa-linkedin fa-2x text-white"></i>
										</a>
									</span>
								<?php
								}
							?>
						</div>
						<div class="profile-cont" rel='profile_<?php echo $c; ?>'>
							<?php echo $found->description; ?>
							<div class="infoProfileBtn"><img src="<?php echo base_url('assets/images/arrow-s.jpg'); ?>"></div>
						</div>
					</div>
        </div>
      <?php }?>
      </div>
    </div>


  </div>
</section>
<script>
	$(document).ready(function() {
		$(".infoProfileBtn").click(function(){
			$(".profile-cont").hide();
		});
		$('.profile-cont').hide();
			$(".toggle").click(function(){
		    $(".profile-cont").not($(this).next()).hide();
		    $("div[rel='profile_" + $(this).attr("profile") + "']").toggleClass('open').show();
		});
	});
	</script>
