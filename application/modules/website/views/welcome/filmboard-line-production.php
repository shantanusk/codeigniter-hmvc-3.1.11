<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if (!$this->agent->is_mobile()){?>
<script type="text/javascript" src="<?php echo base_url('assets/vendor-library/sticky-sidebar/dist/sticky-sidebar.min.js');?>"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        var sidebar = new StickySidebar('#sidebar', {topSpacing: 100,bottomSpacing: 250,
			containerSelector: '#sidebar'});
      });
    </script>
  <?php }?>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 p-0 mb-5 justify-content-center">
      <div class="col-md-12 col-12 m-0 p-0">
        <img class="w-100" src="<?php echo base_url('assets/images/fblpbg.jpg');?>" />
      </div>
    <div class="col-md-8 col-10 m-0 p-0">

      <div class="container-fluid">
        <h6 class="p-3 font-weight-bold">
              Filmboard is here to change the way Line production currently happens.
              <br/>It offers systematic, transparent Line Production services for any film / TV/ Web / ad film related project.
            </h6>
            <h3 class="font-weight-bold text-center my-5">Why Producers / Filmmakers choose us? </h3>
            <img class="w-100" src="<?php echo base_url('assets/images/Infographic.jpg');?>" />
            <h5 class="pt-5 px-5 text-center">Filmboard LP is already being favoured by more and more producers and filmmakers for their line production because of its transparency, convenience and credibility.<br /><br />Some of the projects line produced by us:</h5><br/>
            <iframe width="100%" height="400" src="https://www.youtube.com/embed/rDgpl_n7-MM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <h3 class="bg-danger text-white text-center p-3 pt-4 mb-3">The Asha Verdict – Gaana.com</h3>
            <iframe width="100%" height="400" src="https://www.youtube.com/embed/q-v295hPCvY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <h3 class="bg-green text-white text-center p-3 pt-4 mb-3">IIM Jammu Film (Documentary)</h3>
            <iframe width="100%" height="400" src="https://www.youtube.com/embed/Z8mIF80_osM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <h3 class="bg-danger text-white text-center p-3 pt-4 mb-3">Kahaanibaz (Short Film)- Royal Stag</h3>
      </div>
    </div>
      <div class="col-md-4 col-12 my-3" id="sidebar">
        <div class="container-fluid">
          <h6>Still wondering how we can help? Please provide these quick details and a Filmboard LP executive will get in touch with you shortly.</h6>
          <form>
            <div class="form-group mt-3">
              <label for="exampleInputEmail1">Full Name</label>
              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Full Name">
            </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
              </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Mobile</label>
                  <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Mobile No">
                </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Message / Comments</label>
                    <textarea class="form-control" placeholder="Message / Comments"></textarea>
                  </div>

                    <div class="form-group text-right">
                      <button type="submit" class="btn btn-primary bg-green border-0">Submit</button>
                    </div>

          </form>

        </div>
      </div>
  </div>
</section>
