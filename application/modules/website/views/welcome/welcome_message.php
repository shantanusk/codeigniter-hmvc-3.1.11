<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $descriptions;?>">
    <meta name="keywords" content="<?php echo $keywords;?>">
    <title><?php echo $title;?></title>

    <!-- using online links -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/bootstrap-4.3.1-dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
       integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/sidebar/css/main.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor-library/sidebar/css/sidebar-themes.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css');?>">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/vendor-library/sidebar/img/favicon.png');?>" />
</head>

<body>
  <div class="page-wrapper default-theme sidebar-bg bg1 toggled">
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-dark">
				<span id="toggle-sidebar" class="text-white"><i class="fas fa-bars fa-2x"></i></span>
  			<a class="navbar-brand" href="<?php echo base_url();?>">
  				&nbsp;&nbsp;<?php echo APP_SRT_NAME;?>
  			</a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  				<span class="navbar-toggler-icon"></span>
  			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="#"><i class="fas fa-sign-out-alt"></i></a>
				</li>
				</ul>
			</div>
		</nav>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">

                <!-- sidebar-header  -->
                <div class="sidebar-item sidebar-header d-flex flex-nowrap">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="assets/vendor-library/sidebar/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Jhon
                            <strong>Smith</strong>
                        </span>
                        <span class="user-role">Administrator</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-search  -->
                <div class="sidebar-item sidebar-search">
                    <div>
                        <div class="input-group">
                            <input type="text" class="form-control search-menu" placeholder="Search...">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- sidebar-menu  -->
                <div class=" sidebar-item sidebar-menu">
                    <ul>
                        <li class="header-menu">
                            <span>General</span>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-tachometer-alt"></i>
                                <span class="menu-text">Dashboard</span>
                                <span class="badge badge-pill badge-warning">New</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#">Dashboard 1
                                            <span class="badge badge-pill badge-success">Pro</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">Dashboard 2</a>
                                    </li>
                                    <li>
                                        <a href="#">Dashboard 3</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="menu-text">E-commerce</span>
                                <span class="badge badge-pill badge-danger">3</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#">Products

                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">Orders</a>
                                    </li>
                                    <li>
                                        <a href="#">Credit cart</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="far fa-gem"></i>
                                <span class="menu-text">Components</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#">General</a>
                                    </li>
                                    <li>
                                        <a href="#">Panels</a>
                                    </li>
                                    <li>
                                        <a href="#">Tables</a>
                                    </li>
                                    <li>
                                        <a href="#">Icons</a>
                                    </li>
                                    <li>
                                        <a href="#">Forms</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-chart-line"></i>
                                <span class="menu-text">Charts</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#">Pie chart</a>
                                    </li>
                                    <li>
                                        <a href="#">Line chart</a>
                                    </li>
                                    <li>
                                        <a href="#">Bar chart</a>
                                    </li>
                                    <li>
                                        <a href="#">Histogram</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-globe"></i>
                                <span class="menu-text">Maps</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#">Google maps</a>
                                    </li>
                                    <li>
                                        <a href="#">Open street map</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="header-menu">
                            <span>Extra</span>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-book"></i>
                                <span class="menu-text">Documentation</span>
                                <span class="badge badge-pill badge-primary">Beta</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-calendar"></i>
                                <span class="menu-text">Calendar</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-folder"></i>
                                <span class="menu-text">Examples</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- sidebar-menu  -->
            </div>
            <!-- sidebar-footer  -->
            <div class="sidebar-footer">
                <div class="dropdown">

                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-pill badge-warning notification">3</span>
                    </a>
                    <div class="dropdown-menu notifications" aria-labelledby="dropdownMenuMessage">
                        <div class="notifications-header">
                            <i class="fa fa-bell"></i>
                            Notifications
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
                            <div class="notification-content">
                                <div class="icon">
                                    <i class="fas fa-check text-success border border-success"></i>
                                </div>
                                <div class="content">
                                    <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit. In totam explicabo</div>
                                    <div class="notification-time">
                                        6 minutes ago
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                            <div class="notification-content">
                                <div class="icon">
                                    <i class="fas fa-exclamation text-info border border-info"></i>
                                </div>
                                <div class="content">
                                    <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit. In totam explicabo</div>
                                    <div class="notification-time">
                                        Today
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                            <div class="notification-content">
                                <div class="icon">
                                    <i class="fas fa-exclamation-triangle text-warning border border-warning"></i>
                                </div>
                                <div class="content">
                                    <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit. In totam explicabo</div>
                                    <div class="notification-time">
                                        Yesterday
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-center" href="#">View all notifications</a>
                    </div>
                </div>
                <div class="dropdown">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-envelope"></i>
                        <span class="badge badge-pill badge-success notification">7</span>
                    </a>
                    <div class="dropdown-menu messages" aria-labelledby="dropdownMenuMessage">
                        <div class="messages-header">
                            <i class="fa fa-envelope"></i>
                            Messages
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
                            <div class="message-content">
                                <div class="pic">
                                    <img src="img/user.jpg" alt="">
                                </div>
                                <div class="content">
                                    <div class="message-title">
                                        <strong> Jhon doe</strong>
                                    </div>
                                    <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit. In totam explicabo</div>
                                </div>
                            </div>

                        </a>
                        <a class="dropdown-item" href="#">
                            <div class="message-content">
                                <div class="pic">
                                    <img src="img/user.jpg" alt="">
                                </div>
                                <div class="content">
                                    <div class="message-title">
                                        <strong> Jhon doe</strong>
                                    </div>
                                    <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit. In totam explicabo</div>
                                </div>
                            </div>

                        </a>
                        <a class="dropdown-item" href="#">
                            <div class="message-content">
                                <div class="pic">
                                    <img src="img/user.jpg" alt="">
                                </div>
                                <div class="content">
                                    <div class="message-title">
                                        <strong> Jhon doe</strong>
                                    </div>
                                    <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit. In totam explicabo</div>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-center" href="#">View all messages</a>

                    </div>
                </div>
                <div class="dropdown">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-cog"></i>
                        <span class="badge-sonar"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuMessage">
                        <a class="dropdown-item" href="#">My profile</a>
                        <a class="dropdown-item" href="#">Help</a>
                        <a class="dropdown-item" href="#">Setting</a>
                    </div>
                </div>
                <div>
                    <a href="#">
                        <i class="fa fa-power-off"></i>
                    </a>
                </div>
                <div class="pinned-footer">
                    <a href="#">
                        <i class="fas fa-ellipsis-h"></i>
                    </a>
                </div>
            </div>
        </nav>
        <!-- page-content  -->
        <main class="page-content">
            <div id="overlay" class="overlay"></div>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-md-12">
                       	<div class="row px-3">
							   	<div class="col-md-12">

								   <h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>
									<h2>Pro Sidebar</h2>
									<p>This is a responsive sidebar template with dropdown menu based on bootstrap framework.</p>


								</div>

								<div class="col-md-12 p-0 text-center">
									<nav class="navbar navbar-light bg-dark text-white text-center">
										<a class="w-100"><?php echo $copyright;?></a>
									</nav>
								</div>
						</div>
                    </div>
                </div>
            </div>
        </main>
        <!-- page-content" -->
    </div>
    <!-- page-wrapper -->

    <!-- using online scripts -->
    <script src="<?php echo base_url('assets/vendor-library/jquery-3.3.1/jquery.min.js');?>"></script>
    </script>
    <script src="<?php echo base_url('assets/vendor-library/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js');?>">
    </script>
    <script src="<?php echo base_url('assets/vendor-library/scrollbar/jquery.mCustomScrollbar.concat.min.js');?>"></script>

    <script src="<?php echo base_url('assets/vendor-library/sidebar/js/main.js');?>"></script>
</body>

</html>
