<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid mb-5 border-top-green">
  <div class="row mt-5 m-0 justify-content-center">
    <div class="col-md-8 col-10">
    </div>
      <div class="col-md-4 col-12 my-5">
        <h3 class="text-red mt-5">Oops! Page not found</h3>
        <p>Sorry, but the page you are looking for is not found. Please, make sure you have typed the current URL.</p>
        <a href="<?php echo base_url();?>" class="btn btn-info rounded-0 mb-5">Back to Home</a>
      </div>
  </div>
</section>
