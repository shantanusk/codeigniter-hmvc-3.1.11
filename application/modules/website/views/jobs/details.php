<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid mb-5 border-top-green">
  <div class="row m-0 justify-content-center">
      <div class="col-md-11 mb-5 mt-3">
            <div class="row mb-5 pb-5 shadow p-2">
              <div class="col-12 text-green">
                <h5 class="font-weight-bold">
                  <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;
                  <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($jobs->types)));?>"><?php echo $jobs->types;?></a>
                  <?php echo ($this->agent->is_mobile()) ? "<br/>":"";?>
                  <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($jobs->types.'/'.$jobs->category)));?>">
                    <?php echo (($jobs->category != "")? '<i class="fas fa-angle-double-right"></i> '.$jobs->category : "");?>
                  </a>
                    <?php echo ($this->agent->is_mobile()) ? "<br/>":"";
                      if(!$jobs->category_sub == ""){
                    ?>
                    <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($jobs->types.'/'.$jobs->category.'/'.$jobs->category_sub)));?>">
                      <?php echo ($jobs->category_sub != "")? '<i class="fas fa-angle-double-right"></i> '.$jobs->category_sub : "";?>
                    </a>
                  <?php }?>
                <hr class="border-bottom-green" />
              </div>
              <div class="col-md-2 col-2">
                <img src="<?php echo base_url(); ?>assets/uploads/placeholders/<?php echo $jobs->types; ?>.png" alt="" class="img-responsive w-100">
              </div>
              <div class="col-md-10 col-10">

                <div class="row">
                  <div class="col-md-6">
                    <h5><span class="text-red intcap font-weight-bold"><?php echo $jobs->project_name;?></h5>
                    <hr class="border-bottom-green w-25" align="left" />
                    <?php echo $jobs->project_description;?>
                    <p><strong>Shoot Locations:</strong> <?php echo $this->COM->get_project_location($jobs->project_locations);?></p>
                  </div>
                    <div class="col-md-6 row">
                      <div class="col-md-12 mt-2">

            							<i class="fas fa-share-alt fa-2x"></i>

            								<a onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>">
            									<i class="fab fa-facebook-square fa-2x ml-2 text-dark"></i>
            								</a>

            								<a onclick="window.open(this.href,'_blank', 'width=700, height=300');return false;" href="http://twitter.com/share?text=Check out Job Openings for <?php if(!empty($requirementDetails['sub_sub_category_name'])){ echo $requirementDetails['sub_sub_category_name']; }else if(!empty($requirementDetails['sub_category_name'])){ echo $requirementDetails['sub_category_name']; } ?> on Filmboard Movies&url=<?php echo current_url(); ?>&hashtags=">
            									<i class="fab fa-twitter-square fa-2x ml-2 text-dark"></i>
            								</a>

            								<a href="whatsapp://send?text=<?php echo current_url(); ?>&title=Check out Job Openings for <?php if(!empty($requirementDetails['sub_sub_category_name'])){ echo $requirementDetails['sub_sub_category_name']; }else if(!empty($requirementDetails['sub_category_name'])){ echo $requirementDetails['sub_category_name']; } ?> on Filmboard Movies">
            									<i class="fab fa-whatsapp-square fa-2x ml-2 text-dark"></i>
            								</a>

            								<a onclick="window.open(this.href,'_blank', 'width=700, height=300'); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo current_url(); ?>&title=Check out Job Openings for <?php if(!empty($requirementDetails['sub_sub_category_name'])){ echo $requirementDetails['sub_sub_category_name']; }else if(!empty($requirementDetails['sub_category_name'])){ echo $requirementDetails['sub_category_name']; } ?> on Filmboard Movies&summary=&source=">
            									<i class="fab fa-linkedin fa-2x ml-2 text-dark"></i>
            								</a>

            								<a onclick="window.open(this.href,'_blank', 'width=700, height=300'); return false;" href="https://plus.google.com/share?url=<?php echo current_url(); ?>">
            									<i class="fab fa-google-plus-square fa-2x ml-2 text-dark"></i>
            								</a>

                      </div>
                          <div class="col-md-5 mt-2">
                            <h6 class="text-dark font-weight-bold">Job Status</h6>
                          </div>
                          <div class="col-md-7 mt-2">
                            <?php if(strtotime($jobs->requirement_status) >= strtotime(date('d-m-Y',time()))){
                              echo '<span class="btn btn-success btn-sm border-0 rounded-0">Active <i class="text-white fas fa-check-circle"></i></span>';
                            }else{
                              echo '<span class="btn btn-danger btn-sm border-0 rounded-0">Closed <i class="text-white fas fa-ban"></i></span>';
                            }?>
                          </div>
                          <div class="col-md-5 mt-2">
                            <h6 class="text-dark font-weight-bold">Duration</h6>
                          </div>
                          <div class="col-md-7 mt-2">
                          <?php echo $jobs->project_duration_from;?> - <?php echo $jobs->project_duration_to;?>
                          </div>
                          <div class="col-md-5 mt-2">
                            <h6 class="text-dark font-weight-bold">Last Date To Apply</h6>
                          </div>
                          <div class="col-md-7 mt-2">
                            <?php echo $jobs->requirement_expiry_date;?>
                          </div>

                          <div class="col-md-5 mt-2">
                            <h6 class="text-dark font-weight-bold">Posted On</h6>
                          </div>
                          <div class="col-md-7 mt-2">
                            <?php echo $jobs->created;?>
                          </div>

                    </div>
                  <div class="col-md-12 bg-secondary pb-1 pt-2 text-white font-weight-bold">
                    <?php
                    if(!empty($jobs->company_name)){
                      $producerName = $jobs->company_name;
                    } else {
                      $producerName = $jobs->first_name.' '.$jobs->last_name;
                    }
                    ?>
                    <i class="fas fa-arrow-alt-circle-up text-left"></i> Posted By:<a href="#" class="text-white" target="_blank">
                      <?php echo $producerName; ?>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-12">
                <hr class="border-bottom-green" />
                <h5 class="text-green font-weight-bold">
                  <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;Requirements: <?php echo $jobs->no_of_openings_qty;?> <?php echo ($jobs->gender_type !="") ? $jobs->gender_type: $jobs->category_sub; ?> <?php echo ((!$jobs->category_sub == 0) ? $jobs->category_sub : $jobs->category);?> For <?php echo $jobs->project_type;?></h5>
                <p class="intcap"><?php echo $jobs->any_other_details;?></p>
                <hr class="border-bottom-green w-100" />
              </div>
              <div class="col-md-4">
                <div class="row mt-3">
                  <div class="col-md-12">
                    <h5 class="text-green font-weight-bold"><i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;Price & Location Details</h5>
                    <hr class="border-bottom-green w-100" />
                  </div>
                  <div class="col-md-6 mt-3">
                    <h5 class="text-red">Minimum Budget </h5>
                  </div>
                  <div class="col-md-6 mt-3">
                    <h5 class="text-red"><i class="fas fa-rupee-sign"></i> <?php echo $jobs->min_budget;?> /- </h5>
                  </div>
                  <div class="col-md-6 mt-3">
                    <h5 class="text-red">Maximum Budget</h5>
                  </div>
                  <div class="col-md-6 mt-3">
                    <h5 class="text-red"><i class="fas fa-rupee-sign"></i> <?php echo $jobs->max_budget;?> /- </h5>
                  </div>

                  <div class="col-md-12 mt-3">
                    <hr class="border-bottom-green w-100" />
                  </div>
                  <?php if($jobs->location_amenities !=""){?>
                  <div class="col-md-4 mt-3">
                    <h6 class="text-dark font-weight-bold">Location Amenities</h6>
                  </div>
                  <div class="col-md-8 mt-3">
                    <p><?php echo $jobs->location_amenities;?></p>
                  </div>
                <?php }?>

                </div>

              </div>
              <?php if($jobs->audition_date !=""){?>
              <div class="col-md-4 border-left">
                <h5 class="text-green intcap mt-3 font-weight-bold"><i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;Audition Details</h5>
                <hr class="border-bottom-green w-100" />
                <div class="row mt-3 intcap">
                    <div class="col-md-4 mt-2">
                      <h6 class="text-dark font-weight-bold">Date</h6>
                    </div>
                    <div class="col-md-8 mt-2">
                      <?php echo $jobs->audition_date;?>
                    </div>
                    <div class="col-md-4 mt-2">
                      <h6 class="text-dark font-weight-bold">Time</h6>
                    </div>
                    <div class="col-md-8 mt-2">
                      <?php echo $jobs->audition_time_from;?> - <?php echo $jobs->audition_time_to;?>
                    </div>
                    <div class="col-md-12 mt-2">
                      <h6 class="text-dark font-weight-bold">Address</h6>
                    </div>
                    <div class="col-md-12 mt-2 intcap">
                      <?php echo strtolower($jobs->audition_address);?>
                      <hr />
                    </div>
                    <div class="col-md-12 mt-2">
                      <h6 class="text-dark font-weight-bold">Contact Information</h6>
                    </div>
                    <div class="col-md-12 mt-2">
                      <?php if($jobs->audition_contact_person != ""){echo $jobs->audition_contact_person;?> ( <?php echo $jobs->audition_contact_no;?> <i class="text-green fas fa-phone-square rounded-circle"></i> )<?php }else{ echo "Not Available";}?>
                      <hr />
                    </div>
                    <div class="col-md-12 mt-2">
                      <h6 class="text-dark font-weight-bold">COSTUME / LOOK SPECIFICATION</h6>
                    </div>
                    <div class="col-md-12 mt-2">
                      <?php echo $jobs->audition_costume_specific;?>
                      <hr />
                    </div>
                    <div class="col-md-12 mt-2">
                      <h6 class="text-dark font-weight-bold">SCENE / MONOLOGUE TO BE PREPARED</h6>
                    </div>
                    <div class="col-md-12 mt-2">
                      <i class="fas fa-file-download"></i> <?php if($jobs->audition_biodata == ""){ echo "No Attachment Found";}?>
                    </div>

                </div>

              </div>
            <?php }?>
              <div class="col-md-4 border-left">
                <h5 class="text-green intcap mt-3 font-weight-bold"><i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;Lowest bids so far</h5>
                <hr class="border-bottom-green w-100" />
                <?php if(count($job_bids)>0){?>
                  <div class="row">
                  <?php
                  foreach ($job_bids as $bids) {
                    echo '<div class="col-md-7">
                      <h6 class="text-red font-weight-bold intcap">'.$bids->first_name.' '.$bids->last_name.'</h6>
                    </div><div class="col-md-5">
                      <h6 class="text-red font-weight-bold"><i class="fas fa-rupee-sign"></i> '.$bids->suggested_rate.' /- '.'</h6>
                    </div>';
                  }
                  ?>
                </div>
                  <?php
                }else{
                  ?>
                  <h6 class="text-red font-weight-bold">0 Bid Yet</h6>
                  <?php
                }?>

                <hr class="border-bottom-green w-100" />
                <a class="btn btn-warning text-white rounded-0 <?php if($jobs->requirement_expiry_date<date('d-m-Y')){ echo 'disabled';}?>">Save</a>
                <a class="btn btn-info bg-green border-0 text-white rounded-0 <?php if($jobs->requirement_expiry_date<date('d-m-Y')){ echo 'disabled';}?>"  data-toggle="modal" data-target="#bidModal" data-whatever="@mdo">Apply Now</a>
                <h6 class="mt-3"> If you want to know anything beyond the details mentioned here, call at 9326257428  <i class="text-green fas fa-phone-square fa-2x rounded-circle"></i></h6>

              </div>
              <?php if($jobs->audition_date ==""){?>
                <div class="col-md-4 border-left">
                  <h5 class="text-green intcap mt-3 font-weight-bold"><i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;Similar Requirements</h5>
                  <hr class="border-bottom-green w-100" />
                </div>
              <?php }?>
            </div>


      </div>
  </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
