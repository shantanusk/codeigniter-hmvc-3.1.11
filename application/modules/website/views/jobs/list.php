<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="container-fluid mb-5">
  <div class="row m-0 pt-2 justify-content-center bg-white">

    <div class="col-md-2 mr-2 pr-2 mb-5 m-0 p-0 shadow">
      <div class="row m-0 p-0">
        <?php if(count($category)>0){?>
        <div class="col-md-12 m-0 p-0">
          <hr class="border-bottom-green m-0 p-0" />
          <h5 class="text-red m-0 p-0 my-2">
          <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Category
          </h5>
          <hr class="border-bottom-green m-0 p-0" />
        </div>
        <div class="col-md-12 m-0 p-0 py-2" style="height:200px;overflow-y:auto;overflow-x:hidden;">
          <div class="row">
            <?php
              foreach ($category as $sct) {
                ?>
                <div class="col-12">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="category" id="<?php echo $sct->category_name;?>"  value="<?php echo strtolower(str_replace(" ", "-",$sct->category_name));?>" <?php if($this->uri->segment(3,0) && $this->uri->segment(3)==strtolower(str_replace(" ", "-",$sct->category_name)) && !is_numeric($this->uri->segment(3))){ echo "checked";}?>>
                    <label class="form-check-label" for="<?php echo $sct->category_name;?>"><?php echo $sct->category_name;?></label>
                  </div>
                </div>
                <!--<div class="col-1">
                  <input type="radio" name="category" value="<?php echo strtolower(str_replace(" ", "-",$sct->category_name));?>" <?php if($this->uri->segment(3,0) && $this->uri->segment(3)==strtolower(str_replace(" ", "-",$sct->category_name)) && !is_numeric($this->uri->segment(3))){ echo "checked";}?> />
                </div>
                <div class="col"> <?php echo $sct->category_name;?></div>
                <div class="w-100"></div>-->
                <?php
              }
            ?>
            <script>
              $('input[type="radio"]').on('click', function() {
                   window.location = "<?php echo base_url('requirements/'.$this->uri->segment(2).'/');?>"+$(this).val();
              });
              </script>
          </div>
        </div>
        <?php
      }
        if($this->uri->segment(3,0) && !is_numeric($this->uri->segment(3))){
        $category_sub_filter = $this->COM->get_active_sub_category_by_id($this->COM->get_category_id_by_name(str_replace("-"," ",ucwords($this->uri->segment(3)))));
          if(count($category_sub_filter)>0){
        ?>
        <div class="col-md-12 m-0 p-0">
          <hr class="border-bottom-green m-0 p-0" />
          <h5 class="text-red m-0 p-0 my-2">
          <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Sub Category
          </h5>
          <hr class="border-bottom-green m-0 p-0" />
        </div>
        <div class="col-md-12 m-0 p-0 py-2" style="height:200px;overflow-y:auto;overflow-x:hidden;">
            <div class="row">
              <?php
                foreach ($category_sub_filter as $sct) {
                  ?>
                  <div class="col-12">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="category_sub" id="<?php echo $sct->category_name;?>" value="<?php echo base_url('requirements/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.str_replace(" ","-",strtolower($sct->category_name))).'?'.$_SERVER['QUERY_STRING'];?>" <?php if($this->uri->segment(4,0) && $this->uri->segment(4)==strtolower(str_replace(" ", "-",$sct->category_name)) && !is_numeric($this->uri->segment(4))){ echo "checked";}?> />
                      <label class="form-check-label" for="<?php echo $sct->category_name;?>"><?php echo $sct->category_name;?></label>
                    </div>
                  </div>
                  <?php
                }
              ?>
            </div>
            <script>
              $('input[name="category_sub"]').on('click', function() {
                   window.location = $(this).val();
              });
            </script>
          </div>
        <?php }}

        ?>
          <div class="col-md-12 m-0 p-0">
            <hr class="border-bottom-green m-0 p-0" />
            <h5 class="text-red m-0 p-0 my-2">
            <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Gender
            </h5>
            <hr class="border-bottom-green m-0 p-0" />
          </div>
          <div class="col-md-12 m-0 p-0 py-2">
              <div class="row">
                <div class="col-12">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="male" value="<?php echo current_url().'?';?>gender=male&city=<?php echo $this->input->get('city');?>&order=<?php echo (empty($this->input->get('order')) ? 'desc':$this->input->get('order'));?>" <?php if($this->input->get('gender')== "male"){ echo "checked";}?> />
                    <label class="form-check-label" for="male">Male</label>
                  </div>
                </div>
                  <div class="col-12">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="gender" id="female" value="<?php echo current_url().'?';?>gender=female&city=<?php echo $this->input->get('city');?>&order=<?php echo (empty($this->input->get('order')) ? 'desc':$this->input->get('order'));?>" <?php if($this->input->get('gender')== "female"){ echo "checked";}?> />
                      <label class="form-check-label" for="female">Female</label>
                    </div>
                  </div>

                    <div class="col-12">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="others" value="<?php echo current_url().'?';?>gender=others&city=<?php echo $this->input->get('city');?>&order=<?php echo (empty($this->input->get('order')) ? 'desc':$this->input->get('order'));?>" <?php if($this->input->get('gender')== "others"){ echo "checked";}?> />
                        <label class="form-check-label" for="others">Others</label>
                      </div>
                    </div>
              </div>

              <script>
                $('input[name="gender"]').on('click', function() {
                     window.location = $(this).val();
                });
                </script>
            </div>
          <div class="col-md-12 m-0 p-0">
            <h5 class="text-red m-0 p-0 my-2">
            <i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i> Location
            </h5>
            <hr class="border-bottom-green m-0 p-0" />
          </div>
          <div class="col-md-12 m-0 p-0 py-2 mb-5" style="height:200px;overflow-y:auto;overflow-x:hidden;">
              <div class="row">
                <?php
                    foreach ($city as $cty) {
                    ?>
                      <div class="col-12">
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="city" id="<?php echo $cty->city_name;?>" <?php if($this->input->get('city')== str_replace(" ","-",strtolower($cty->city_name))){ echo "checked";}?> value="<?php echo current_url().'?';?>gender=<?php echo $this->input->get('gender');?>&city=<?php echo str_replace(" ","-",strtolower($cty->city_name));?>&city_id=<?php echo $cty->id;?>&order=<?php echo (empty($this->input->get('order')) ? 'desc':$this->input->get('order'));?>" />
                          <label class="form-check-label" for="<?php echo $cty->city_name;?>"><?php echo $cty->city_name;?></label>
                        </div>
                      </div>

                    <?php
                  }
                ?>
              </div>

              <script>
                $('input[name="city"]').on('click', function() {
                     window.location = $(this).val();
                });
              </script>
            </div>

      </div>

    </div>
        <div class="col-md-9 mb-5">

          <?php if(count($jobs)==0){
            ?>
            Sorry, we do not have any options in this category currently. If you provide such service or were looking for this service, please <a href="<?php echo base_url();?>/contact-us.php" class="btn btn-primary">Contact Us</a>
            <?php
          }foreach ($jobs as $jd) {
            ?>
            <div class="row mb-5 shadow p-2">
              <div class="col-12 text-green">
                <h5><i class="fas fa-ellipsis-v"></i><i class="fas fa-ellipsis-v"></i>&nbsp;
                  <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($jd->types)));?>"><?php echo $jd->types;?></a>
                  <?php echo ($this->agent->is_mobile()) ? "<br/>":"";?>
                  <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($jd->types.'/'.$jd->category)));?>">
                    <?php echo (($jd->category != "")? '<i class="fas fa-angle-double-right"></i> '.$jd->category : "");?>
                  </a>
                    <?php echo ($this->agent->is_mobile()) ? "<br/>":"";
                      if(!$jd->category_sub == ""){
                    ?>
                    <a class="text-green" href="<?php echo base_url('requirements/'.str_replace(" ","-", strtolower($jd->types.'/'.$jd->category.'/'.$jd->category_sub)));?>">
                      <?php echo ($jd->category_sub != "")? '<i class="fas fa-angle-double-right"></i> '.$jd->category_sub : "";?>
                    </a>
                  <?php }?>
                <hr class="border-bottom-green w-100" />
              </div>
              <div class="col-md-2 col-2">
                <img src="<?php echo base_url(); ?>assets/uploads/placeholders/<?php echo $jd->types; ?>.png" alt="" class="img-responsive w-100">
              </div>
              <div class="col-md-7 col-10">
                <h5><span class="text-red intcap"><?php echo $jd->project_name;?></h5>
                <hr class="border-bottom-green w-25" align="left" />
                  <?php echo $jd->any_other_details;?>
                <p class="mt-3">Shoot Locations: <?php echo $this->COM->get_project_location($jd->project_locations);?></p>
              </div>

              <div class="col-md-3 col-12 py-5 font-weight-bold text-center">
                <?php
                  if(strtotime($jd->requirement_status) >= strtotime(date('d-m-Y',time()))){
                    echo '<span class="btn btn-success btn-sm border-0 rounded-0">Active <i class="text-white fas fa-check-circle"></i></span>';
                  }else{
                    echo '<span class="btn btn-danger btn-sm border-0 rounded-0">Closed <i class="text-white fas fa-ban"></i></span>';
                  }
                  ?>
                  <a class="btn btn-success btn-sm bg-green border-0 rounded-0 text-white" target="_blank" href="<?php echo base_url('requirements/details/'.$jd->id.'/'.$jd->category_id.'/'.$jd->sub_category_id.'/'.$jd->sub_sub_category_id.'/'.str_replace(" ", "-",strtolower($jd->types.'/'.$jd->category.'/'.($jd->category_sub == "" ? 'require-for':$jd->category_sub).'/'.$jd->project_name)));?>" target="_blank">View Details</a>
              </div>
              <div class="col-md-4 bg-secondary pb-1 pt-2 text-white font-weight-bold">
                <?php
                if(!empty($jd->company_name)){
                  $producerName = $jd->company_name;
                } else {
                  $producerName = $jd->first_name.' '.$jd->last_name;
                }
                ?>
                <i class="fas fa-arrow-alt-circle-up"></i> Posted by:<a href="#" class="text-white" target="_blank">
                  <?php echo $producerName; ?>
                </a>
              </div>
              <div class="col-md-4 text-justify bg-secondary pb-1 pt-2 text-white font-weight-bold">
                Posted on: <?php echo date("d-m-Y",strtotime($jd->created)); ?>
              </div>
              <div class="col-md-4 text-justify bg-secondary pb-1 pt-2 text-white font-weight-bold">
                Last Date To Apply: <?php echo date("d-m-Y",strtotime($jd->requirement_expiry_date)); ?>
              </div>
            </div>
            <?php
          }?>
          <div class="row">
            <?php if(isset($pagination_des)){?>
                              <div class="col-md-6 py-2 text-dark text-center">
                                  <?php
                                  echo $pagination_des;
                                  ?>
                              </div>
                              <div class="col-md-6 py-1 text-right">
                                  <?php
                                  echo $links;
                                  ?>
                              </div>
                              <?php }?>
          </div>
      </div>
  </div>
</section>
