<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products_model
 *
 * @author shiv
 */
class Jobs_models extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
        function get_requirement_search($page, $offset){
          $type_id = 0;
          if($this->uri->segment(2,0) && !is_numeric($this->uri->segment(2))){
            $type = ucwords(str_replace("-", " ",$this->uri->segment(2)));
            $type_id = $this->COM->get_type_id_by_name($type);
          }
          $category_id =0;
          if($this->uri->segment(3, 0) && !is_numeric($this->uri->segment(3))){
            $category = ucwords(str_replace("-", " ",$this->uri->segment(3)));
            $category_id = $this->COM->get_category_id_by_name($category);

          }
          $category_sub_id=0;
          if($this->uri->segment(4, 0) && !is_numeric($this->uri->segment(4))){
            $category_sub = ucwords(str_replace("-", " ",$this->uri->segment(4)));
            $category_sub_id = $this->COM->get_category_sub_id_by_name($category_sub);

          }
          $this->db->select('pr.*, u.first_name, u.last_name, u.company_name, p.project_name, p.project_status, p.project_type, p.project_description, p.project_locations, p.project_duration_from, p.project_duration_to, c.category_name as types, sc.category_name as category, ssc.category_name as category_sub');
          $this->db->from('fb_post_requirements pr');
          $this->db->join('fb_projects p','on pr.project_id = p.id', 'LEFT');
          $this->db->join('fb_category_master c','on pr.category_id = c.id', 'LEFT');
          $this->db->join('fb_sub_category_master sc','on pr.sub_category_id = sc.id', 'LEFT');
          $this->db->join('fb_sub_sub_category_master ssc','pr.sub_sub_category_id = ssc.id', 'LEFT');
          $this->db->join('fb_user u','pr.producer_id = u.id', 'LEFT');
          $this->db->where('pr.isDrafted', 'No');
          $this->db->where_not_in('pr.requirement_status', 'Closed');
          if(!$type_id == "" || !$type_id == 0){
            $this->db->where('c.id', $type_id);
          }
          if(!$category_id == "" || !$category_id == 0){
            $this->db->where('sc.id', $category_id);
          }
          if(!$category_sub_id == "" || !$category_sub_id == 0){
            $this->db->where('ssc.id', $category_sub_id);
          }
          if(!empty($this->input->get("gender"))){
            $this->db->where('pr.gender_type', $this->input->get("gender"));
          }
          if(!empty($this->input->get("city_id"))){
            $this->db->like('pr.locations', $this->input->get("city_id"));
          }
          if($this->input->get("order") == "asc"){
            $this->db->order_by('pr.created', 'asc');
          }else{
            $this->db->order_by('pr.created', 'desc');
          }
          $this->db->limit($page, $offset);
          $query = $this->db->get();
          return $query->result();
        }

        function get_requirement_search_count(){
          $type_id = 0;
          if($this->uri->segment(2,0) && !is_numeric($this->uri->segment(2))){
            $type = ucwords(str_replace("-", " ",$this->uri->segment(2)));
            $type_id = $this->COM->get_type_id_by_name($type);
          }
          $category_id =0;
          if($this->uri->segment(3, 0) && !is_numeric($this->uri->segment(3))){
            $category = ucwords(str_replace("-", " ",$this->uri->segment(3)));
            $category_id = $this->COM->get_category_id_by_name($category);

          }
          $category_sub_id=0;
          if($this->uri->segment(4, 0) && !is_numeric($this->uri->segment(4))){
            $category_sub = ucwords(str_replace("-", " ",$this->uri->segment(4)));
            $category_sub_id = $this->COM->get_category_sub_id_by_name($category_sub);

          }
          $this->db->select('pr.*, u.first_name, u.last_name, u.company_name, p.project_name, p.project_status, p.project_type, p.project_description, p.project_locations, p.project_duration_from, p.project_duration_to, c.category_name as types, sc.category_name as category, ssc.category_name as category_sub');
          $this->db->from('fb_post_requirements pr');
          $this->db->join('fb_projects p','on pr.project_id = p.id', 'LEFT');
          $this->db->join('fb_category_master c','on pr.category_id = c.id', 'LEFT');
          $this->db->join('fb_sub_category_master sc','on pr.sub_category_id = sc.id', 'LEFT');
          $this->db->join('fb_sub_sub_category_master ssc','pr.sub_sub_category_id = ssc.id', 'LEFT');
          $this->db->join('fb_user u','pr.producer_id = u.id', 'LEFT');
          $this->db->where('pr.isDrafted', 'No');
          $this->db->where_not_in('pr.requirement_status', 'Closed');
          if(!$type_id == "" || !$type_id == 0){
            $this->db->where('c.id', $type_id);
          }
          if(!$category_id == "" || !$category_id == 0){
            $this->db->where('sc.id', $category_id);
          }
          if(!$category_sub_id == "" || !$category_sub_id == 0){
            $this->db->where('ssc.id', $category_sub_id);
          }
          if(!empty($this->input->get("gender"))){
            $this->db->where('pr.gender_type', $this->input->get("gender"));
          }
          if(!empty($this->input->get("city_id"))){
            $this->db->like('pr.locations', $this->input->get("city_id"));
          }
          if($this->input->get("order") == "asc"){
            $this->db->order_by('pr.created', 'asc');
          }else{
            $this->db->order_by('pr.created', 'desc');
          }
          $query = $this->db->get();
          return $query->result();
        }


    function get_requirement_list($limit=0){

      $this->db->select('pr.*, u.first_name, u.last_name, u.company_name, p.project_name, p.project_status, p.project_type, p.project_description, p.project_locations, p.project_duration_from, p.project_duration_to, c.category_name as types, sc.category_name as category, ssc.category_name as category_sub');
      $this->db->from('fb_post_requirements pr');
      $this->db->join('fb_projects p','on pr.project_id = p.id', 'LEFT');
      $this->db->join('fb_category_master c','on pr.category_id = c.id', 'LEFT');
      $this->db->join('fb_sub_category_master sc','on pr.sub_category_id = sc.id', 'LEFT');
      $this->db->join('fb_sub_sub_category_master ssc','pr.sub_sub_category_id = ssc.id', 'LEFT');
      $this->db->join('fb_user u','pr.producer_id = u.id', 'LEFT');
      $this->db->where('pr.isDrafted', 'No');
      $this->db->where_not_in('pr.requirement_status', 'Closed');

      if($limit==5){
        $this->db->limit($limit);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function get_requirement_details(){
      $this->db->select('pr.*, u.first_name, u.last_name, u.company_name, p.project_name, p.project_status, p.project_type, p.project_description, p.project_locations, p.project_duration_from, p.project_duration_to, c.category_name as types, sc.category_name as category, ssc.category_name as category_sub');
      $this->db->from('fb_post_requirements pr');
      $this->db->join('fb_projects p','pr.project_id = p.id', 'LEFT');
      $this->db->join('fb_category_master c','pr.category_id = c.id', 'LEFT');
      $this->db->join('fb_sub_category_master sc','pr.sub_category_id = sc.id', 'LEFT');
      $this->db->join('fb_sub_sub_category_master ssc','pr.sub_sub_category_id = ssc.id', 'LEFT');
      $this->db->join('fb_user u','pr.producer_id = u.id', 'LEFT');
      $this->db->where('pr.isDrafted', 'No');
      $this->db->where_not_in('pr.requirement_status', 'Closed');
      $this->db->where('pr.id', $this->uri->segment(3));
      $query = $this->db->get();
      return $query->row();
    }
}
