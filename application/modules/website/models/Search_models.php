<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products_model
 *
 * @author shiv
 */
class Search_models extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

            /** * Function to get title of youtube video by its video id */
        function get_youtube_title($ref) {
            @$json = file_get_contents('http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=' . $ref . '&format=json'); //get JSON video details
            $details = json_decode($json, true); //parse the JSON into an array
            return $details['title']; //return the video title
        }

        /** * Function to get youtube video id from youtube urls */
        function get_youtube_id($url) {

            // $fetch=explode("v=", $url);
            // $videoid=$fetch[1];

            // http://youtu.be/dQw4w9WgXcQ
            // http://www.youtube.com/embed/dQw4w9WgXcQ
            // http://www.youtube.com/watch?v=dQw4w9WgXcQ
            // http://www.youtube.com/?v=dQw4w9WgXcQ
            // http://www.youtube.com/v/dQw4w9WgXcQ
            // http://www.youtube.com/e/dQw4w9WgXcQ
            // http://www.youtube.com/user/username#p/u/11/dQw4w9WgXcQ
            // http://www.youtube.com/sandalsResorts#p/c/54B8C800269D7C1B/0/dQw4w9WgXcQ
            // http://www.youtube.com/watch?feature=player_embedded&v=dQw4w9WgXcQ
            // http://www.youtube.com/?feature=player_embedded&v=dQw4w9WgXcQ
            // It also works on the youtube-nocookie.com URL with the same above options.
            // It will also pull the ID from the URL in an embed code (both iframe and object tags)

            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
            @$youtube_id = $match[1];
            return $youtube_id;
        }

    function get_user_by_id($user_id){
      $this->db->where('id', $user_id);
      return $this->db->get('fb_user')->row();
    }
    function search_list($type, $category = "", $category_sub = "",$limit="Yes", $per_page, $offset){
      if($type == "Locations"){

        $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category,e.subCategory as category_sub_id, a.first_name, a.last_name, a.email, a.mobile, b.*');
        $this->db->from('fb_user a');
        $this->db->join('fb_user_location_details b','a.id=b.user_id', 'INNER');
        $this->db->join('fb_user_category e','e.category_id=b.category_id and b.user_id=e.user_id', 'LEFT');
        $this->db->join('fb_category_master d','e.mainCategory=d.id', 'LEFT');
        $this->db->join('fb_sub_category_master c','c.id=e.subCategory', 'LEFT');
        $this->db->where('a.is_account_blocked', '0');
        $this->db->where('a.is_deleted', '0');
        //$this->db->where('b.category_type', 'subCatid');
        $this->db->where_not_in('a.profile_image', '');
        if($type != "" && $type != "ALL"){
            $this->db->like('d.category_name', $type, 'both');
        }
        if($category != ""){
            $this->db->like('c.category_name', $category, 'both');
        }
        if($this->input->get('gender')){
          if($this->input->get('gender') == 'male'){
            $this->db->where('a.gender', 1);
          }
            if($this->input->get('gender') == 'female'){
              $this->db->where('a.gender', 2);
            }
              if($this->input->get('gender') == 'others'){
                $this->db->where('a.gender', 3);
              }
        }
        if(!empty($this->input->get("city_id"))){
          $this->db->where('a.city', $this->input->get("city_id"));
        }
        if($this->input->get("order") == "asc"){
          $this->db->order_by('a.created', 'asc');
        }else{
          $this->db->order_by('a.created', 'desc');
        }
        if($limit=="Yes"){
            $this->db->limit($per_page, $offset);
        }

        $query = $this->db->get();
        return $query->result();
      }else if($type == "Services"){

        $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category,e.subCategory as category_sub_id, a.first_name, a.last_name, a.email, a.mobile, b.*');
        $this->db->from('fb_user a');
        $this->db->join('fb_user_service_details b','a.id=b.user_id', 'INNER');
        $this->db->join('fb_user_category e','e.category_id=b.category_id and b.user_id=e.user_id', 'LEFT');
        $this->db->join('fb_category_master d','e.mainCategory=d.id', 'LEFT');
        $this->db->join('fb_sub_category_master c','c.id=e.subCategory', 'LEFT');
        $this->db->where('a.is_account_blocked', '0');
        $this->db->where('a.is_deleted', '0');
        //$this->db->where('b.category_type', 'subCatid');
        $this->db->where_not_in('a.profile_image', '');
        if($type != "" && $type != "ALL"){
            $this->db->like('d.category_name', $type, 'both');
        }
        if($category != ""){
            $this->db->like('c.category_name', $category, 'both');
        }
        if($this->input->get('gender')){
          if($this->input->get('gender') == 'male'){
            $this->db->where('a.gender', 1);
          }
            if($this->input->get('gender') == 'female'){
              $this->db->where('a.gender', 2);
            }
              if($this->input->get('gender') == 'others'){
                $this->db->where('a.gender', 3);
              }
        }
        if(!empty($this->input->get("city_id"))){
          $this->db->where('a.city', $this->input->get("city_id"));
        }
        if($this->input->get("order") == "asc"){
          $this->db->order_by('a.created', 'asc');
        }else{
          $this->db->order_by('a.created', 'desc');
        }
        if($limit=="Yes"){
            $this->db->limit($per_page, $offset);
        }

        $query = $this->db->get();
        return $query->result();
      }else{
        /*$this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category,e.subCategory as category_sub_id, a.first_name, a.last_name, a.email, a.mobile, b.*');
        $this->db->from('fb_user a');
        $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
        $this->db->join('fb_user_category e','e.category_id=b.category_id and b.user_id=e.user_id', 'LEFT');
        $this->db->join('fb_category_master d','e.mainCategory=d.id', 'LEFT');
        $this->db->join('fb_sub_category_master c','c.id=e.subCategory', 'LEFT');*/
        /*$this->db->select('a.id as user_id,
        b.id as pr_id,d.id as types_id,
        d.category_name as types,
        c.id as category_id, c.category_name as category,
        e.subSubCategory as category_sub_id, a.first_name, a.last_name, a.email, a.mobile, b.*');
        $this->db->from('fb_user a');
        $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
        $this->db->join('fb_user_category e','e.category_id=b.category_id and b.user_id=e.user_id', 'INNER');
        $this->db->join('fb_category_master d','e.mainCategory=d.id', 'INNER');
        $this->db->join('fb_sub_category_master c','c.id=e.subCategory', 'INNER');
        $this->db->where('a.is_account_blocked', '0');
        $this->db->where('a.is_deleted', '0');*/
        //$this->db->where('b.category_type', 'subCatid');
        /*$this->db->select('a.id as user_id,b.id as pr_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, b.*, b.category_id as category_sub_id, a.profile_image as apr, b.profile_image as bpr');
        $this->db->from('fb_user a');
        $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
        $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
        $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
        $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
        $this->db->where('a.is_account_blocked', '0');
        $this->db->where('a.is_deleted', '0');
        $this->db->where('b.category_type', 'subsubCatid');
        $this->db->where_not_in('a.profile_image', '');
        if($type != "" && $type != "ALL"){
            $this->db->like('d.category_name', $type, 'both');
        }
        if($category != ""){
            $this->db->like('c.category_name', $category, 'both');
        }
        if($this->input->get('gender')){
          if($this->input->get('gender') == 'male'){
            $this->db->where('a.gender', 1);
          }
            if($this->input->get('gender') == 'female'){
              $this->db->where('a.gender', 2);
            }
              if($this->input->get('gender') == 'others'){
                $this->db->where('a.gender', 3);
              }
        }
        if(!empty($this->input->get("city_id"))){
          $this->db->where('a.city', $this->input->get("city_id"));
        }
        if($this->input->get("order") == "asc"){
          $this->db->order_by('b.created', 'asc');
        }else{
          $this->db->order_by('b.created', 'desc');
        }
        if($limit=="Yes"){
            $this->db->limit($per_page, $offset);
        }

        $query = $this->db->get();
        $result = $query->result();*/
        $this->db->select('a.id as user_id,b.id as pr_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, a.first_name, a.last_name, b.*, b.category_id as category_sub_id, a.profile_image as apr, b.profile_image as bpr');
        $this->db->from('fb_user a');
        $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
        $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
        $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
        $this->db->where('a.is_account_blocked', '0');
        $this->db->where('a.is_deleted', '0');
        //$this->db->where('b.category_type', 'subCatid');
        $this->db->where("(a.profile_image!='' OR b.profile_image!='')");

        if($type != "" && $type != "ALL"){
            $this->db->like('d.category_name', $type, 'both');
        }
        if($category != ""){
            $this->db->like('c.category_name', $category, 'both');
        }
        if($this->input->get('gender')){
          if($this->input->get('gender') == 'male'){
            $this->db->where('a.gender', 1);
          }
            if($this->input->get('gender') == 'female'){
              $this->db->where('a.gender', 2);
            }
              if($this->input->get('gender') == 'others'){
                $this->db->where('a.gender', 3);
              }
        }
        if(!empty($this->input->get("city_id"))){
          $this->db->where('a.city', $this->input->get("city_id"));
        }
        if($this->input->get("order") == "asc"){
          $this->db->order_by('b.created', 'asc');
        }else{
          $this->db->order_by('b.created', 'desc');
        }
        if($limit=="Yes"){
            $this->db->limit($per_page, $offset);
        }
        $query2 = $this->db->get();
        $result2 = $query2->result();
        return $result2;
      }
      //echo count($query->result());
      //echo "<pre>";
      //print_r($query->result());
      //echo $this->db->last_query();
    }
    function get_user_profile_by_user_id($id, $profile_id, $type_id, $category_id, $category_sub_id){
      if($category_sub_id==0){
        $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, a.*, b.*, b.cover_pic as cp');
        $this->db->from('fb_user a');
        $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
        $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
        $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
        $this->db->where('b.category_type', 'subCatid');
        //$this->db->where('d.id', $this->uri->segment(5));
        //$this->db->where('c.id', $this->uri->segment(6));
        $this->db->where('a.id', $id);
        $this->db->where('b.id', $profile_id);
        $query = $this->db->get();
        return $query->row();
      }else{
        $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.*, b.*');
        $this->db->from('fb_user a');
        $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
        $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
        $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
        $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
        $this->db->where('b.category_type', 'subsubCatid');
        //$this->db->where('d.id', $this->uri->segment(5));
        //$this->db->where('c.id', $this->uri->segment(6));
        if($category_sub_id !== 0){
            $this->db->where('e.id', $category_sub_id);
        }
        $this->db->where('a.id', $id);
        $this->db->where('b.id', $profile_id);
        $query = $this->db->get();
        return $query->row();
      }
    }

        function get_user_profile_services_by_user_id($id, $profile_id, $type_id, $category_id, $category_sub_id){
          $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile,a.profile_no, b.*');
          $this->db->from('fb_user a');
          $this->db->join('fb_user_service_details b','a.id=b.user_id', 'INNER');
          $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
          $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
          $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
          $this->db->where('a.is_account_blocked', '0');
          $this->db->where('a.is_deleted', '0');
          $this->db->where('b.category_type', 'subsubCatid');
          $this->db->where_not_in('b.profile_image', '');
            //$this->db->where('d.id', $this->uri->segment(5));
            //$this->db->where('c.id', $this->uri->segment(6));
            if($category_sub_id !== 0){
                $this->db->where('e.id', $category_sub_id);
            }
            $this->db->where('a.id', $id);
            $this->db->where('b.id', $profile_id);
            $query = $this->db->get();
            return $query->row();
        }
            function get_user_profile_locations_by_user_id($id, $profile_id, $type_id, $category_id, $category_sub_id){
                $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile,a.profile_no, b.*');
                $this->db->from('fb_user a');
                $this->db->join('fb_user_location_details b','a.id=b.user_id', 'INNER');
                $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                $this->db->where('a.is_account_blocked', '0');
                $this->db->where('a.is_deleted', '0');
                $this->db->where('b.category_type', 'subsubCatid');
                $this->db->where_not_in('b.profile_image', '');
                //$this->db->where('d.id', $this->uri->segment(5));
                //$this->db->where('c.id', $this->uri->segment(6));
                if($category_sub_id !== 0){
                    $this->db->where('e.id', $category_sub_id);
                }
                $this->db->where('a.id', $id);
                $this->db->where('b.id', $profile_id);
                $query = $this->db->get();
                return $query->row();
            }
    function get_user_languages_by_user_id($id, $profile_id){
      $this->db->where('user_id', $id);
      $this->db->where('profile_id', $profile_id);
      return $this->db->get('fb_user_languages_known')->result();
    }
        function get_user_interest_by_user_id($id, $profile_id){
          $this->db->where('user_id', $id);
          $this->db->where('profile_id', $profile_id);
          return $this->db->get('fb_user_interested_in')->result();
        }

            function get_user_genres_by_user_id($id, $profile_id){
              $this->db->where('user_id', $id);
              $this->db->where('profile_id', $profile_id);
              return $this->db->get('fb_user_genres')->result();
            }
                function get_user_worked_for_by_user_id($id, $profile_id){
                  $this->db->where('user_id', $id);
                  //$this->db->where('profile_id', $this->uri->segment(4));
                  return $this->db->get('fb_user_worked_for')->result();
                }
                    function get_user_roles_played_by_user_id($id, $profile_id){
                      $this->db->where('user_id', $id);
                      $this->db->where('profile_id', $profile_id);
                      return $this->db->get('fb_user_roles_played')->result();
                    }
                        function get_user_service_locations_by_user_id($id, $profile_id){
                          $this->db->where('user_id', $id);
                          //$this->db->where('profile_id', $this->uri->segment(4));
                          return $this->db->get('fb_user_service_locations')->result();
                        }
                            function get_user_audio_links_by_user_id($id, $profile_id){
                              $this->db->where('user_id', $id);
                              $this->db->where('profile_id', $profile_id);
                              return $this->db->get('fb_user_audio_links')->result();
                            }
                                function get_user_video_links_by_user_id($id, $profile_id){
                                  $this->db->where('user_id', $id);
                                  //$this->db->where('profile_id', $this->uri->segment(4));
                                  return $this->db->get('fb_user_video_links')->result();
                                }
                                    function get_user_photos_by_user_id($id, $profile_id){
                                      $this->db->where('user_id', $id);
                                      //$this->db->where('profile_id', $this->uri->segment(4));
                                      $this->db->where('is_drafted', 'No');
                                      return $this->db->get('fb_user_photos')->result();
                                    }
                                    function get_user_location_photos_by_user_id($id, $profile_id){
                                      $this->db->where('user_id', $id);
                                      $this->db->where('profile_id', $profile_id);
                                      $this->db->where('is_drafted', 'No');
                                      return $this->db->get('fb_user_location_photos')->result();
                                    }
                                    function get_user_services_photos_by_user_id($id, $profile_id){
                                      $this->db->where('user_id', $id);
                                      $this->db->where('profile_id', $profile_id);
                                      $this->db->where('is_drafted', 'No');
                                      return $this->db->get('fb_user_service_photos')->result();
                                    }

}
