<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- page-content  -->
        <main class="page-content">
            <div id="overlay" class="overlay"></div>
            <div class="container-fluid p-0 m-0">
                <div class="row p-0 m-0">
                    <div class="col-md-12">
                       	<div class="row mb-5">										
							<div class="col-md-12 p-0 m-0">
								<div class="row" style="background:#e9ecef !important;">
									<div class="col-md-6">
										<nav aria-label="breadcrumb">
											<ol class="breadcrumb rounded-0 mb-0 mt-0">
											<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
											</ol>
										</nav>
									</div>
									<div class="col-md-6 text-right">
										<div class="btn-group btn-group-sm my-2 mr-2" role="group">                  
											<a class="btn btn-info text-white" data-toggle="modal" data-target="#addnewModal">
											<i class="fas fa-plus-square text-white"></i>&nbsp;&nbsp;Add New
											</a>
										
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 p-0 m-0 main-content">
								<?php if($this->session->flashdata('message')) {$message =$this->session->flashdata('message'); ?>
								<div class="<?php echo $message['class'] ?>" role="alert">
									<strong><?php echo $message['type'] ?>!</strong> <?php echo $message['message'] ?>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<script type="application/javascript">
									/** After windod Load */
									$(window).bind("load", function() {
									window.setTimeout(function() {
										$(".alert").fadeTo(500, 0).slideUp(500, function(){
											$(this).remove();
										});
									}, 10000);
									});
									</script>
								<?php }?>
								<div class="row m-0 p-0 justify-content-center mb-5">
									
									<div class="col-md-6 px-4 my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Sales Revenue</h4>
										</div>
										<div class="chart col-md-12 p-0 pt-2">
											<canvas id="areaChart" class="w-100"></canvas>
										</div>
									</div>
									<div class="col-md-6 px-4  my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Enquiry Growth</h4>
										</div>
										<div class="chart col-md-12 p-0 pt-2">
											<canvas id="lineChart" class="w-100"></canvas>
										</div>
									</div>
									<div class="col-md-6 px-4 my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Enquiry Source</h4>
										</div>
										<div class="chart col-md-12 p-0 pt-2">
											<canvas id="donutChart" class="w-100"></canvas>
										</div>
									</div>
									
									<div class="col-md-6 px-4 my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Enquiry Status</h4>
										</div>
										<div class="chart col-md-12 p-0 pt-2">
											<canvas id="pieChart" class="w-100"></canvas>
										</div>
									</div>
									
									<div class="col-md-6 px-4 my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Enquiry Conversion</h4>
										</div>
										<div class="chart col-md-12 p-0">
											<canvas id="barChart" class="w-100"></canvas>
										</div>
									</div>
									
									<div class="col-md-6 px-4 my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Enquiry Lost</h4>
										</div>
										<div class="chart col-md-12 p-0">
											<canvas id="stackedBarChart" class="w-100"></canvas>
										</div>
									</div>
									<div class="col-md-6 px-4 my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Product Sold</h4>
										</div>
										<div class="col-md-12 table-responsive p-0">
											<table class="table table-striped table-hover">
											<thead class="thead-dark">
												<th>Product</th>
												<th>Monthly</th>
												<th>Weekly</th>
												<th>Today</th>
												<th>Expected</th>
											</thead>
											<tbody>
												<?php for($i=1;$i<8;$i++){?>
												<tr>
												
												<td>PRDID000000<?php echo $i;?></td>
												<td>121</td>
												<td>111</td>
												<td>111</td>
												<td>111</td>
												</tr>
												<?php }?>
											</tbody>
											</table>
										</div>
									</div>
									
									<div class="col-md-6 px-4 my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Todays Follow Up</h4>
										</div>
										<div class="col-md-12 table-responsive p-0">
											<table class="table table-striped table-hover">
											<thead class="thead-dark">
												<th>Enquiry ID</th>
												<th>Status</th>
												<th>Follow Up</th>
												<th>Action</th>
											</thead>
											<tbody>
												<?php for($i=1;$i<7;$i++){?>
												<tr>
												
												<td>ENQ000000<?php echo $i;?></td>
												<td>Open</td>
												<td><?php echo date('Y-m-d');?></td>
												<td>
													<div class="btn-group btn-group-sm" role="group" aria-label="...">
													<button type="button" class="btn btn-info" title="Call Now"><i class="fas fa-phone-square"></i></button>
													<button type="button" class="btn btn-info" title="Send SMS"><i class="fas fa-sms"></i></button>
													<button type="button" class="btn btn-info" title="Send Email"><i class="fas fa-envelope-open-text"></i></button>
													<button type="button" class="btn btn-info" title="View Details"><i class="far fa-list-alt"></i></button>  
													</div>
												</td>
												</tr>
												<?php }?>
											</tbody>
											</table>
										</div>
									</div>
									
									<div class="col-md-12 px-4 my-3 row">
										<div class="col-md-12 mt-2 bg-dark text-white py-3">
											<h4>:: Latest Transactions</h4>
										</div>
										<div class="col-md-12 p-0 table-responsive">
											<table class="table table-striped table-hover">
											<thead class="thead-dark">
												<th>Invoice ID</th>
												<th>Enquiry ID</th>
												<th>Invoice Date</th>
												<th>Amount</th>
												<th>Status</th>
												<th>Print</th>
											</thead> 
											<tbody>
												<?php for($i=1;$i<7;$i++){?>
												<tr>
												<td>INV000000<?php echo $i;?></td>
												<td>ENQ000000<?php echo $i;?></td>
												<td><?php echo date('Y-m-d');?></td>
												<td>123456</td>
												<td>Paid</td>
												<td>
													<div class="btn-group btn-group-sm" role="group" aria-label="...">
													<button type="button" class="btn btn-info" title="Call Now"><i class="fas fa-phone-square"></i></button>
													<button type="button" class="btn btn-info" title="Send SMS"><i class="fas fa-sms"></i></button>
													<button type="button" class="btn btn-info" title="Send Email"><i class="fas fa-envelope-open-text"></i></button>
													<button type="button" class="btn btn-info" title="Print"><i class="fas fa-print"></i></button> 
													<button type="button" class="btn btn-info" title="View Details"><i class="far fa-list-alt"></i></button>  
													</div>
												</td>
												</tr>
												<?php }?>
											</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
          
						</div>
                    </div>
                    <div class="col-md-12 px-4 text-center">
						<nav class="navbar fixed-bottom navbar-light bg-dark text-white text-center">
							<a class="w-100"><?php echo $copyright;?></a>
						</nav>
                    </div>
                </div>
            </div>
        </main>
        <!-- page-content" -->
		<!-- ChartJS -->
		<script src="<?php echo base_url();?>assets/vendor-library/chart.js/Chart.min.js"></script>
          <script>
          $(function () {
            
            var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

            var areaChartData = {
              labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
              datasets: [
                {
                  label               : 'Sales',
                  backgroundColor     : 'rgba(60,141,188,0.9)',
                  borderColor         : 'rgba(60,141,188,0.8)',
                  pointRadius          : false,
                  pointColor          : '#3b8bba',
                  pointStrokeColor    : 'rgba(60,141,188,1)',
                  pointHighlightFill  : '#fff',
                  pointHighlightStroke: 'rgba(60,141,188,1)',
                  data                : [28, 48, 40, 19, 86, 27, 90]
                },
                {
                  label               : 'Purchase',
                  backgroundColor     : 'rgba(210, 214, 222, 1)',
                  borderColor         : 'rgba(210, 214, 222, 1)',
                  pointRadius         : false,
                  pointColor          : 'rgba(210, 214, 222, 1)',
                  pointStrokeColor    : '#c1c7d1',
                  pointHighlightFill  : '#fff',
                  pointHighlightStroke: 'rgba(220,220,220,1)',
                  data                : [65, 59, 80, 81, 56, 55, 40]
                },
              ]
            }

            var areaChartOptions = {
              maintainAspectRatio : false,
              responsive : true,
              legend: {
                display: true
              },
              scales: {
                xAxes: [{
                  gridLines : {
                    display : false,
                  }
                }],
                yAxes: [{
                  gridLines : {
                    display : true,
                  }
                }]
              }
            }


            var areaChart       = new Chart(areaChartCanvas, { 
              type: 'line',
              data: areaChartData, 
              options: areaChartOptions
            })

            var lineChartOptions = {
              maintainAspectRatio : false,
              responsive : true,
              legend: {
                display: true
              },
              scales: {
                xAxes: [{
                  gridLines : {
                    display : false,
                  }
                }],
                yAxes: [{
                  gridLines : {
                    display : true,
                  }
                }]
              }
			}
			
            var areaChartData = {
              labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
              datasets: [
                {
                  label               : 'Converted',
                  backgroundColor     : 'rgba(60,141,188,0.9)',
                  borderColor         : 'rgba(60,141,188,0.8)',
                  pointRadius          : false,
                  pointColor          : '#3b8bba',
                  pointStrokeColor    : 'rgba(60,141,188,1)',
                  pointHighlightFill  : '#fff',
                  pointHighlightStroke: 'rgba(60,141,188,1)',
                  data                : [28, 48, 40, 19, 86, 27, 90]
                },
                {
                  label               : 'Incoming',
                  backgroundColor     : 'rgba(210, 214, 222, 1)',
                  borderColor         : 'rgba(210, 214, 222, 1)',
                  pointRadius         : false,
                  pointColor          : 'rgba(210, 214, 222, 1)',
                  pointStrokeColor    : '#c1c7d1',
                  pointHighlightFill  : '#fff',
                  pointHighlightStroke: 'rgba(220,220,220,1)',
                  data                : [65, 59, 80, 81, 56, 55, 40]
                },
              ]
            }
            
            var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
            var lineChartOptions = jQuery.extend(true, {}, lineChartOptions)
            var lineChartData = jQuery.extend(true, {}, areaChartData)
            lineChartData.datasets[0].fill = false;
            lineChartData.datasets[1].fill = false;
            lineChartOptions.datasetFill = false


            var lineChart = new Chart(lineChartCanvas, { 
              type: 'line',
              data: lineChartData, 
              options: lineChartOptions
            })

            var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
            var donutData        = {
              labels: [
                  'Direct', 
                  'FB',
                  'Addwords', 
                  'Referal', 
                  'Instagram', 
                  'Others', 
              ],
              datasets: [
                {
                  data: [700,500,400,600,300,100],
                  backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                }
              ]
            }
            var donutOptions     = {
              maintainAspectRatio : false,
              responsive : true,
              legend: {
                display: false
              },
            }
            
            var donutChart = new Chart(donutChartCanvas, {
              type: 'bar',
              data: donutData,
              options: donutOptions      
            })
            var donutData        = {
              labels: [
                  'Pending', 
                  'Open',
                  'Processing', 
                  'On Hold', 
                  'Completed', 
                  'Others', 
              ],
              datasets: [
                {
                  data: [700,500,400,600,300,100],
                  backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                }
              ]
            }
            
            var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
            var pieData        = donutData;
            var pieOptions     = {
              maintainAspectRatio : false,
              responsive : true,
            }
          
            var pieChart = new Chart(pieChartCanvas, {
              type: 'pie',
              data: pieData,
              options: pieOptions      
            })

            var barChartCanvas = $('#barChart').get(0).getContext('2d')
            var barChartData = jQuery.extend(true, {}, areaChartData)
            var temp0 = areaChartData.datasets[0]
            var temp1 = areaChartData.datasets[1]
            barChartData.datasets[0] = temp1
            barChartData.datasets[1] = temp0

            var barChartOptions = {
              responsive              : true,
              maintainAspectRatio     : false,
              datasetFill             : false
            }

            var barChart = new Chart(barChartCanvas, {
              type: 'bar', 
              data: barChartData,
              options: barChartOptions
            })

            var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
            var stackedBarChartData = jQuery.extend(true, {}, barChartData)

            var stackedBarChartOptions = {
              responsive              : true,
              maintainAspectRatio     : false,
              scales: {
                xAxes: [{
                  stacked: false,
                }],
                yAxes: [{
                  stacked: true
                }]
              }
            }

            var stackedBarChart = new Chart(stackedBarChartCanvas, {
              type: 'bar', 
              data: stackedBarChartData,
              options: stackedBarChartOptions
            })
          })
        </script>
