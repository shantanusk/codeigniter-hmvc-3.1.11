<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products_model
 *
 * @author shiv
 */
class Cart_models extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_product_image_by_id($product_id){
        $sql = "SELECT * from pr_product_images where product_id='".$product_id."' order by created_time desc";        
        $query = $this->db->query($sql);  
        return $query->row();
    }
    function get_cart_list(){
        $total_weight = 0;
                    foreach ($this->cart->contents() as $items){
                        if($items['units'] == "KG" || $items['units'] == "Kg"){
                            $total_weight = $total_weight +$items['qty'];
                        }else if($items['units'] == "Units"){
                            $total_weight += (($items['qty']*100)/1000);
                        }
                    }
        $i = 1;
        $html ="";
        $html .= '
            <h5 class="pb-2">Shopping Basket</h5>';
            $html .= form_open(base_url('cart/update')).'
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>QTY</th>
                                        <th><span class="fa fa-inr"></span></th>
                                        <th>Tax</th>
                                        <th class="text-right">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                foreach ($this->cart->contents() as $items){
                                    $html .= '<tr>
                                                <td>
                                                    '.form_hidden($i.'[rowid]', $items['rowid']).'
                                                    <a href="'.base_url('store/cart/remove/'.$items['rowid']).'">
                                                        <span class="fa fa-trash" style="color: #FF0000;"></span>
                                                        '.$items['name'].'</a>';
                                                        if ($this->cart->has_options($items['rowid']) == TRUE){
                                                            $html .='<p>';
                                                            foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value){

                                                                    $html .='<strong>'.$option_name.':</strong> '.$option_value.'<br />';
                                                                
                                                            }   
                                                             $html .='</p>';
                                                        }
                                                $html .='</p>
                                                </td>
                                                <td>'.$items['qty'].' '.$items['units'].'</td>
                                                <td>';
                                                if($items['tax']>0){
                                                    $html .=round($items['price']-($items['price']/($items['tax']+100)*$items['tax']),2,PHP_ROUND_HALF_UP);
                                                }else{
                                                    $html .= $this->cart->format_number($items['price']);
                                                } 
                                                $html .='
                                        </td>
                                                <td>'.$this->cart->format_number($items['tax']).' %</td>
                                                <td style="text-align:right">'.$this->cart->format_number($items['subtotal']).'</td>
                                        </tr>';
                                        $i++;

                                }
                            $html .= '
                                <tr>
                                        <td colspan="3"> <strong>Grand Total</strong></td>
                                        <td colspan="2" class="text-right"><span class="fa fa-inr"></span> '.$this->cart->format_number($this->cart->total()).'</td>
                                </tr>

                                </tbody>
                            </table>

                        <p>';
                        /*form_submit('', 'Update Cart', "class='btn btn-primary m-1'"); */
                        $logged_in_data = $this->session->userdata('logged_in_data');
                        if($logged_in_data['customer_type_name'] == "Premium" && $this->cart->total()>=150){
                            $html .='<a href="'.base_url('checkout').'" class="btn btn-primary m-1">Place Order</a><br /><br />
                                <small class="alert-warning p-2"><span class="fa fa-inr"></span> 150 Minimum Order Quantity</small>';
                        }else if($logged_in_data['customer_type_name'] != "Premium" && $this->cart->total()>=150){
                            
                            if($this->cart->total()<=300){
                                $html .='<a href="'.base_url('store/cart/checkout').'" class="btn btn-primary">Place Order</a><br /><br />
                                <small class="alert-warning p-2"><span class="fa fa-inr"></span> 150 Minimum Order Quantity</small>';
                            }else{

                                $html .='<a href="'.base_url('store/cart/checkout').'" class="btn btn-primary">Place Order</a><br /><br />
                                <small class="alert-warning p-2"><span class="fa fa-inr"></span> 150 Minimum Order Quantity</small>';
                            }
                        }else{
                            
                                if($this->cart->total()<=300){

                                    $html .='<a href="'.base_url('checkout').'" class="btn btn-primary disabled">Place Order</a><br /><br />
                                <small class="alert-warning p-2"><span class="fa fa-inr"></span> 150 Minimum Order Quantity</small>';
                                }else{

                                    $html .='<a href="'.base_url('store/checkout').'" class="btn btn-primary disabled">Place Order</a>';
                                }
                            
                        }
                        $html .='</p>
                          </form>';
                          echo $html;
    }
     /*Product Grid List View*/
     public function get_product_grid_view($start, $offset, $cat = "All", $col=2){
        if($cat == "All"){
            $list = $this->get_product_store_list_p($start, $offset);
        }else{
           $list =  $this->get_product_store_list_category($cat);
        }
        $i=1;
        $grid_view = '<div class="row justify-content-center">';
        $total = count((array)$list);
        if($total>0){
        foreach ($list as $product){
            $img = "";
            $image = $this->get_product_image_by_id($product->product_id);
            if(isset($image->file_new_name)){
                /*if($this->agent->is_mobile() && $offset ==0){
                    $img = base_url('assets/img/product/thumb/'.$image->file_new_name);
                }else{*/
                    $img = base_url('assets/uploads/product/mobile/'.$image->file_new_name);    
                //}
                
            }else{
                $img = base_url('assets/img/product/image_not_available.jpg');
            }
            $grid_view .= '<div class="col-md-'.$col.' col-6 col-sm-6 mt-4 p-0">
                <div class="m-2 bg-white shadow">
                <form method="post" action="'.base_url("store/cart/add").'">';
                    $data = array(
                        'id'  => $product->product_id,
                        'price'   => $product->unit_price,
                        'name'   => ucwords(strtolower($product->product_name)),
                        'units'   => $product->unit_name,
                        'unit_id'   => $product->units_id,
                        'tax'   => ($product->taxes==NULL)? 0:$product->taxes
                    );

                    $grid_view .= form_hidden($data);
                    $grid_view .= '
                <img class="card-img-top" src="'.$img.'" alt="'.$product->product_name.'">
                              <div class="card-body text-center" style="min-height:206px;">
                                  <a href="'.base_url(str_replace(" ","-",strtolower($product->category_name)).'/'.str_replace(" ", "-", strtolower($product->product_name))).'" style="color: #424242;">
                                  <h5 class="card-title text-center">
                                    '.ucwords(strtolower($product->product_name)).'
                                    </h5>
                                  <h5 class="card-title mt-1 text-center">'.$product->product_name_unicode.'</h5>
                                  <h6 class="text-justify mt-3" style="font-size: 15px !important;">
                                    ';
                                        if($product->unit_market_price >0 ){
                                    $grid_view .= '
                                    Our Price: <span class="fa fa-inr"></span> '.$product->unit_price.' / '.ucwords(strtolower($product->unit_name));
                                    $grid_view .= "<br />Market Price: <s><span class='fa fa-inr'></span> ".$product->unit_market_price.' / ';
                                            if($product->unit_name == "Units"){$grid_view .= "Unit";}else{$grid_view .= ucwords(strtolower($product->unit_name));}
                                            $grid_view .=  '</s>
                                    <br />You Save: <span style="color: #FF0000;">'.round(((($product->unit_market_price-$product->unit_price)*100)/$product->unit_market_price),0,PHP_ROUND_HALF_UP).'%</span>
                                    ';
                                    }else{
                                        $grid_view .= '
                                            Our Price: <span class="fa fa-inr"></span> '.$product->unit_price.' / '.ucwords(strtolower($product->unit_name));
                                     }
                                     $grid_view .= '
                                    </h6>
                                  </a>
                                  <p class="card-text text-justify">
                                      ';
                                      if(strlen($product->short_description)>60){
                                        $grid_view .= ucwords(strtolower(substr($product->short_description,0,60))).'...';
                                      }else{
                                        $grid_view .= ucwords(strtolower(substr($product->short_description,0,60)));
                                      }
                                      $grid_view .= '
                                  </p>
                                  
                                </div>
                                <div class="card-footer text-center">';
                                if($product->unit_name == "Units"){ 
                                    $grid_view .= '
                                  <select class="custom-select mt-3" name="unit" id="unit">
                                      <option value="1">1 Unit(s)</option>
                                      <option value="2">2 Unit(s)</option>
                                      <option value="3">3 Unit(s)</option>
                                      <option value="4">4 Unit(s)</option>
                                      <option value="5">5 Unit(s)</option>
                                      <option value="6">6 Unit(s)</option>
                                      <option value="7">7 Unit(s)</option>
                                      <option value="8">8 Unit(s)</option>
                                      <option value="9">9 Unit(s)</option>
                                      <option value="10">10 Unit(s)</option>
                                  </select>';
                                  }else{
                                    $grid_view .= '<select class="custom-select mt-3" name="unit" id="unit">';
                                    if($product->min_weight == '0.10'){
                                      
                                          $grid_view .= '
                                      <option value="0.100">100 Gram</option>
                                      <option value="0.250">250 Gram</option>
                                      <option value="0.500">500 Gram</option>';  
                                    }
                                    if($product->min_weight == '0.25'){
                                        
                                          $grid_view .= '
                                      <option value="0.250">250 Gram</option>
                                      <option value="0.500">500 Gram</option>';
                                    }if($product->min_weight == '0.50'){
                                        
                                          $grid_view .= '
                                      <option value="0.500">500 Gram</option>';
                                    }
                                    
                                          $grid_view .= '
                                      <option value="1" selected>1 Kg</option>
                                      <option value="2">2 Kg</option>
                                      <option value="3">3 Kg</option>
                                      <option value="4">4 Kg</option>
                                      <option value="5">5 Kg</option>
                                      <option value="6">6 Kg</option>
                                      <option value="7">7 Kg</option>
                                      <option value="8">8 Kg</option>
                                      <option value="9">9 Kg</option>
                                      <option value="10">10 Kg</option>';
                                    $grid_view .= '</select>';
                                   }
                                   $onclick = "ga('send', 'event', 'Store', 'Add To Box', '".$product->product_name."');";
                                  $grid_view .= '<button onclick="'.$onclick.'" type="submit" class="btn btn-primary mt-3 mb-3">Add To Box</button>
                              </div>
                </form>
                </div>
                            </div>';
        }
        $grid_view .= '<div class="col-md-12 text-center mt-3">
                        '.$this->pagination->create_links().'
                    </div></div>';
        echo $grid_view;
        }else{
            $grid_view .= '<div class="col-md-6 col-6 col-sm-6 mt-4 p-0">
                <div class="m-2 bg-white shadow">';
                    $grid_view .= '
                              <div class="card-body text-center">
                                  <h6 class="card-title text-center">
                                    '.ucwords(strtolower("No Products Found!")).'
                                    </h6>
                                    <hr style="border: 2px solid #fd8a36;width:5%;">
                                  <p class="text-justify">
                                      '.ucwords(strtolower("Currently There Is No Products In ".ucwords(str_replace("-"," ",strtolower($this->uri->segment(1))))." Category. ")).'
                                  </p>';
                                  $grid_view .= '
                </div>
                            </div>';
        echo $grid_view;
        }
        //print_r($list);
    }
    public function get_product_store_list_p($start, $offset){
        if($offset<0){
            $offset=0;
        }
        //$sql = "SELECT a.*, b.category_name, c.stock_status_name, d.unit_name FROM `pr_product` a inner join pr_category b on a.category_id=b.category_id INNER join pr_stock_status c ON a.stock_status=c.stock_status_id INNER JOIN pr_units d on a.units_id=d.unit_id where a.category_id!='3' and a.is_active_for_sale !='0' order by b.category_name desc limit $offset, $start"; // order by rand()        
        $sql = "SELECT a.*, (CASE WHEN a.category_id='1' THEN 'Vegetables' 
        WHEN a.category_id='2' THEN 'Snacks' 
        WHEN a.category_id='3' THEN 'Membership' 
        WHEN a.category_id='4' THEN 'Fruits' 
        WHEN a.category_id='5' THEN 'Exotic Products'
        WHEN a.category_id='6' THEN 'Ayurvedic Products'
        WHEN a.category_id='8' THEN 'Rice'
        WHEN a.category_id='9' THEN 'Spices'
        WHEN a.category_id='10' THEN 'Flours'
        WHEN a.category_id='11' THEN 'Non Veg Products'
        WHEN a.category_id='13' THEN 'Milk Based Products'
        WHEN a.category_id='14' THEN 'Seasonal Products' 
        WHEN a.category_id='17' THEN 'Pulses' 
        WHEN a.category_id='18' THEN 'Pickles'
        WHEN a.category_id='19' THEN 'Oil And Ghee'
        WHEN a.category_id='20' THEN 'Processed Rice'
        WHEN a.category_id='21' THEN 'Sweeteners' 
        WHEN a.category_id='22' THEN ' 	Exotic Vegetables'
        END) as 'category_name', (CASE when a.units_id=1 then 'KG' when a.units_id=3 then 'Units' ELSE 0 END) as 'unit_name', COUNT(b.product_id) as 'quantity' FROM `pr_product` a LEFT JOIN sales_order_details b on b.product_id=a.product_id where a.is_active_for_sale =1 and a.domain like 'www.kalingafresh.com' GROUP by product_id order by COUNT(b.product_id) DESC limit $offset, $start";
        $query = $this->db->query($sql);  
       return $query->result();
    }

    public function get_product_store_list_category($category = "vegetables"){
        $c_id= 1;
        
        if($category == "Rice"){
            $c_id= 8;
        }
        if($category == "Pulses"){
            $c_id= 17;
        }
        if($category == "Snacks"){
            $c_id= 2;
        }
        if($category == "Spices"){
            $c_id= 9;
        }
        if($category == "Pickles"){
            $c_id= 18;
        }
        if($category == "Flours"){
            $c_id= 10;
        }
        if($category == "Oil And Ghee"){
            $c_id= 19;
        }
        if($category == "Processed Rice"){
            $c_id= 20;
        }
        if($category == "Sweeteners"){
            $c_id= 21;
        }
        if($category == "Vegetables"){
            $c_id= 1;
        }
        if($category == "Fruits"){
            $c_id= 4;
        }
        if($category == "Exotic Vegetables"){
            $c_id= 22;
        }
        if($category == "Seasonal Products"){
            $c_id= 14;
        }
        if($category == "Non Veg Products"){
            $c_id= 11;
        }
        if($category == "Milk Based Products"){
            $c_id= 13;
        }
        if($category == "Membership"){
            $c_id= 3;
        }
        if($category == "Exotic Products"){
            $c_id= 5;
        }
        if($category == "Ayurvedic Products"){
            $c_id= 6;
        }
        
        //$sql = "SELECT a.*, b.category_name, c.stock_status_name, d.unit_name FROM `pr_product` a inner join pr_category b on a.category_id=b.category_id INNER join pr_stock_status c ON a.stock_status=c.stock_status_id INNER JOIN pr_units d on a.units_id=d.unit_id where b.category_name='".$category."' and a.is_active_for_sale !='0' order by product_name";        
        $sql = "SELECT a.*, (CASE WHEN a.category_id='1' THEN 'Vegetables' 
        WHEN a.category_id='2' THEN 'Snacks' 
        WHEN a.category_id='3' THEN 'Membership' 
        WHEN a.category_id='4' THEN 'Fruits' 
        WHEN a.category_id='5' THEN 'Exotic Products'
        WHEN a.category_id='6' THEN 'Ayurvedic Products'
        WHEN a.category_id='8' THEN 'Rice'
        WHEN a.category_id='9' THEN 'Spices'
        WHEN a.category_id='10' THEN 'Flours'
        WHEN a.category_id='11' THEN 'Non Veg Products'
        WHEN a.category_id='13' THEN 'Milk Based Products'
        WHEN a.category_id='14' THEN 'Seasonal Products' 
        WHEN a.category_id='17' THEN 'Pulses' 
        WHEN a.category_id='18' THEN 'Pickles'
        WHEN a.category_id='19' THEN 'Oil And Ghee'
        WHEN a.category_id='20' THEN 'Processed Rice'
        WHEN a.category_id='21' THEN 'Sweeteners' 
        WHEN a.category_id='22' THEN 'Exotic Vegetables'
        END) as 'category_name', (CASE when a.units_id=1 then 'KG' when a.units_id=3 then 'Units' ELSE 0 END) as 'unit_name', COUNT(b.product_id) as 'quantity' FROM `pr_product` a LEFT JOIN sales_order_details b on b.product_id=a.product_id where a.category_id in(".$c_id.") and a.is_active_for_sale =1 and a.domain like 'www.kalingafresh.com' GROUP by product_id order by COUNT(b.product_id) DESC";
        $query = $this->db->query($sql);  
        return $query->result();
    }
}