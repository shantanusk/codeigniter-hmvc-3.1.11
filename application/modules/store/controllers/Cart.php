<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends Frontend {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
    protected $data = array();
	public function index()
	{
		$this->data['page'] = "Store";
		$p=24;
            if(!empty($this->uri->segment(2))){
                $offset = (int)$this->uri->segment(2)-1;
                $offset = (($offset*$p));
            }else{
                $offset = 0;
            }
            $this->data['offset'] = $offset;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'/store/';
            $this->db->where("is_active_for_sale", 1);
            $this->db->where("domain", "www.kalingafresh.com");
        	$config['total_rows'] = $this->db->get('pr_product')->num_rows();
        	$config['per_page'] = $p;
        	$this->data['per_page'] = $p;
        	$config['num_links'] = 1;
        	$config['use_page_numbers'] = TRUE;
            $this->pagination->initialize($config);
		$view = array(		
			'shop/product-grid-view'			
		);
		$this->template($view, $this->data, true);
	}
	function add()
	{
        // Set array for send data.
        $price = ($this->input->post('price')*$this->input->post('unit'));
		$insert_data = array(
			'id' => $this->input->post('id'),
			'name' => $this->input->post('name'),
			'price' => $this->input->post('price'),
			'qty' => $this->input->post('unit'),
			'units' => $this->input->post('units'),
			'unit_id' => $this->input->post('unit_id'),
			'tax' => $this->input->post('tax')
		);		

                 // This function add items into cart.
		$this->cart->insert($insert_data);
	      
                // This will show insert data in cart.
		$url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url();
		redirect($url);
		//print_r($_POST);
		//print_r($this->cart->contents());
	}
	
	function remove($rowid) {
        // Check rowid value.
		if ($rowid==="all"){
            // Destroy data which store in  session.
			$this->cart->destroy();
		}else{
            // Destroy selected rowid in session.
			$data = array(
				'rowid'   => $rowid,
				'qty'     => 0
			);
            // Update cart data, after cancle.
			$this->cart->update($data);
		}
		
        // This will show cancle data in cart.
		$url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url();
		redirect($url);
	}
	
	function update(){
        //echo "<pre>";
        //print_r($_POST);
        $cart_info =  $_POST;
 		foreach( $cart_info as $id => $cart)
		{	
            $rowid = $cart['rowid'];
            $qty = $cart['qty'];
            $data = array(
				'rowid'   => $rowid,
				'qty'     => $qty
			);
            //print_r($data);
            $this->cart->update($data);
		}
	
		$url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url();
		redirect($url);
	}
	public function checkout()
	{
		$this->data['page'] = "Store";
		$p=24;
            if(!empty($this->uri->segment(2))){
                $offset = (int)$this->uri->segment(2)-1;
                $offset = (($offset*$p));
            }else{
                $offset = 0;
            }
            $this->data['offset'] = $offset;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'/store/';
            $this->db->where("is_active_for_sale", 1);
            $this->db->where("domain", "www.kalingafresh.com");
        	$config['total_rows'] = $this->db->get('pr_product')->num_rows();
        	$config['per_page'] = $p;
        	$this->data['per_page'] = $p;
        	$config['num_links'] = 1;
        	$config['use_page_numbers'] = TRUE;
            $this->pagination->initialize($config);
		$view = array(		
			'shop/product-grid-view'			
		);
		$this->template($view, $this->data, true);
	}
}
