<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends Frontend {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
    protected $data = array();
	public function index()
	{
		$this->data['page'] = "Store";
		$p=24;
            if(!empty($this->uri->segment(2))){
                $offset = (int)$this->uri->segment(2)-1;
                $offset = (($offset*$p));
            }else{
                $offset = 0;
            }
            $this->data['offset'] = $offset;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'/store/';
            $this->db->where("is_active_for_sale", 1);
            $this->db->where("domain", "www.kalingafresh.com");
        	$config['total_rows'] = $this->db->get('pr_product')->num_rows();
        	$config['per_page'] = $p;
        	$this->data['per_page'] = $p;
        	$config['num_links'] = 1;
        	$config['use_page_numbers'] = TRUE;
            $this->pagination->initialize($config);
		$view = array(
			'shop/product-grid-view'			
		);
		$this->template($view, $this->data, true);
	}
}
