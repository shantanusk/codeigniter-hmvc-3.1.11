<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
    <!-- Main content -->
    <section class="container-fluid">
        <!--<input class="form-control" id="myInput" type="text" placeholder="Search..">-->
        <?php   if($this->cart->format_number($this->cart->total())==0){
                    if($page == "Category Wise List"){
                        $this->CRT->get_product_grid_view($per_page, $offset, $category);            
                    }else{

                        $this->CRT->get_product_grid_view($per_page, $offset);
                    }
                }else{
                    ?>
                    <div class="row justify-content-center">
                        <?php if(!$this->agent->is_mobile()){?>
                            <div class="col-md-8 col-6">
                                <?php if($page == "Category Wise List"){
                                        $this->CRT->get_product_grid_view($per_page, $offset, $category, 3);            
                                    }else{
                                        $this->CRT->get_product_grid_view($per_page, $offset, "All", 3);
                                    }?>
                            </div>
                            <div class="col-md-4 col-6 bg-white mt-4 p-4">
                                <?php if($this->cart->total()>0){$this->CRT->get_cart_list();}?>
                            
                            </div>
                        <?php }else{?>
                <div class="col-12 bg-white mt-4 p-4">
                  <?php if($this->cart->format_number($this->cart->total())>0){$this->Cart->get_cart_list();}?>
                            
                </div>
                <div class="col-12">
                    <?php if($page == "Category Wise List"){
                        $this->CRT->get_product_grid_view($per_page, $offset, $category);            
                    }else{
                        $this->CRT->get_product_grid_view($per_page, $offset);
                    }?>
                </div>
                <?php }?>
                    </div>
                    <?php
                }
        ?>
    </section>
