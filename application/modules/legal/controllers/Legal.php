<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Legal extends Frontend {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
     protected $page ="Legal";
     function __construct(){
       parent::__construct();
       $CI =& get_instance();
     }

	public function terms_conditions()
	{
    $this->data['page'] = $this->page = "Terms & Conditions";
    $this->data['legal'] = $this->COM->legal();
    $this->data['page_data'] = "terms_conditions";
		$view = array(
      'terms-and-conditions'
		);
		$this->template($view, $this->data, true);
	}
  	public function privacy_policy()
  	{
      $this->data['page'] = $this->page = "Privacy Policy";
      $this->data['legal'] = $this->COM->legal();
      $this->data['page_data'] = "privacy_policy";
  		$view = array(
        'terms-and-conditions'
  		);
  		$this->template($view, $this->data, true);
  	}
    	public function disclaimer()
    	{
        $this->data['page'] = $this->page = "Disclaimer";
        $this->data['legal'] = $this->COM->legal();
        $this->data['page_data'] = "disclaimer";
    		$view = array(
          'terms-and-conditions'
    		);
    		$this->template($view, $this->data, true);
    	}
}
