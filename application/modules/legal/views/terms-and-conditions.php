<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="container-fluid m-0 p-0 border-top-green">
  <div class="row m-0 p-0 justify-content-center">

      <div class="col-md-10 col-12 m-0 p-0 mb-5 text-justify">
        <h3 class="text-green font-weight-bold mt-3"><?php echo $page;?></h3>
        <hr class="border-bottom-green" />
        <div class="row px-4">

          <div class="col-12">
            <?php
            echo $legal[0][$page_data];?>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
