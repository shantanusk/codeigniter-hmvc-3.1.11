<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Common
 *
 * @author shiv
 */
class Common extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function get_count($table) {
    		return $this->db->count_all($table);
    	}
    	public function get_pagination_list($table, $limit, $start) {
    		$this->db->limit($limit, $start);
    		$query = $this->db->get($table);

    		return $query->result();
    	}

    	public function get_pagination_custom_query_list($query) {
    		//$this->db->limit($limit, $start);
    		$query = $this->db->query($query);

    		return $query->result();
    	}
      function get_country_list(){
        $query = $this->db->get('fb_country_master');
        return $query->result();
      }
    public function get_banner(){
        $this->db->where('active', 'Yes');
        $query = $this->db->get('fb_banner_master');
        return $query->result();
    }
    public function get_active_region(){
        $this->db->where('active', 'Yes');
        $query = $this->db->get('fb_region_master');
        return $query->result();
    }
    public function get_active_type(){
        $this->db->where('active', 'Yes');
        $query = $this->db->get('fb_category_master');
        return $query->result();
    }
    public function get_active_category_by_id($type_id){
        $this->db->where('active', 'Yes');
        $this->db->where('category_id', $type_id);
        $query = $this->db->get('fb_sub_category_master');
        return $query->result();
    }

    public function get_active_category(){
        $this->db->where('active', 'Yes');
        $query = $this->db->get('fb_sub_category_master');
        return $query->result();
    }
    function get_active_sub_category_by_id($category_id){
      $query = $this->db->query("select * from fb_sub_sub_category_master where subcategory_id='" . $category_id . "' and active='Yes' order by category_name");
      return $query->result();
    }

    function get_active_sub_category(){
      $query = $this->db->query("select * from fb_sub_sub_category_master where active='Yes' order by category_name");
      return $query->result();
    }

    function get_city(){
      $query = $this->db->query("select * from fb_city_master where is_showInListing='Yes' order by city_name");
      return $query->result();
    }
    function reviews(){
      $query = $this->db->query('select sr.*, u.first_name, u.last_name, u.profile_image as seller_image FROM fb_seller_review as sr LEFT JOIN fb_user as u ON u.id=sr.seller_id order by sr.star_rating desc limit 0,5');
      return $query->result();
    }

    function get_project_location($locations){
      //$query = $this->db->query('select city_name as project_locations from fb_city_master where id IN ('.$locations.')');
      $this->db->select('city_name');
      $this->db->where_in('id', explode(",",$locations));
      $query = $this->db->get('fb_city_master');
      //print_r($query->result_array());
      $citys = array();
      foreach ($query->result() as $city) {
        $citys[] = $city->city_name;
      }
      return implode(", ",$citys);
    }

    function get_social_link(){
      $query = $this->db->query('select * from fb_contact_us_social');
      return $query->result();
    }


        function before_filmboard_content(){
          $query = $this->db->query('select * from fb_before_filmboard_content');
          return $query->row();
        }


            function after_filmboard_content(){
              $query = $this->db->query('select * from fb_after_filmboard_content');
              return $query->result();
            }

            function faq_banner(){
              $query = $this->db->query('select * from fb_faq_banner_content');
              return $query->result();
            }

            function contact(){
              $query = $this->db->query('select * from fb_contact_us_master');
              return $query->row();
            }

            function founder(){
              $query = $this->db->query("select * from fb_founders_master where active='Yes'");
              return $query->result();
            }

            function strategic_advisors(){
              $query = $this->db->query("select * from fb_strategic_advisors_master where active='Yes' order by view_order asc");
              return $query->result();
            }

            function career(){
              $query = $this->db->query("select * from fb_career_content");
              return $query->row();
            }

            function vacancy(){
              $query = $this->db->query("select * from fb_vacancy_master where active='Yes'");
              return $query->result();
            }
            function media(){
              $query = $this->db->query("select * from fb_news_media_master");
              return $query->row();
            }

            function news(){
              $query = $this->db->query("select * from fb_news_media_feeds where active='Yes'");
              return $query->result();
            }

            function legal(){
              $query = $this->db->query("select * from fb_legal_pages");
              return $query->result_array();
            }
            function all_jobs(){
              $query = $this->db->query("select pr.*, u.first_name, u.last_name, u.company_name, p.project_name, p.project_status, p.project_type, p.project_description, p.project_locations, p.project_duration_from, p.project_duration_to, c.category_name as main_category_name, sc.category_name as sub_category_name, ssc.category_name as sub_sub_category_name from fb_post_requirements as pr left join fb_projects as p on pr.project_id = p.id left join fb_category_master as c on pr.category_id = c.id left join fb_sub_category_master as sc on pr.sub_category_id = sc.id left join fb_sub_sub_category_master as ssc on pr.sub_sub_category_id = ssc.id left join fb_user as u on pr.producer_id = u.id where pr.isDrafted = 'No' and pr.requirement_status != 'Closed'  order by pr.created desc");
              return $query->result();
            }

            function get_job_by_id($id){
              $query = $this->db->query("select pr.*, u.first_name, u.last_name, u.company_name, p.project_name, p.project_status, p.project_type, p.project_description, p.project_locations, p.project_duration_from, p.project_duration_to, c.category_name as main_category_name, sc.category_name as sub_category_name, ssc.category_name as sub_sub_category_name from fb_post_requirements as pr left join fb_projects as p on pr.project_id = p.id left join fb_category_master as c on pr.category_id = c.id left join fb_sub_category_master as sc on pr.sub_category_id = sc.id left join fb_sub_sub_category_master as ssc on pr.sub_sub_category_id = ssc.id left join fb_user as u on pr.producer_id = u.id where pr.isDrafted = 'No' and pr.requirement_status != 'Closed' and pr.id='".$id."'  order by pr.created desc");
              return $query->result();
            }

            function get_bids_by_id($job_id){
              $query = $this->db->query("select fb_user_applied_jobs.suggested_rate, fb_user.first_name, fb_user.last_name from fb_user_applied_jobs
											inner join fb_user on fb_user.id=fb_user_applied_jobs.seller_id
											where fb_user_applied_jobs.requirement_id='".$job_id."' order by suggested_rate asc");
                      return $query->result();
            }

            function get_active_users($category, $category_sub, $category_sub_sub, $page, $limit){
              /*SELECT a.*, b.*, c.category_name as type, d.category_name as category, e.category_name as category_sub FROM `fb_user` a inner join fb_user_profile_details b on a.id=b.user_id LEFT JOIN fb_user_category f on a.id=f.user_id INNER JOIN fb_category_master c on f.mainCategory=c.id LEFT JOIN fb_sub_category_master d on f.subCategory=d.id LEFT JOIN fb_sub_sub_category_master e on f.subSubCategory=e.id  where a.profile_image!='' and a.is_account_blocked=0 and a.is_deleted=0 and b.category_id!='' and b.description!='' limit 100*/
              $query = $this->db->query("select uc.*, f.category_id, c.category_name as type, d.category_name as category, e.category_name as category_sub from fb_user uc INNER JOIN fb_user_category f on uc.id=f.user_id INNER JOIN fb_category_master c on f.mainCategory=c.id INNER JOIN fb_sub_category_master d on f.subCategory=d.id LEFT JOIN fb_sub_sub_category_master e on f.subSubCategory=e.id");
              return $query->result();
            }

            function get_user_profile($user_id, $category_id){
              // upd.category_id='".$listDetail['category_id']."' and upd.category_type='".$listDetail['category_type']."'
              $query = $this->db->query("SELECT * FROM fb_user_profile_details where user_id='".$user_id."' and category_id='".$category_id."'");
              return $query->row();
            }

            function get_type_id_by_name($type_name){
              $this->db->select('id');
              $this->db->where_in('category_name', $type_name);
              $query = $this->db->get('fb_category_master');
              return $query->row()->id;
            }

            function get_category_id_by_permalink(){
              $this->db->select('id');
              $this->db->where('permalink', $this->uri->segment(2).'/'.$this->uri->segment(3));
              $query = $this->db->get('fb_sub_category_master');
              return $query->row()->id;
            }
            function get_category_id_by_name($category){
              $this->db->select('id');
              $this->db->like('category_name', $category);
              $query = $this->db->get('fb_sub_category_master');
              return $query->row()->id;
            }

            function get_category_sub_id_by_permalink(){
              $this->db->select('id');
              $this->db->where('permalink', $this->uri->segment(2).'/'.$this->uri->segment(3).(!$this->uri->segment(4) == "details" ? '/'.$this->uri->segment(4):''));
              $query = $this->db->get('fb_sub_sub_category_master');
              return $query->row()->id;
            }
            function get_category_sub_id_by_name($category_sub){
              $this->db->select('id');
              $this->db->where_in('category_name', $category_sub);
              $query = $this->db->get('fb_sub_sub_category_master');
              return $query->row()->id;
            }


            function get_category_sub_name_by_id($category_sub){
              $this->db->select('category_name');
              $this->db->where_in('id', $category_sub);
              $query = $this->db->get('fb_sub_sub_category_master');
              return $query->row()->category_name;
            }

            function get_user_profile_search($type, $category = "", $category_sub = "", $depth=All, $per_page, $offset){
              $result = array();
              if($type == "Talent" || $type == "Crew"){
                if($depth==3){
                  $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, b.*');
                  $this->db->from('fb_user a');
                  $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                  $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                  $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                  $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                  $this->db->where('a.is_account_blocked', '0');
                  $this->db->where('a.is_deleted', '0');
                  $this->db->where('b.category_type', 'subsubCatid');
                  $this->db->where_not_in('b.profile_image', '');
                  if($type != "" && $type != "ALL"){
                      $this->db->like('d.category_name', $type, 'both');
                  }
                  if($category != ""){
                      $this->db->like('c.category_name', $category, 'both');
                  }
                  if($category_sub != ""){
                      $this->db->like('e.category_name', $category_sub, 'both');
                  }
                  if($this->input->get('gender')){
                    if($this->input->get('gender') == 'male'){
                      $this->db->where('a.gender', 1);
                    }
                      if($this->input->get('gender') == 'female'){
                        $this->db->where('a.gender', 2);
                      }
                        if($this->input->get('gender') == 'others'){
                          $this->db->where('a.gender', 3);
                        }
                  }
                  if(!empty($this->input->get("city_id"))){
                    $this->db->where('a.city', $this->input->get("city_id"));
                  }
                  if($this->input->get("order") == "asc"){
                    $this->db->order_by('b.created', 'asc');
                  }else{
                    $this->db->order_by('b.created', 'desc');
                  }
                  $this->db->limit($per_page, $offset);
                  $query = $this->db->get();
		              $result = $query->result();
                }elseif($depth==2){
                  /*SELECT a.id, b.user_id, d.category_name as types
                  FROM `fb_user` a INNER join fb_user_profile_details b on a.id=b.user_id
                  LEFT JOIN fb_user_category c on c.category_id=b.category_id and b.user_id=c.user_id
                  LEFT join fb_category_master d on c.mainCategory=d.id                  */
                    $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                    $this->db->from('fb_user a');
                    $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                    $this->db->join('fb_user_category e','e.category_id=b.category_id and b.user_id=e.user_id', 'LEFT');
                    $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
                    $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                    $this->db->where('a.is_account_blocked', '0');
                    $this->db->where('a.is_deleted', '0');
                    $this->db->where('b.category_type', 'subCatid');
                    $this->db->where_not_in('b.profile_image', '');
                    /*$this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                    $this->db->from('fb_user a');
                    $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                    $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
                    $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                    $this->db->where('a.is_account_blocked', '0');
                    $this->db->where('a.is_deleted', '0');
                    $this->db->where('b.category_type', 'subCatid');
                    $this->db->where_not_in('b.profile_image', '');*/
                    if($type != "" && $type != "ALL"){
                        $this->db->like('d.category_name', $type, 'both');
                    }
                    if($category != ""){
                        $this->db->like('c.category_name', $category, 'both');
                    }
                    if($this->input->get('gender')){
                      if($this->input->get('gender') == 'male'){
                        $this->db->where('a.gender', 1);
                      }
                        if($this->input->get('gender') == 'female'){
                          $this->db->where('a.gender', 2);
                        }
                          if($this->input->get('gender') == 'others'){
                            $this->db->where('a.gender', 3);
                          }
                    }
                    if(!empty($this->input->get("city_id"))){
                      $this->db->where('a.city', $this->input->get("city_id"));
                    }
                    if($this->input->get("order") == "asc"){
                      $this->db->order_by('b.created', 'asc');
                    }else{
                      $this->db->order_by('b.created', 'desc');
                    }
                    $this->db->limit($per_page, $offset);
                    $query = $this->db->get();
  		              $result = $query->result();
                }else{
                  $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, b.*');
                  $this->db->from('fb_user a');
                  $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                  $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                  $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                  $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                  $this->db->where('a.is_account_blocked', '0');
                  $this->db->where('a.is_deleted', '0');
                  $this->db->where('b.category_type', 'subsubCatid');
                  $this->db->where_not_in('b.profile_image', '');
                  if($type != "" && $type != "ALL"){
                      $this->db->like('d.category_name', $type, 'both');
                  }
                  if($category != ""){
                      $this->db->like('c.category_name', $category, 'both');
                  }
                  if($category_sub != ""){
                      $this->db->like('e.category_name', $category_sub, 'both');
                  }
                  if($this->input->get('gender')){
                    if($this->input->get('gender') == 'male'){
                      $this->db->where('a.gender', 1);
                    }
                      if($this->input->get('gender') == 'female'){
                        $this->db->where('a.gender', 2);
                      }
                        if($this->input->get('gender') == 'others'){
                          $this->db->where('a.gender', 3);
                        }
                  }
                  if(!empty($this->input->get("city_id"))){
                    $this->db->where('a.city', $this->input->get("city_id"));
                  }
                  if($this->input->get("order") == "asc"){
                    $this->db->order_by('b.created', 'asc');
                  }else{
                    $this->db->order_by('b.created', 'desc');
                  }
                  $this->db->limit($per_page, $offset);
                  $query1 = $this->db->get();
		              $result1 = $query1->result();
                  $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, a.first_name, a.last_name, a.email, a.mobile, b.*');
                  $this->db->from('fb_user a');
                  $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                  $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
                  $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                  $this->db->where('a.is_account_blocked', '0');
                  $this->db->where('a.is_deleted', '0');
                  $this->db->where('b.category_type', 'subCatid');
                  $this->db->where_not_in('b.profile_image', '');
                  if($type != "" && $type != "ALL"){
                      $this->db->like('d.category_name', $type, 'both');
                  }
                  if($category != ""){
                      $this->db->like('c.category_name', $category, 'both');
                  }
                  if($this->input->get('gender')){
                    if($this->input->get('gender') == 'male'){
                      $this->db->where('a.gender', 1);
                    }
                      if($this->input->get('gender') == 'female'){
                        $this->db->where('a.gender', 2);
                      }
                        if($this->input->get('gender') == 'others'){
                          $this->db->where('a.gender', 3);
                        }
                  }
                  if(!empty($this->input->get("city_id"))){
                    $this->db->where('a.city', $this->input->get("city_id"));
                  }
                  if($this->input->get("order") == "asc"){
                    $this->db->order_by('b.created', 'asc');
                  }else{
                    $this->db->order_by('b.created', 'desc');
                  }
                  $this->db->limit($per_page, $offset);
                  $query2 = $this->db->get();
                  $result2 = $query2->result();
                  $result = array_merge($result1, $result2);
                }
              }else if($type == "Locations"){
                $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                $this->db->from('fb_user a');
                $this->db->join('fb_user_location_details b','a.id=b.user_id', 'INNER');
                $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                $this->db->where('a.is_account_blocked', '0');
                $this->db->where('a.is_deleted', '0');
                $this->db->where('b.category_type', 'subsubCatid');
                $this->db->where_not_in('b.profile_image', '');
                if($type != "" && $type != "ALL"){
                    $this->db->like('d.category_name', $type, 'both');
                }
                if($category != ""){
                    $this->db->like('c.category_name', $category, 'both');
                }
                if($category_sub != ""){
                    $this->db->like('e.category_name', $category_sub, 'both');
                }
                if(!empty($this->input->get("city_id"))){
                  $this->db->where('a.city', $this->input->get("city_id"));
                }
                if($this->input->get("order") == "asc"){
                  $this->db->order_by('b.created', 'asc');
                }else{
                  $this->db->order_by('b.created', 'desc');
                }
                $this->db->limit($per_page, $offset);
                $query = $this->db->get();
                $result = $query->result();
              }else if($type == "Services"){
                $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                $this->db->from('fb_user a');
                $this->db->join('fb_user_service_details b','a.id=b.user_id', 'INNER');
                $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                $this->db->where('a.is_account_blocked', '0');
                $this->db->where('a.is_deleted', '0');
                $this->db->where('b.category_type', 'subsubCatid');
                $this->db->where_not_in('b.profile_image', '');
                if($type != "" && $type != "ALL"){
                    $this->db->like('d.category_name', $type, 'both');
                }
                if($category != ""){
                    $this->db->like('c.category_name', $category, 'both');
                }
                if($category_sub != ""){
                    $this->db->like('e.category_name', $category_sub, 'both');
                }
                if(!empty($this->input->get("city_id"))){
                  $this->db->where('a.city', $this->input->get("city_id"));
                }
                if($this->input->get("order") == "asc"){
                  $this->db->order_by('b.created', 'asc');
                }else{
                  $this->db->order_by('b.created', 'desc');
                }
                $this->db->limit($per_page, $offset);
                $query = $this->db->get();
                $result = $query->result();

              }else{
                $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, b.*');
                $this->db->from('fb_user a');
                $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                $this->db->where('a.is_account_blocked', '0');
                $this->db->where('a.is_deleted', '0');
                $this->db->where('b.category_type', 'subsubCatid');
                $this->db->where_not_in('b.profile_image', '');
                if($type != "" && $type != "ALL"){
                    $this->db->like('d.category_name', $type, 'both');
                }
                if($category != ""){
                    $this->db->like('c.category_name', $category, 'both');
                }
                if($category_sub != ""){
                    $this->db->like('e.category_name', $category_sub, 'both');
                }
                if($this->input->get('gender')){
                  if($this->input->get('gender') == 'male'){
                    $this->db->where('a.gender', 1);
                  }
                    if($this->input->get('gender') == 'female'){
                      $this->db->where('a.gender', 2);
                    }
                      if($this->input->get('gender') == 'others'){
                        $this->db->where('a.gender', 3);
                      }
                }
                if(!empty($this->input->get("city_id"))){
                  $this->db->where('a.city', $this->input->get("city_id"));
                }
                if($this->input->get("order") == "asc"){
                  $this->db->order_by('b.created', 'asc');
                }else{
                  $this->db->order_by('b.created', 'desc');
                }
                $this->db->limit($per_page, $offset);
                $query1 = $this->db->get();
                $result1 = $query1->result();
                $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, a.first_name, a.last_name, a.email, a.mobile, b.*');
                $this->db->from('fb_user a');
                $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
                $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                $this->db->where('a.is_account_blocked', '0');
                $this->db->where('a.is_deleted', '0');
                $this->db->where('b.category_type', 'subCatid');
                $this->db->where_not_in('b.profile_image', '');
                if($type != "" && $type != "ALL"){
                    $this->db->like('d.category_name', $type, 'both');
                }
                if($category != ""){
                    $this->db->like('c.category_name', $category, 'both');
                }
                if($this->input->get('gender')){
                  if($this->input->get('gender') == 'male'){
                    $this->db->where('a.gender', 1);
                  }
                    if($this->input->get('gender') == 'female'){
                      $this->db->where('a.gender', 2);
                    }
                      if($this->input->get('gender') == 'others'){
                        $this->db->where('a.gender', 3);
                      }
                }
                if(!empty($this->input->get("city_id"))){
                  $this->db->where('a.city', $this->input->get("city_id"));
                }
                if($this->input->get("order") == "asc"){
                  $this->db->order_by('b.created', 'asc');
                }else{
                  $this->db->order_by('b.created', 'desc');
                }
                $this->db->limit($per_page, $offset);
                $query2 = $this->db->get();
                $result2 = $query2->result();
                $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                $this->db->from('fb_user a');
                $this->db->join('fb_user_location_details b','a.id=b.user_id', 'INNER');
                $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                $this->db->where('a.is_account_blocked', '0');
                $this->db->where('a.is_deleted', '0');
                $this->db->where('b.category_type', 'subsubCatid');
                $this->db->where_not_in('b.profile_image', '');
                if($type != "" && $type != "ALL"){
                    $this->db->like('d.category_name', $type, 'both');
                }
                if($category != ""){
                    $this->db->like('c.category_name', $category, 'both');
                }
                if($category_sub != ""){
                    $this->db->like('e.category_name', $category_sub, 'both');
                }
                if(!empty($this->input->get("city_id"))){
                  $this->db->where('a.city', $this->input->get("city_id"));
                }
                if($this->input->get("order") == "asc"){
                  $this->db->order_by('b.created', 'asc');
                }else{
                  $this->db->order_by('b.created', 'desc');
                }
                $this->db->limit($per_page, $offset);
                $query3 = $this->db->get();
                $result3 = $query3->result();
                $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                $this->db->from('fb_user a');
                $this->db->join('fb_user_service_details b','a.id=b.user_id', 'INNER');
                $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                $this->db->where('a.is_account_blocked', '0');
                $this->db->where('a.is_deleted', '0');
                $this->db->where('b.category_type', 'subsubCatid');
                $this->db->where_not_in('b.profile_image', '');
                if($type != "" && $type != "ALL"){
                    $this->db->like('d.category_name', $type, 'both');
                }
                if($category != ""){
                    $this->db->like('c.category_name', $category, 'both');
                }
                if($category_sub != ""){
                    $this->db->like('e.category_name', $category_sub, 'both');
                }
                if(!empty($this->input->get("city_id"))){
                  $this->db->where('a.city', $this->input->get("city_id"));
                }
                if($this->input->get("order") == "asc"){
                  $this->db->order_by('b.created', 'asc');
                }else{
                  $this->db->order_by('b.created', 'desc');
                }
                $this->db->limit($per_page, $offset);
                $query4 = $this->db->get();
                $result4 = $query4->result();
                $result = array_merge($result1, $result2, $result3, $result4);
              }
              return $result;

            }

                        function get_user_profile_search_count($type, $category = "", $category_sub = "", $depth=All, $per_page, $offset){
                          $result = array();
                          if($type == "Talent" || $type == "Crew"){
                            if($depth==3){
                              $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, b.*');
                              $this->db->from('fb_user a');
                              $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                              $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                              $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                              $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                              $this->db->where('a.is_account_blocked', '0');
                              $this->db->where('a.is_deleted', '0');
                              $this->db->where('b.category_type', 'subsubCatid');
                              $this->db->where_not_in('b.profile_image', '');
                              if($type != "" && $type != "ALL"){
                                  $this->db->like('d.category_name', $type, 'both');
                              }
                              if($category != ""){
                                  $this->db->like('c.category_name', $category, 'both');
                              }
                              if($category_sub != ""){
                                  $this->db->like('e.category_name', $category_sub, 'both');
                              }
                              if($this->input->get('gender')){
                                if($this->input->get('gender') == 'male'){
                                  $this->db->where('a.gender', 1);
                                }
                                  if($this->input->get('gender') == 'female'){
                                    $this->db->where('a.gender', 2);
                                  }
                                    if($this->input->get('gender') == 'others'){
                                      $this->db->where('a.gender', 3);
                                    }
                              }
                              if(!empty($this->input->get("city_id"))){
                                $this->db->where('a.city', $this->input->get("city_id"));
                              }
                              if($this->input->get("order") == "asc"){
                                $this->db->order_by('b.created', 'asc');
                              }else{
                                $this->db->order_by('b.created', 'desc');
                              }
                              $query = $this->db->get();
            		              $result = $query->result();
                            }elseif($depth==2){
                              $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                              $this->db->from('fb_user a');
                              $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                              $this->db->join('fb_user_category e','e.category_id=b.category_id and b.user_id=e.user_id', 'LEFT');
                              $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
                              $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                              $this->db->where('a.is_account_blocked', '0');
                              $this->db->where('a.is_deleted', '0');
                              $this->db->where('b.category_type', 'subCatid');
                              $this->db->where_not_in('b.profile_image', '');
                                if($type != "" && $type != "ALL"){
                                    $this->db->like('d.category_name', $type, 'both');
                                }
                                if($category != ""){
                                    $this->db->like('c.category_name', $category, 'both');
                                }
                                if($this->input->get('gender')){
                                  if($this->input->get('gender') == 'male'){
                                    $this->db->where('a.gender', 1);
                                  }
                                    if($this->input->get('gender') == 'female'){
                                      $this->db->where('a.gender', 2);
                                    }
                                      if($this->input->get('gender') == 'others'){
                                        $this->db->where('a.gender', 3);
                                      }
                                }
                                if(!empty($this->input->get("city_id"))){
                                  $this->db->where('a.city', $this->input->get("city_id"));
                                }
                                if($this->input->get("order") == "asc"){
                                  $this->db->order_by('b.created', 'asc');
                                }else{
                                  $this->db->order_by('b.created', 'desc');
                                }
                                $query = $this->db->get();
              		              $result = $query->result();
                            }else{
                              $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, b.*');
                              $this->db->from('fb_user a');
                              $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                              $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                              $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                              $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                              $this->db->where('a.is_account_blocked', '0');
                              $this->db->where('a.is_deleted', '0');
                              $this->db->where('b.category_type', 'subsubCatid');
                              $this->db->where_not_in('b.profile_image', '');
                              if($type != "" && $type != "ALL"){
                                  $this->db->like('d.category_name', $type, 'both');
                              }
                              if($category != ""){
                                  $this->db->like('c.category_name', $category, 'both');
                              }
                              if($category_sub != ""){
                                  $this->db->like('e.category_name', $category_sub, 'both');
                              }
                              if($this->input->get('gender')){
                                if($this->input->get('gender') == 'male'){
                                  $this->db->where('a.gender', 1);
                                }
                                  if($this->input->get('gender') == 'female'){
                                    $this->db->where('a.gender', 2);
                                  }
                                    if($this->input->get('gender') == 'others'){
                                      $this->db->where('a.gender', 3);
                                    }
                              }
                              if(!empty($this->input->get("city_id"))){
                                $this->db->where('a.city', $this->input->get("city_id"));
                              }
                              if($this->input->get("order") == "asc"){
                                $this->db->order_by('b.created', 'asc');
                              }else{
                                $this->db->order_by('b.created', 'desc');
                              }
                              $query1 = $this->db->get();
            		              $result1 = $query1->result();
                              $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, a.first_name, a.last_name, a.email, a.mobile, b.*');
                              $this->db->from('fb_user a');
                              $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                              $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
                              $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                              $this->db->where('a.is_account_blocked', '0');
                              $this->db->where('a.is_deleted', '0');
                              $this->db->where('b.category_type', 'subCatid');
                              $this->db->where_not_in('b.profile_image', '');
                              if($type != "" && $type != "ALL"){
                                  $this->db->like('d.category_name', $type, 'both');
                              }
                              if($category != ""){
                                  $this->db->like('c.category_name', $category, 'both');
                              }
                              if($this->input->get('gender')){
                                if($this->input->get('gender') == 'male'){
                                  $this->db->where('a.gender', 1);
                                }
                                  if($this->input->get('gender') == 'female'){
                                    $this->db->where('a.gender', 2);
                                  }
                                    if($this->input->get('gender') == 'others'){
                                      $this->db->where('a.gender', 3);
                                    }
                              }
                              if(!empty($this->input->get("city_id"))){
                                $this->db->where('a.city', $this->input->get("city_id"));
                              }
                              if($this->input->get("order") == "asc"){
                                $this->db->order_by('b.created', 'asc');
                              }else{
                                $this->db->order_by('b.created', 'desc');
                              }
                              $query2 = $this->db->get();
                              $result2 = $query2->result();
                              $result = array_merge($result1, $result2);
                            }
                          }else if($type == "Locations"){
                            $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                            $this->db->from('fb_user a');
                            $this->db->join('fb_user_location_details b','a.id=b.user_id', 'INNER');
                            $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                            $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                            $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                            $this->db->where('a.is_account_blocked', '0');
                            $this->db->where('a.is_deleted', '0');
                            $this->db->where('b.category_type', 'subsubCatid');
                            $this->db->where_not_in('b.profile_image', '');
                            if($type != "" && $type != "ALL"){
                                $this->db->like('d.category_name', $type, 'both');
                            }
                            if($category != ""){
                                $this->db->like('c.category_name', $category, 'both');
                            }
                            if($category_sub != ""){
                                $this->db->like('e.category_name', $category_sub, 'both');
                            }
                            if(!empty($this->input->get("city_id"))){
                              $this->db->where('a.city', $this->input->get("city_id"));
                            }
                            if($this->input->get("order") == "asc"){
                              $this->db->order_by('b.created', 'asc');
                            }else{
                              $this->db->order_by('b.created', 'desc');
                            }
                            $query = $this->db->get();
                            $result = $query->result();
                          }else if($type == "Services"){
                            $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                            $this->db->from('fb_user a');
                            $this->db->join('fb_user_service_details b','a.id=b.user_id', 'INNER');
                            $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                            $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                            $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                            $this->db->where('a.is_account_blocked', '0');
                            $this->db->where('a.is_deleted', '0');
                            $this->db->where('b.category_type', 'subsubCatid');
                            $this->db->where_not_in('b.profile_image', '');
                            if($type != "" && $type != "ALL"){
                                $this->db->like('d.category_name', $type, 'both');
                            }
                            if($category != ""){
                                $this->db->like('c.category_name', $category, 'both');
                            }
                            if($category_sub != ""){
                                $this->db->like('e.category_name', $category_sub, 'both');
                            }
                            if(!empty($this->input->get("city_id"))){
                              $this->db->where('a.city', $this->input->get("city_id"));
                            }
                            if($this->input->get("order") == "asc"){
                              $this->db->order_by('b.created', 'asc');
                            }else{
                              $this->db->order_by('b.created', 'desc');
                            }
                            $query = $this->db->get();
                            $result = $query->result();

                          }else{
                            $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, b.*');
                            $this->db->from('fb_user a');
                            $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                            $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                            $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                            $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                            $this->db->where('a.is_account_blocked', '0');
                            $this->db->where('a.is_deleted', '0');
                            $this->db->where('b.category_type', 'subsubCatid');
                            $this->db->where_not_in('b.profile_image', '');
                            if($type != "" && $type != "ALL"){
                                $this->db->like('d.category_name', $type, 'both');
                            }
                            if($category != ""){
                                $this->db->like('c.category_name', $category, 'both');
                            }
                            if($category_sub != ""){
                                $this->db->like('e.category_name', $category_sub, 'both');
                            }
                            if($this->input->get('gender')){
                              if($this->input->get('gender') == 'male'){
                                $this->db->where('a.gender', 1);
                              }
                                if($this->input->get('gender') == 'female'){
                                  $this->db->where('a.gender', 2);
                                }
                                  if($this->input->get('gender') == 'others'){
                                    $this->db->where('a.gender', 3);
                                  }
                            }
                            if(!empty($this->input->get("city_id"))){
                              $this->db->where('a.city', $this->input->get("city_id"));
                            }
                            if($this->input->get("order") == "asc"){
                              $this->db->order_by('b.created', 'asc');
                            }else{
                              $this->db->order_by('b.created', 'desc');
                            }
                            $query1 = $this->db->get();
                            $result1 = $query1->result();
                            $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, a.first_name, a.last_name, a.email, a.mobile, b.*');
                            $this->db->from('fb_user a');
                            $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                            $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
                            $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                            $this->db->where('a.is_account_blocked', '0');
                            $this->db->where('a.is_deleted', '0');
                            $this->db->where('b.category_type', 'subCatid');
                            $this->db->where_not_in('b.profile_image', '');
                            if($type != "" && $type != "ALL"){
                                $this->db->like('d.category_name', $type, 'both');
                            }
                            if($category != ""){
                                $this->db->like('c.category_name', $category, 'both');
                            }
                            if($this->input->get('gender')){
                              if($this->input->get('gender') == 'male'){
                                $this->db->where('a.gender', 1);
                              }
                                if($this->input->get('gender') == 'female'){
                                  $this->db->where('a.gender', 2);
                                }
                                  if($this->input->get('gender') == 'others'){
                                    $this->db->where('a.gender', 3);
                                  }
                            }
                            if(!empty($this->input->get("city_id"))){
                              $this->db->where('a.city', $this->input->get("city_id"));
                            }
                            if($this->input->get("order") == "asc"){
                              $this->db->order_by('b.created', 'asc');
                            }else{
                              $this->db->order_by('b.created', 'desc');
                            }
                            $query2 = $this->db->get();
                            $result2 = $query2->result();
                            $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                            $this->db->from('fb_user a');
                            $this->db->join('fb_user_location_details b','a.id=b.user_id', 'INNER');
                            $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                            $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                            $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                            $this->db->where('a.is_account_blocked', '0');
                            $this->db->where('a.is_deleted', '0');
                            $this->db->where('b.category_type', 'subsubCatid');
                            $this->db->where_not_in('b.profile_image', '');
                            if($type != "" && $type != "ALL"){
                                $this->db->like('d.category_name', $type, 'both');
                            }
                            if($category != ""){
                                $this->db->like('c.category_name', $category, 'both');
                            }
                            if($category_sub != ""){
                                $this->db->like('e.category_name', $category_sub, 'both');
                            }
                            if(!empty($this->input->get("city_id"))){
                              $this->db->where('a.city', $this->input->get("city_id"));
                            }
                            if($this->input->get("order") == "asc"){
                              $this->db->order_by('b.created', 'asc');
                            }else{
                              $this->db->order_by('b.created', 'desc');
                            }
                            $query3 = $this->db->get();
                            $result3 = $query3->result();
                            $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.email, a.mobile, b.*');
                            $this->db->from('fb_user a');
                            $this->db->join('fb_user_service_details b','a.id=b.user_id', 'INNER');
                            $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                            $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                            $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                            $this->db->where('a.is_account_blocked', '0');
                            $this->db->where('a.is_deleted', '0');
                            $this->db->where('b.category_type', 'subsubCatid');
                            $this->db->where_not_in('b.profile_image', '');
                            if($type != "" && $type != "ALL"){
                                $this->db->like('d.category_name', $type, 'both');
                            }
                            if($category != ""){
                                $this->db->like('c.category_name', $category, 'both');
                            }
                            if($category_sub != ""){
                                $this->db->like('e.category_name', $category_sub, 'both');
                            }
                            if(!empty($this->input->get("city_id"))){
                              $this->db->where('a.city', $this->input->get("city_id"));
                            }
                            if($this->input->get("order") == "asc"){
                              $this->db->order_by('b.created', 'asc');
                            }else{
                              $this->db->order_by('b.created', 'desc');
                            }
                            $query4 = $this->db->get();
                            $result4 = $query4->result();
                            $result = array_merge($result1, $result2, $result3, $result4);
                          }
                          return $result;

                        }


                        function get_active_profile_list(){
                          $this->db->select('a.id as user_id,b.id as pr_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name');
                          $this->db->from('fb_user a');
                          $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                          $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                          $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                          $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                          $this->db->where('a.is_account_blocked', '0');
                          $this->db->where('a.is_deleted', '0');
                          $this->db->where('b.category_type', 'subsubCatid');
                          $this->db->where_not_in('b.profile_image', '');
                          $query1 = $this->db->get();
                          $result1 = $query1->result();
                          $this->db->select('a.id as user_id,b.id as pr_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, a.first_name, a.last_name');
                          $this->db->from('fb_user a');
                          $this->db->join('fb_user_profile_details b','a.id=b.user_id', 'INNER');
                          $this->db->join('fb_sub_category_master c','c.id=b.category_id', 'INNER');
                          $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                          $this->db->where('a.is_account_blocked', '0');
                          $this->db->where('a.is_deleted', '0');
                          $this->db->where('b.category_type', 'subCatid');
                          $this->db->where_not_in('b.profile_image', '');
                          $query2 = $this->db->get();
                          $result2 = $query2->result();
                          $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, b.proeprty_name');
                          $this->db->from('fb_user a');
                          $this->db->join('fb_user_location_details b','a.id=b.user_id', 'INNER');
                          $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                          $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                          $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                          $this->db->where('a.is_account_blocked', '0');
                          $this->db->where('a.is_deleted', '0');
                          $this->db->where('b.category_type', 'subsubCatid');
                          $this->db->where_not_in('b.profile_image', '');
                          $query3 = $this->db->get();
                          $result3 = $query3->result();
                          $this->db->select('a.id as user_id,b.id as pr_id,d.id as types_id, d.category_name as types, c.id as category_id, c.category_name as category, e.id as category_sub_id, e.category_name as category_sub, a.first_name, a.last_name, a.profile_no, b.*');
                          $this->db->from('fb_user a');
                          $this->db->join('fb_user_service_details b','a.id=b.user_id', 'INNER');
                          $this->db->join('fb_sub_sub_category_master e','e.id=b.category_id', 'INNER');
                          $this->db->join('fb_sub_category_master c','c.id=e.subcategory_id', 'INNER');
                          $this->db->join('fb_category_master d','c.category_id=d.id', 'INNER');
                          $this->db->where('a.is_account_blocked', '0');
                          $this->db->where('a.is_deleted', '0');
                          $this->db->where('b.category_type', 'subsubCatid');
                          $this->db->where_not_in('b.profile_image', '');
                          $query4 = $this->db->get();
                          $result4 = $query4->result();
                          $result = array_merge($result1, $result2, $result3, $result4);
                          return $result;
                        }

                        function get_user_type_by_user_id($user_id){
                          $this->db->distinct();
                          $this->db->select('a.mainCategory,b.category_name, b.permalink, b.category_icon');
                          $this->db->from('fb_user_category a');
                          $this->db->join('fb_category_master b', 'a.mainCategory=b.id');
                          $this->db->where('a.user_id', $user_id);
                          $this->db->order_by('b.category_name');
                          $query = $this->db->get();
                          return $query->result();
                        }
                        function get_user_category_by_user_id($user_id, $type_id){
                          $this->db->distinct();
                          $this->db->select('b.category_name, b.permalink, b.category_icon, a.category_type, `a`.mainCategory');
                          $this->db->from('fb_user_category a');
                          $this->db->join('fb_category_master b', 'a.mainCategory=b.id');
                          $this->db->where('a.user_id', $user_id);
                          $this->db->where('a.mainCategory', $type_id);
                          $this->db->order_by('b.category_name');
                          $query = $this->db->get();
                          return $query->result();
                        }


                        function get_user_sub_category_by_user_id($user_id, $type_id, $category_type){

                          if($category_type == "subCatId"){
                            $this->db->select('b.category_name, b.permalink');
                            $this->db->from('fb_user_category a');
                            $this->db->join('fb_sub_category_master b', 'b.id=a.subCategory');
                            $this->db->where('a.user_id', $user_id);
                            $this->db->where('a.mainCategory', $type_id);
                            $this->db->where('category_type', 'subCatid');
                            $this->db->order_by('b.category_name');
                            $query = $this->db->get();
                            return $query->result();
                          }
                          if($category_type == "subSubCatId"){
                            $this->db->select('b.category_name, b.permalink');
                            $this->db->from('fb_user_category a');
                            $this->db->join('fb_sub_sub_category_master b', 'b.id=a.subSubCategory');
                            $this->db->where('a.user_id', $user_id);
                            $this->db->where('a.mainCategory', $type_id);
                            $this->db->where('category_type', 'subSubCatid');
                            $this->db->order_by('b.category_name');
                            $query = $this->db->get();
                            return $query->result();
                          }


                        }
	function file_check($url, $file_name){
		if(file_exists($url) && !$file_name == "")
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
