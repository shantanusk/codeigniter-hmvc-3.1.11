<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * proeraz.com
 *
 * @package    Proeraz.com By Shantanusk.com
 * 
 * @since      Version 0.0.1
 * @filesource
 * @todo       (description)
 *
 */

class Shop extends MY_Controller
{
    //
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
    protected $data = array();

    /**
     * [__construct description]
     *
     * @method __construct
     */
    public function __construct()
    {
        // To inherit directly the attributes of the parent class.
        parent::__construct();

        // CI profiler
        $this->output->enable_profiler(false);

        // This function returns the main CodeIgniter object.
        // Normally, to call any of the available CodeIgniter object or pre defined library classes then you need to declare.
        $CI =& get_instance();

        //Example data
        // Site name
        $this->data['name'] = APP_NAME;

        //Example data
        // Browser tab
        $this->data['title'] = ucfirst(APP_NAME);
        $this->data['keywords'] = ucfirst(APP_NAME);
        $this->data['descriptions'] = ucfirst(APP_NAME);

    }

    /**
     * Template loading function for AdminLTE
     *
     * @method template
     *
     * @param  string   $template_name The template name
     * @param  array    $data          All extra datas you want to display
     * @param  boolean  $return        Load the complete template structure
     *                                 with the menues, sidebar, ... or only the page template
     *
     * @return [type]                  Display the template
     */
    protected function template($template_name, $data, $return)
    {
        $content = "";
        if ($return === true) {

            $content  = $this->load->view('theme/template_shop_v1/header', $this->data);
            $content .= $this->load->view('theme/template_shop_v1/navbar', $this->data);
            $content .= $this->load->view('theme/template_shop_v1/search', $this->data);
            foreach($template_name as $temp){
                $content .= $this->load->view($temp, $this->data);
            }            
            $content .= $this->load->view('theme/template_shop_v1/footer-content', $this->data);
            $content .= $this->load->view('theme/template_shop_v1/footer', $this->data);

            return $content;
        } else {
            foreach($template_name as $temp){
                $content .= $this->load->view($temp, $this->data);
            } 
        }
    }
}
