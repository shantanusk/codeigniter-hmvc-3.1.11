<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * proeraz.com
 *
 * @package    Proeraz.com By Shantanusk.com
 *
 * @since      Version 0.0.1
 * @filesource
 * @todo       (description)
 *
 */

class User_backend extends MY_Controller
{
    //
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
    protected $data = array();

    /**
     * [__construct description]
     *
     * @method __construct
     */

    public function __construct()
    {
        // To inherit directly the attributes of the parent class.
        parent::__construct();

        // CI profiler
        $this->output->enable_profiler(false);

        // This function returns the main CodeIgniter object.
        // Normally, to call any of the available CodeIgniter object or pre defined library classes then you need to declare.
        $CI =& get_instance();

        //Example data
        // Site name
        $this->data['name'] = APP_NAME;

        //Example data
        // Browser tab
        $this->data['title'] = "Filmboard Movies is here to change the way films are made in India : ".$this->page." :: ".APP_SRT_NAME;
        $this->data['keywords'] = "Filmboard is unique e-commerce platform that enables filmmakers to book verified film-related services, locations, crew and talent in few easy steps online.";
        $this->data['descriptions'] = "Filmboard is unique e-commerce platform that enables filmmakers to book verified film-related services, locations, crew and talent in few easy steps online.";



        if(!$this->session->userdata('is_logged_in')){
          $this->data['is_logged_in'] = FALSE;
            redirect(base_url('user/sign-in'));
        }else{
            $this->logged_in_user = $this->session->userdata('logged_data');
            $this->data['logged_in_user'] = $this->logged_in_user;
            $this->data['is_type'] = $this->session->userdata('is_type');
            $this->data['is_logged_in'] = TRUE;
            $this->data['user_id'] = $this->logged_in_user->id;
        }
    }

    /**
     * Template loading function for AdminLTE
     *
     * @method template
     *
     * @param  string   $template_name The template name
     * @param  array    $data          All extra datas you want to display
     * @param  boolean  $return        Load the complete template structure
     *                                 with the menues, sidebar, ... or only the page template
     *
     * @return [type]                  Display the template
     */
    protected function template($template_name, $data, $return)
    {
        $content = "";
        if ($return === true) {
          $content  = $this->load->view('theme/template_user_v1/header', $this->data);
            $content .= $this->load->view('theme/template_user_v1/top-navbar', $this->data);
              $content .= $this->load->view('theme/template_user_v1/left-sidebar', $this->data);

            //$content .= $this->load->view('shop/banner', $this->data);

            foreach($template_name as $temp){
                $content .= $this->load->view($temp, $this->data);
            }
            $content .= $this->load->view('theme/template_user_v1/footer', $this->data);

            return $content;
        } else {
            foreach($template_name as $temp){
                $content .= $this->load->view($temp, $this->data);
            }
        }
    }


    protected function pagination_summary(){
        $total_rows = $this->data['total_rows'];
        if($total_rows>10){
            if($this->pagination->cur_page==1)
                $start = $this->pagination->cur_page;
            else
                $start = (($this->pagination->cur_page-1) * $this->pagination->per_page)+1;

            if($this->pagination->cur_page * $this->pagination->per_page > $total_rows)
                $end = $total_rows;
            else
                $end = $this->pagination->cur_page * $this->pagination->per_page;

            $this->data['pagination_des'] = "Showing ".($start)." to ".($end)." from ". $total_rows." results";
        }else
        {
            if($total_rows>0){
                $this->data['pagination_des'] = "Showing ".($this->pagination->cur_page+1)." to ".($total_rows)." from ". $total_rows." results";
            }else{
                $this->data['pagination_des'] = "No Record Found!";
            }
        }
    }
}
