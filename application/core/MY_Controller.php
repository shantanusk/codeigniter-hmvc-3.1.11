<?php defined('BASEPATH') or exit('No direct script access allowed');


class MY_Controller extends MX_Controller
{	

	function __construct() 
	{
		parent::__construct();
		$this->_hmvc_fixes();
		 // Copyright year calculation for the footer
		 $begin = 2017;
		 $end =  date("Y");
		 $date = "$begin - $end";
 
		 // Copyright
		 $this->data['copyright'] = $date." &copy; ".APP_NAME.". All Right Reserved.";
	}
	
	function _hmvc_fixes()
	{		
		//fix callback form_validation		
		//https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
